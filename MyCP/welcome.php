<?php
$PageTitle = "Manage Users";
include('header.php');

function unEscapeChars($value){
	if (is_array($value))
		$value = array_map('security', $value);
	else
		$value = stripslashes($value);
	
	return $value;
}

$where = " WHERE 1 ";
$PAGE_SIZE = 10;

if (isset($_REQUEST['mode']) && intval($_REQUEST['user']) > 0 && $_REQUEST['mode'] == 'delete') {
    
    $userid = intval($_REQUEST['user']);

    $sqlselect1 = "SELECT * FROM jos_users WHERE id = '" . $userid . "'";
    $queryse1   = $db->query($sqlselect1);
    $rsCount    = $queryse1->size();
  
    try {
        $db->begainTransaction();
            
    if ($rsCount > 0)
    {
       	
					
	   
        $sql = "delete from jos_community_wall WHERE type in('photos') AND contentid in (select id from jos_community_photos where creator =" . $userid.")";
        $db->query($sql);

        $sql = "delete from notification_post where posttype in('PHOTO') AND  mainid in (select id from jos_community_photos where creator =" . $userid.")";
        $db->query($sql);

        $sql = "delete from notification_comment where posttype in('PHOTO') AND mainid in (select id from jos_community_photos where creator =" . $userid.")";
        $db->query($sql);

        $sql = "delete from report where type in('photos') AND  postid in (select id from jos_community_photos where creator =" . $userid.")";
        $db->query($sql);
        
        $sql = "delete from jos_community_likes where element = 'photo' AND  uid in (select id from jos_community_photos where creator =" . $userid.")";
        $db->query($sql);
        
        $sql = "delete from photohashtag where type in('PHOTOS') AND photoid in (select id from jos_community_photos where creator =" . $userid.")";
        $db->query($sql);
        
				 
        
        $sql = "delete from jos_community_likes where element = 'album' AND  uid in (select id from jos_community_photos_albums where creator =" . $userid.")";
        $db->query($sql);
        
        $sql = "delete from photohashtag where type in('ALBUMS') AND albumid in (select id from jos_community_photos_albums where creator =" . $userid.")";
        $db->query($sql);
        
        $sql = "delete from report where type in('albums') AND  postid in (select id from jos_community_photos_albums where creator =" . $userid.")";
        $db->query($sql);	 
        
        $sql = "delete from jos_community_wall where type in('albums') AND contentid in (select id from jos_community_photos_albums where creator =" . $userid.")";
        $db->query($sql);
         
	$sql = "delete from notification_post where posttype in('ALBUM') AND  mainid in (select id from jos_community_photos_albums where creator =" . $userid.")";
        $db->query($sql);	 
	
        $sql = "delete from notification_comment where posttype in('ALBUM') AND mainid in (select id from jos_community_photos_albums where creator =" . $userid.")";
        $db->query($sql);	 
        
        
        $sql = "delete from jos_community_photos WHERE albumid in (select id from jos_community_photos_albums where creator =" . $userid.")";
        $db->query($sql);
        
        
        $sql = "delete from comment_collage WHERE  albumid in (select id from jos_community_photos_albums where creator =" . $userid.")";
        $db->query($sql);
        
               
        
        $sql = "delete from albumdetail where userid =" . $userid;
        $db->query($sql);
        
        $sql = "delete from jos_community_photos_albums where creator =" . $userid;
        $db->query($sql);
        
        
                	
        $sql="delete from jos_community_fields where id in (select field_id from jos_community_fields_values where user_id =" . $userid.")";
        $db->query($sql);
        
        $sql="delete from jos_community_fields_values where user_id =" . $userid;
        $db->query($sql);
        
        
        $sql="delete from jos_community_activities where actor =" . $userid;
        $db->query($sql);
        
        $sql="delete from jos_community_wall where post_by =" . $userid;
        $db->query($sql);
        
        $sql = "delete from notification_post where userid =" . $userid;
        $db->query($sql);
            
        $sql = "delete from notification_comment where userid =" . $userid;
        $db->query($sql);

        $sql = "delete from notification_other where userid =" . $userid;
        $db->query($sql);
        
        $sql = "delete from notification_other where mainid =" . $userid;
        $db->query($sql);
                    
        $sql = "delete from jos_community_users where userid =" . $userid;
        $db->query($sql);
        
        $sql="delete from jos_core_acl_groups_aro_map where aro_id in (select id from jos_core_acl_aro where value =" . $userid.")";
        $db->query($sql);
        
        $sql="delete from jos_core_acl_aro where value =" . $userid;
        $db->query($sql);
        
        $sql="delete from user_token where user_id =" . $userid;
        $db->query($sql);
        
        $sql="delete from last_seen where userid =" . $userid;
        $db->query($sql);
                       
        $sql = "delete from jos_community_blocklist where userid =" . $userid;
        $db->query($sql);

        $sql = "delete from jos_community_blocklist where blocked_userid =" . $userid;
        $db->query($sql);

        $sql = "delete from jos_community_connection where connect_from =" . $userid;
        $db->query($sql);

        $sql = "delete from jos_community_connection where connect_to =" . $userid;
        $db->query($sql);

         

    }
    $db->query("delete from jos_users where id =" . $userid);
    
    $db->commit();

    $_SESSION["msg"] = "Successfully deleted.";
    
    }catch(Exception $e)
        {
                $db->rollBack();
                $_SESSION['errormsg']="We are sorry.Something goes wrong, please try again.<br>";
        }
          cheader('welcome.php');
}

if (($_REQUEST["mode"] == "block") && intval($_REQUEST['id'] > 0)) {


    $user_no    =   intval($_REQUEST["id"]);
    $status     =   $_REQUEST["status"];
    $db_status = 0;
    if (strtoupper($status) == 'BLOCK') {
        $db_status = 1;
    } else if (strtoupper($status) == 'UNBLOCK') {
        $db_status = 0;
    }

    $sql = "UPDATE jos_users SET block='" . $db_status . "' WHERE id=" . $user_no;
    $res = $db->query($sql);
    $_SESSION["msg"] = "User status changed successfully.";
    cheader("welcome.php");
}

if (isset($_REQUEST['name']) && trim($_REQUEST['name']) != '') {
    $name   =   security(trim($_REQUEST['name']));
    $where .=  " and UPPER(name) like '%" . strtoupper($name) . "%' ";
}
if (isset($_REQUEST['username']) && trim($_REQUEST['username']) != '') {
    $username = security(trim($_REQUEST['username']));
    $where   .= " and UPPER(username) like '%" . strtoupper($username) . "%' ";
}
if (isset($_REQUEST['email']) && trim($_REQUEST['email']) != '') {
    $email  =   security(trim($_REQUEST['email']));
    $where .=  " and email like '%" . $email . "%' ";
}
?>
<style>
    #managetable tbody td{vertical-align:top !important;}
    .sendcheckbox{ float:none !important; width:auto !important;}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_path ?>css/popup_box.css" />


<script src="<?php echo base_path ?>js/popup.js" type="text/javascript"></script>
<script>

     function confirmDelete()
	{
		var agree=confirm("Are you sure you want to delete this?");
		if (agree)
			 return true ;
		else
			 return false ;
	}
    function confirmStatus()
    {
        var agree = confirm("Are you sure you want to change user's status?");
        if (agree)
            return true;
        else
            return false;
    }
    function Changepagesize(url, pagesize)
    {
        var url = url + "&pagesize=" + pagesize;
        window.location = url;
    }
    
</script>


<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3 style="cursor: s-resize;">Manage Users</h3>

        <div class="clear"></div>
    </div>
    <div class="content-box-content">
        <?php
        $ERROR_MSG = isset($_SESSION["errormsg"]) ? $_SESSION["errormsg"] : '';
        $MSG = isset($_SESSION["msg"]) ? $_SESSION["msg"] : '';
        if ($ERROR_MSG != "") {
            ?>
            <div class="notification error png_bg"> <a class="close" href="javascript:showDetails('msgError');"><img alt="close" title="Close this notification" src="<?php echo base_path ?>images/cross.png"></a>
                <div><?php echo $ERROR_MSG; ?></div>
            </div>
        <?php } elseif ($err_msg != '') {
        ?>

            <div class="notification error png_bg" id="msgError"> <a class="close" href="javascript:showDetails('msgError');"><img alt="close" title="Close this notification" src="<?php echo base_path ?>images/icons/cross.png"></a>
                <div><?php echo $err_msg; ?></div>

            </div>
        <?php } elseif ($MSG != "") {
            ?>
                    <div class="notification success png_bg"> <a class="close" href="javascript:showDetails('msgOk');"><img alt="close" title="Close this notification" src="<?php echo base_path ?>images/cross.png"></a>
                        <div><?php echo $i; ?><?php echo $MSG; ?></div>
                    </div>
            <?php
        }

        unset($_SESSION["errormsg"]);
        unset($_SESSION["msg"]);
        ?>  
        <!--Search Div-->

        <div style="padding-left:5px; padding-right:5px">
            <div>
                <form action="welcome.php" method="get">
                    <table cellpadding="2" cellspacing="2" width="100%">
                        <tr>
                            <td colspan="7"><h3>Search By</h3></td>
                        </tr>
                        <tr>

                            <td width="13%" align="right">Name</td>
                            <td width="10%" align="left">
                                <input type="text" name="name" value="<?php echo $name ?>" />
                            </td>
                            <td width="13%" align="right">Username</td>
                            <td width="10%" align="left">
                                <input type="text" name="username" value="<?php echo $username ?>" />
                            </td>
                            <td width="13%" align="right">Email</td>
                            <td width="10%" align="left">
                                <input type="text" name="email" value="<?php echo $email ?>" />
                            </td>

                            <td><input  class="button" type="submit" name="btngo" value="Search" style="padding:0px; width:100px; margin-top:5px"/></td>
                        </tr>

                    </table>
                </form>
            </div>
        </div>
        <!--Search Div-->

        <div class="Registerinner">
            <?php
            $so = "DESC";
            $sql = "SELECT * FROM jos_users " . $where;



            $res = $db->query($sql);
            $resultCount = $res->size();

            if ($resultCount > 0) {


                if (!isset($_GET['pagesize']))
                    $pagesize = $PAGE_SIZE;
                else {
                    if (intval($_GET['pagesize']) <= 0)
                        $pagesize = $PAGE_SIZE;
                    else
                        $pagesize = intval($_GET['pagesize']);
                }

                if (!isset($_GET['pageindex']))
                    $pageindex = 1;
                else {
                    if (intval($_GET['pageindex']) <= 0)
                        $pageindex = 1;
                    else
                        $pageindex = intval($_GET['pageindex']);
                }
                $totalpages = ceil($resultCount / $pagesize);
                $limitstr = "limit " . ($pageindex - 1) * $pagesize . ", " . $pagesize;
                $rcount = $pageindex * $pagesize;

                if (isset($_GET["so"])) {
                    $so = $_GET["so"];
                    if ($so == "ASC")
                        $so = "DESC";
                    else
                        $so = "ASC";
                } else
                    $so = "ASC";

                if (isset($_GET["oby"]) && $_GET["oby"] != "") {
                    switch ($_GET["oby"]) {


                        case "name":
                            $orderBy = " name ";
                            break;
                        case "username":
                            $orderBy = " username ";
                            break;
                        case "email":
                            $orderBy = " email ";
                            break;

                        default:
                            $orderBy = "id";
                    }
                    $queryB = $sql . " order by " . $orderBy . " $so $limitstr";
                } else {
                    $queryB = $sql . " ORDER BY id DESC $limitstr";
                }

                $resultB = $db->query($queryB);
                

            }
            $qStr = $_SERVER['PHP_SELF'] . "?pageindex=" . $pageindex . "&pagesize=" . ($pagesize) . "&so=".$so."&username=".$_GET["username"]."&name=".$_GET["name"]."&email=".$_GET["email"];
            $qStrPageSize = $_SERVER['PHP_SELF'] . "?pageindex=" . $pageindex . "&so=" . $so . "&oby=" . $_GET["oby"]."&username=".$_GET["username"]."&name=".$_GET["name"]."&email=".$_GET["email"];

            ?>

                <table border="1" width="100%" id="managetable">
                    <thead>
                    <tr>
                        <th width="80" height="">Sr.No</th>
                        <th width="250"><div align="left"><a href="<?php echo $qStr.'&oby='.'name' ?>">Name    <?php 
			   if($_GET['oby']=='name') echo '<img src="'.base_path.'images/'.strtolower($so).'.gif" />';
			   ?></a></div></th>
                        <th width="250"><div align="left"><a href="<?php echo $qStr.'&oby='.'username' ?>">Username    <?php 
			   if($_GET['oby']=='username') echo '<img src="'.base_path.'images/'.strtolower($so).'.gif" />';
			   ?></a></div></th>
                        <th width="250"><div align="left"><a href="<?php echo $qStr.'&oby='.'email' ?>">Email    <?php 
			   if($_GET['oby']=='email') echo '<img src="'.base_path.'images/'.strtolower($so).'.gif" />';
			   ?></a></div></th>
                        <th width="250">Status</th>
                        <th width="100">Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        if ($resultCount > 0) {
                                $result = $db->query($sql);
                        $i = $pagesize * ($pageindex - 1);
                        while ($row = $resultB->fetch()) 
                        {
                        $i++;
                        ?>
                                <tr><td height="30px;" valign="top"><?php echo $i; ?></td>

                                    <td valign="top"><?php echo unEscapeChars($row['name']) ?></td>
                                    <td valign="top"><?php echo unEscapeChars($row['username']) ?></td>

                                    <td valign="top"><?php echo unEscapeChars($row['email']) ?></td>
                                    <td class="text-red">
                                    <?php
                                    if ($row['block'] == '0') {
                                        $but_lbl = "Block";
                                        $but_cls = "button red";
                                        $but_val = 'Block';
                                    }

                                    if ($row['block'] == '1') {
                                        $but_lbl = "Unblock";
                                        $but_cls = "button ";
                                        $but_val = 'Unblock';
                                    }
                                    ?>
                                        <a href="?id=<?php echo $row['id'] ?>&mode=block&status=<?php echo $but_val; ?>"  onclick="return confirmStatus();" style="width:50px;text-align:center;" class="<?php echo $but_cls ?>"><?php echo $but_lbl; ?></a>
                                    </td>

                                    <td valign="top">
                                        <a href="welcome.php?user=<?php echo $row['id']; ?>&mode=delete" onclick="return confirmDelete()" title="Delete"><img src="<?php echo base_path ?>images/dd.gif" /></a>
                                    </td>

                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                                <tr><td colspan="7"><center>Record not found.</center></td></tr>
                        <?php }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="7"><div class="pagination">
                                    <div  class="align-left"><abbr>View</abbr>
                                        <select name="pagesize" onchange="Changepagesize('<?php echo $qStrPageSize ?>', this.value)">
                                        <?php
                                        $i = $PAGE_SIZE;
                                        while ($i <= $resultCount + $PAGE_SIZE) {
                                            if (($i % $PAGE_SIZE) == 0) {
                                                if ($i == $pagesize) {
                                                    ?>
                                                        <option value="<?php echo($i); ?>" selected><?php echo($i); ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option value="<?php echo($i); ?>" ><?php echo($i); ?></option>
                                                        <?php
                                                    }
                                                }
                                                $i = $i + $PAGE_SIZE;
                                            } // end of for loop
                                            ?>
                                        </select>
                                        <abbr>Row(s) per page</abbr>
                                    </div>
                                    <div class="align-right">
                                        <?php
                                          $url = $_SERVER['PHP_SELF'] . "?so=" . $_GET["so"] . "&pagesize=" . ($pagesize) . "&oby=" . $_GET["oby"] . "&username=".$_GET["username"]."&name=".$_GET["name"]."&email=".$_GET["email"];
                                        echo getPagingHtml($resultCount, $pagesize, $pageindex, $url)
                                        ?>
                                    </div>
                                </div></td>
                        </tr>
                    </tfoot>
                </table>
            
            <div style="clear:both"></div>

        </div>
    </div>
    <div class="clear"></div> 
</div>
<?php include("footer.php"); ?>