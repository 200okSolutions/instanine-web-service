<?php
$PageTitle = "Manage Pals";
include('header.php');
$where	=	" WHERE 1";
$PAGE_SIZE=10;

if(isset($_REQUEST['mode']) && intval($_REQUEST['id']) > 0 && $_REQUEST['mode']=='delete')
{
	$id=intval($_GET['id']);
	$sql=" DELETE FROM `admin_post` WHERE `id` = $id";
	$db->query($sql);
	$_SESSION["msg"]	=	"Pal deleted successfully.";
	cheader("manage_post.php");
}

if(isset($_REQUEST['category']) && trim($_REQUEST['category'])!='')
{
	$category=security(trim($_REQUEST['category']));
	$where.=" and AP.cat_id = '".$category."' ";
}


?>
<style>
#managetable tbody td{vertical-align:top !important;}
</style>
<script>

function confirmDelete()
	{
		var agree=confirm("Are you sure to delete this Post ?");
		if (agree)
			 return true ;
		else
			 return false ;
	}
 	function Changepagesize(url, pagesize)
	{
		var url = url+"&pagesize="+pagesize;
		window.location=url;
	}
</script>.

<div class="content-box"><!-- Start Content Box -->
  
  <div class="content-box-header">
     <h3 style="cursor: s-resize;"><?php echo $PageTitle?></h3>
    
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
  <?php 
					$ERROR_MSG= isset($_SESSION["errormsg"]) ? $_SESSION["errormsg"] :'';
					$MSG=  isset($_SESSION["msg"]) ? $_SESSION["msg"]  : '';
					if ($ERROR_MSG!="") {?>
      <div class="notification error png_bg"> <a class="close" href="javascript:showDetails('msgError');"><img alt="close" title="Close this notification" src="<?php echo base_path ?>images/cross.png"></a>
        <div><?php echo $ERROR_MSG;?></div>
      </div>
      <?php } 
	  elseif ($err_msg!='') {?>
     
    <div class="notification error png_bg" id="msgError"> <a class="close" href="javascript:showDetails('msgError');"><img alt="close" title="Close this notification" src="<?php echo base_path ?>images/icons/cross.png"></a>
      <div><?php echo $err_msg;?></div>
      
    </div>
    <?php }
							   elseif ($MSG!="") {?>
      <div class="notification success png_bg"> <a class="close" href="javascript:showDetails('msgOk');"><img alt="close" title="Close this notification" src="<?php echo base_path ?>images/cross.png"></a>
        <div><?php echo $i;?><?php echo $MSG;?></div>
      </div>
      <?php } 
	  
	unset($_SESSION["errormsg"]);
	unset($_SESSION["msg"]);
?>  
 <!--Search Div-->
  
  <div style="padding-left:5px; padding-right:5px">
	<div>
		<form action="" method="get">
		<table cellpadding="2" cellspacing="2" width="100%">
			<tr>
            		<td colspan="6"><h3>Search By</h3></td>
            </tr>
			<tr>
            	
				<td width="13%" align="right">Category Name</td>
    			<td width="10%" align="left">
                <select name="category" id="category" >
         			<option value="">Select category</option>
         			<?php $sql = $db->query("select * from  `admin_category`");
								while($row = $sql->fetch())
								{
								 ?>
         				<option value="<?php echo $row['cat_id']; ?>"><?php echo $row['cat_name']; ?></option>
                                <?php } ?>
         
         </select>
         <script type="text/javascript">
		 document.getElementById("category").value	=	"<?php echo $category?>";
		 </script>
                </td>
                
               
                 <td><input  class="button" type="submit" name="btngo" value="Search" style="padding:0px; width:100px; margin-top:5px"/> 				</td>
             </tr>
             
	</table>
    </form>
</div>
</div>
  <!--Search Div-->
  
   <div class="Registerinner">
<?php


$sql ="SELECT AP.id,title,AP.image,AP.upload_type,AP.thumbnail,description,AC.cat_name from admin_post AS AP INNER join admin_category AS AC on AP.cat_id=AC.cat_id ".$where;



$res=$db->query($sql);
$resultCount = $res->size();
	
		if ($resultCount > 0)
		{
					
					
					if(!isset($_GET['pagesize']))
					$pagesize =$PAGE_SIZE;
					else{
					if(intval($_GET['pagesize'])<=0)
					$pagesize =$PAGE_SIZE;
					else
					$pagesize = intval($_GET['pagesize']);
					}
				
					if(!isset($_GET['pageindex']))
					$pageindex = 1;
					else{
					if(intval($_GET['pageindex'])<=0)
					$pageindex = 1;
					else
					$pageindex = intval($_GET['pageindex']) ;
		}
					$totalpages = ceil($resultCount/$pagesize);
					$limitstr 	= "limit ".($pageindex-1)*$pagesize.", ".$pagesize;
					$rcount 	= $pageindex*$pagesize;
										
					if (isset($_GET["so"]))
					{
						$so=$_GET["so"];
						if ($so=="ASC")
							$so="DESC";
						else
							$so="ASC";
					}
					else
						$so="ASC";
					
					if (isset($_GET["oby"]) && $_GET["oby"]!="")
					{
						switch($_GET["oby"])
						{
							
							
							case "title":
								$orderBy	=	" UP.title ";
							break;
							
							default:
							$orderBy	=	"dtdate";
							
						}
						$queryB = $sql." order by ".$orderBy." $so $limitstr" ;
					}	
					else
					{
						$queryB = $sql." ORDER BY AP.dtdate  DESC $limitstr" ;
					}	
					
					$resultB = $db->query($queryB);
					
					
					
		
					$qStr			=	$_SERVER['PHP_SELF']."?pageindex=".$pageindex."&pagesize=".($pagesize)."&so=$so&category=".$_GET['category'];
					$qry_string		=	$_SERVER['QUERY_STRING'];
				  	$qStrPageSize	=	$_SERVER['PHP_SELF']."?".$qStr;
		 }
		 

?>

<table border="1" id="managetable">
<?php
if(isset($_GET['mess']))
{
?>
<tr><td><?php echo ($_GET['mess']) ?></td></tr>
<?php }?>
<tr>
<td height="">Sr.No</td>
<td>Title</td>
<td>Category Name</td>
<td>Image</td>
<td>Description</td>
<td width="50px;">Edit</td>
<td width="50px;">Delete</td>
</tr>
<tbody>
<?php 
if($resultCount > 0)
{
$i=1;
while($row=$resultB->fetch())
	{
?>
<tr><td height="30px;" valign="top"><?php echo $i; ?></td>

<td valign="top"><?php echo unEscapeChars($row['title']) ?></td>

<td><?php echo $row['cat_name']; ?></td>
<?php   if($row['upload_type']=='IMAGE'){
			$img_path = "../".$row['image'];
			if (!file_exists($img_path))
				$img_path	= base_path."images/defult-photo.png";
		}else if($row['upload_type']=='VIDEO'){
			$img_path = base_path."images/defult-photo.png";
			}?>

<td><img src="<?php echo $img_path; ?>" width="70px" height="50" /></td>

<td valign="top"><?php echo unEscapeChars($row['description']); ?></td>

	<td valign="top"><a href="galery.php?id=<?php echo $row['id']; ?>" title="Edit"><img src="images/user_edit.png" height="15px;" /></a></td>

<td valign="top"><a href="manage_post.php?id=<?php echo $row['id']; ?>&mode=delete" onclick="return confirmDelete()" title="Delete"><img src="images/trash.png" height="15px;" /></a></td>


</tr>
<?php 
$i++;	
	}
}else{?>
<tr><td colspan="7" align="center"><strong>Record not found.</strong></td></tr>
<?php }
?>
</tbody>
</table>
<tfoot>
          <tr>
            <td colspan="7"><div class="pagination">
                <div  class="align-left"><abbr>View</abbr>
                  <select name="pagesize" onchange="Changepagesize('<?php echo $qStrPageSize?>',this.value)">
                    <?php
							$i = $PAGE_SIZE;
		                    while($i <= $resultCount+$PAGE_SIZE)
							{
								if(($i%$PAGE_SIZE) == 0)
								{
		                      		if( $i == $pagesize)
									{
									?>
                    <option value="<?php echo($i) ; ?>" selected><?php echo($i) ; ?></option>
                    <?php
									}						
									else
									{
									?>
                    <option value="<?php echo($i) ; ?>" ><?php echo($i) ; ?></option>
                    <?php
									}
								}
								$i=$i+$PAGE_SIZE;
							} // end of for loop
							?>
                  </select>
                  <abbr>Row(s) per page</abbr>
                  </div>
                  <div class="align-right">
                  <?php								
				$qry_string	=	$_SERVER['QUERY_STRING'];
				$url		=	$_SERVER['PHP_SELF']."?".$qry_string; 
				echo getPagingHtml($resultCount,$pagesize,$pageindex,$url)			
			?>
                </div>
              </div></td>
          </tr>
        </tfoot>





          <div style="clear:both"></div>
        
      </div>
    </div>
     <div class="clear"></div> 
</div>
<?php include("footer.php"); ?>
