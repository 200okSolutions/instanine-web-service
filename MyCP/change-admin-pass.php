<?php 
$PageTitle = "Change Password";
include('header.php');
include('permission.php');

$sql="select * from adminlogin where userid='".$_SESSION["admin"]."'";
$result1=$db->query($sql);
if ($result1->size()>0)
{
	$row=$result1->fetch();
	$userid=$row["userid"];
	$email=$row["email"];
	$email2=$row["email2"];
	$username=$row["name"];
}
if ($_POST["btnsubmit"])
{
	$email		=	security(strtolower(trim($_POST["p-email"])));
	$email2		=	security(strtolower(trim($_POST["s-email"])));
	
	$sql	=	" select * from adminlogin where userid='".$_SESSION["admin"]."' ".
				" and pass='".md5($_POST["oldpass"])."'";
	$result1=	$db->query($sql);
	
	if ($result1->size()>0)
	{
		if($_POST['username']==$_SESSION["admin"])
		{
			
			$sql1="update `adminlogin` set `userid`='".$_POST["username"]."', `pass`='". md5($_POST["confpass"])."', email='".$email."', email2='".$email2."'  where `userid`='".$_POST["id"]."'";
			//echo $sql1;
			//exit;
			$result=$db->query($sql1);
			$_SESSION["msg"]="User name and password Successfully changed !";
		}
		else
		{
			 $sql2="select * from adminlogin where userid='".$_POST['username']."'";
			 $res=$db->query($sql2);
			//echo $res->size();
			if($res->size()>0)
			{
				$_SESSION['errormsg']="User name already exist!";		
			}
			else
			{
				$sql1="update `adminlogin` set `userid`='".$_POST["username"]."', `pass`='". md5($_POST["confpass"])."' where `userid`='".$_POST["id"]."'";
	//			echo $sql1;
				$result=$db->query($sql1);
				$_SESSION["msg"]="User name and password Successfully changed !";
			}
		}
		cheader("change-admin-pass.php?mode=logout");
	}
	else
		$_SESSION['errormsg']="User name and old password does not match !";
}

?>
<script src="../js/jquery.metadata.js" type="text/javascript"></script>
<script src="../js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
$.metadata.setType("attr", "validate");
$(document).ready(function() {
	$('#frmuser').validate({
	 wrapper: "div",
	})
 });
</script>

    <div id=title-bar>
      <ul id=breadcrumbs>
        <li><a href="dashboard.html" title=Home><span id=bc-home></span></a></li>
        <li class=no-hover>Change Admin Password</li>
      </ul>
    </div>
    <div class="shadow-bottom shadow-titlebar"></div>
    <div id=main-content>
      <div class=container_12>
        <div class=grid_12>
          <h1>Reset Password</h1>
          <P><?php echo (PAGE_DETAIL) ?></P>
        </div>
        <div class=grid_6>
         <?php 
$ERROR_MSG= isset($_SESSION["errormsg"]) ? $_SESSION["errormsg"] :'';
$MSG=  isset($_SESSION["msg"]) ? $_SESSION["msg"]  : '';
if ($ERROR_MSG!="") {?>
	<div class="msg" id="msgError" >
    <div class="msg-error">
    <p><?php echo $ERROR_MSG;?></p>
     <a class="close" href="javascript:showDetails('msgError');">close</a></div></div>
  <?php }
elseif ($err_msg!='') {?>
	<div class="msg" id="msgError" >
    <div class="msg-error">
    <p><?php echo $err_msg;?></p>
    <a class="close" href="javascript:showDetails('msgError');">close</a></div></div>
  <?php }   
elseif ($MSG!="") {?>
    <div class="msg" id="msgOk" >
    <div class="msg-ok">
    <p><?php echo $MSG;?></p><a class="close" href="javascript:showDetails('msgOk');">close</a></div></div>
    <?php } 
	unset($_SESSION["errormsg"]);
	unset($_SESSION["msg"]);
?>  
          <div class=block-border>
            <div class=block-header>
              <h1>
              <?php 
			  if ($USER_TYPE=="ADMIN"){?>
	              Reset Super Administrator (CEO) User Name & Password 
             <?php }else{ ?>
             	Reset Your Password 
              <?php } ?>
              </h1>
              <span></span> </div>
              
              <form name="frmuser" id="frmuser" action="#" method="post" class="block-content form"  >
              
              <div id="errorMsg" style="display:<? if ($strErr!="") { echo "block";} else {echo "none";} ?>; "></div>
             
              <input type='hidden' name='id' size='25' value="<?php echo $userid ?>">
              <div class="_100">
              	<p>
                <label for=textfield>User Name</label>
                <input type='text' name='username' size='25' value="<?php echo $userid ?>">
                </p>
                </div>
                <div class="_100">
              	<p>
                <label for=textfield>Primary Email</label>
                <input type='text' class="required email" name='p-email' size='25' value="<?php echo $email ?>">
                </p>
                </div>
                <div class="_100">
              	<p>
                <label for=textfield>Secondary Email</label>
                <input type='text' name='s-email' size='25' value="<?php echo $email2 ?>">
                </p>
                </div>
                <div class="_100">
              	<p>
                <label for=textfield>Old Password</label>
                <input type="password" name="oldpass" size="25" class="required">
                </p>
                </div>
                <div class="_100">
              	<p>
                <label for=textfield>New Password</label>
                <input type="password" name="pass" id="pass" size="25" minlength="5" maxlength="15" class="required" >
                </p>
                </div>
                <div class="_100">
              	<p>
                <label for=textfield>Confirm Password</label>
                <input type="password" name="confpass" size="25" class="required" equalTo="#pass">
                </p>
                </div>	
                
                <div class=clear></div>
                <div class="error"><?php if ($msg!="") {
	echo "<div class='error'>$msg";
	} ?></div>
              <div class=block-actions>
                
                <ul class="actions-left">
                <li>
                <input type="submit" class="button" style="height:26px; padding-top:4px; width:" name="btnsubmit" value=" Submit ">
                </li>
                </ul>
              <ul class=actions-right>
                  <li>
                   <input type="reset" class="button red" style="height:26px; padding-top:4px; width:50px;" name="save" value="Clear ">
                  </li>
                </ul>
				
              
              </div>
              </form>
          </div>
        </div>
      </div>
    </div>
<?php include('footer.php');?>



