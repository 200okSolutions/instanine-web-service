<?php 
require_once("../web-services/config.php");
require_once("../web-services/functions.php");
require_once('../web-services/classes/mysql.class.php');
require_once('../web-services/classes/functions_general.php');
require_once('../web-services/classes/global.inc.php');
// Include user class        


define("PAGE_DETAIL"," ");
if (!is_array($_POST)) {
foreach ($_POST as $key => $value) {
  $_POST[$key] = mysql_real_escape_string($value);
}
}
//This stops SQL Injection in GET vars
foreach ($_GET as $key => $value) {
  $_GET[$key] = mysql_real_escape_string($value);
}



function checkUserActive($user){
	global $db;
	$result=$db->query("select * from adminlogin where userid='$user'");
	
	if ($result->size()<=0)
		return false;
	else
		return true;
}

if (!isset($_SESSION['admin']) || (!checkUserActive($_SESSION['admin'])) || empty($_SESSION['admin']) )
	cheader("index.php");

if (@$_GET["mode"]=="logout")
{
	session_unset();
	session_destroy();
	cheader("index.php");
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?php echo base_path;?>images/favicon.gif" type="image/png" rel="icon">
<title><?php echo  $PageTitle ?></title>

<!--                       CSS                       -->

<!-- Reset Stylesheet -->
<link rel="stylesheet" href="<?php echo base_path?>css/reset.css" type="text/css" media="screen">

<!-- Main Stylesheet -->
<link rel="stylesheet" href="<?php echo base_path?>css/style.css" type="text/css" media="screen">

<!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
<link rel="stylesheet" href="<?php echo base_path?>css/invalid.css" type="text/css" media="screen">
<script type="text/javascript" src="<?php echo base_path?>js/jquery.min.js"></script>

<!-- jQuery Configuration -->
<script type="text/javascript" src="<?php echo base_path?>css/simpla.js"></script>

<!-- Facebox jQuery Plugin -->
<script type="text/javascript" src="<?php echo base_path?>css/facebox.js"></script>

<!-- jQuery WYSIWYG Plugin -->
<script type="text/javascript" src="<?php echo base_path?>css/jquery.js"></script>

<!-- jQuery Datepicker Plugin -->
<script type="text/javascript" src="<?php echo base_path?>css/jquery.htm"></script>
<script type="text/javascript" src="<?php echo base_path?>css/jquery_002.js"></script>
<script type="text/javascript" src="<?php echo base_path?>ckeditor/ckeditor.js"></script>

</head>

<body>
<div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->

<?php require_once('sidebar.php'); ?>
<div id="main-content"> <!-- Main Content Section with everything -->
    
    <noscript>
    <!-- Show a notification if the user has disabled javascript -->
    <div class="notification error png_bg">
      <div> Javascript is disabled or is not supported by your browser. Please <a href="http://browsehappy.com/" title="Upgrade to a better browser">upgrade</a> your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface properly. </div>
    </div>
    </noscript>
    
     
    <!-- Page Head -->
    
    <!-- End .shortcut-buttons-set -->
    
    <div class="clear"></div>
    <!-- End .clear -->