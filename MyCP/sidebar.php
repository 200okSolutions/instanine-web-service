
<div id="sidebar">
    <div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->

        <h1 id="sidebar-title">LOGO</h1>

        <!-- Logo (221px wide) --> 
        <a href="<?php echo base_path ?>welcome.php"><img src="<?= base_path ?>images/logo.png" width="220px;" style="padding-top:10px;" /></a> 

        <!-- Sidebar Profile links -->
        <div id="profile-links"> Hello, <a href="#" ><?php echo $_SESSION["name"] ?></a>&nbsp;&nbsp;&nbsp;
            <?php /* ?><a href="<?php echo base_path?>" title="View the Site" target="new">View the Site</a><?php */ ?>  <a href="<?php echo $_SERVER['PHP_SELF'] ?>?mode=logout" title="Sign Out">Sign Out</a> </div>
        <ul id="main-nav">
            <li> <a href="<?php echo base_path ?>welcome.php" class="nav-top-item no-submenu"> Dashboard </a> </li>


            <!--        <li> <a style="padding-right: 15px;" href="javascript:void(0);" class="nav-top-item"> User Management </a>
                      <ul style="display: none;">
                        <li><a href="manage-users.php">Manage Users</a></li>
                      </ul>
                    </li>-->
            
            <li> <a style="padding-right: 15px;" class="nav-top-item"> Manage Album  </a>
                <ul style="display: none;">
                    <li><a href="album-list.php">Album List </a></li>
                </ul>
            </li>

            <li> <a style="padding-right: 15px;" href="<?php echo base_path ?>inappropriate.php" class="nav-top-item"> Report Inappropriate  </a>
                <ul style="display: none;">
                    <li><a href="album-inappropriate.php">Album Report inappropriate </a></li>
                    <li><a href="photo-inappropriate.php">Photo Report inappropriate </a></li>
                </ul>
            </li>

            <li> <a style="padding-right: 15px;" href="#" class="nav-top-item"> Settings </a>
                <ul style="display: none;">

                    <li><a href="change-pass.php">Change Password</a></li>

                    <li><a href="<?php echo $_SERVER['PHP_SELF'] ?>?mode=logout" title="Sign Out">Sign Out</a></li>
                </ul>
            </li>
        </ul>

    </div>
</div>
