<?php
$PageTitle = "Photo Report Inappropriate ";
include('header.php');

$PAGE_SIZE = 10;

if (isset($_REQUEST['mode']) && intval($_REQUEST['id']) > 0) {
    $id     =   intval($_GET['id']);
    $sqlqs  =   " DELETE FROM report WHERE `id` = $id";
    $db->query($sqlqs);

    $_SESSION["msg"] = "Successfully deleted.";
    cheader("photo-inappropriate.php");
}


$sql    = "SELECT R.userid AS userid, R.msg AS msg, R.id AS id, R.dtdate AS dtdate, P.caption AS caption FROM report AS R "
        . "INNER JOIN jos_community_photos AS P ON R.postid = P.id where R.type = 'photos' ";


$result      = $db->query($sql);
$resultCount = $result->size();

if ($resultCount > 0) {

    if (!isset($_GET['pagesize']))
        $pagesize = $PAGE_SIZE;
    else {
        if (intval($_GET['pagesize']) <= 0)
            $pagesize = $PAGE_SIZE;
        else
            $pagesize = intval($_GET['pagesize']);
    }

    if (!isset($_GET['pageindex']))
        $pageindex = 1;
    else {
        if (intval($_GET['pageindex']) <= 0)
            $pageindex = 1;
        else
            $pageindex = intval($_GET['pageindex']);
    }
    $totalpages = ceil($resultCount / $pagesize);
    $limitstr = "limit " . ($pageindex - 1) * $pagesize . ", " . $pagesize;
    $rcount = $pageindex * $pagesize;

    if (isset($_GET["so"])) {
        $so = $_GET["so"];
        if ($so == "ASC")
            $so = "DESC";
        else
            $so = "ASC";
    } else
        $so = "ASC";

    if (isset($_GET["oby"]) && $_GET["oby"] != "") {
        switch ($_GET["oby"]) {

            case "caption":
                $orderBy = " P.caption ";
                break;

            default:
                $orderBy = "R.id";
        }
        $queryB = $sql . " order by " . $orderBy . " $so $limitstr";
    } else {
        $queryB = $sql . " ORDER BY R.dtdate  DESC $limitstr";
    }

    $resultB = $db->query($queryB);




    $qStr = $_SERVER['PHP_SELF'] . "?pageindex=" . $pageindex . "&pagesize=" . ($pagesize) . "&so=$so";
    $qry_string = $_SERVER['QUERY_STRING'];
    $qStrPageSize = $_SERVER['PHP_SELF'] . "?" . $qStr;
}
?>
<link rel="stylesheet" href="<?= base_path ?>css/validationEngine.jquery.css" type="text/css"/>

<script src="<?= base_path ?>js/jquery-1.7.2.min.js" type="text/javascript">
</script>
<script src="<?= base_path ?>js/popup.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo base_path ?>css/popup_box2.css" />
<script src="<?= base_path ?>js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8">
</script>
<script src="<?= base_path ?>js/jquery.validationEngine.js" type="text/javascript" charset="utf-8">
</script>


<style>
    .button:active{
        padding: 4px !important;
    }

</style>
<!--******************************Interface*****************************-->

<div class="content-box"><!-- Start Content Box -->

    <div class="content-box-header">
        <h3 style="cursor: s-resize;">Photo Report Inappropriate </h3>
        <div class="clear"></div>
    </div>
    <!-- End .content-box-header -->

    <div class="content-box-content"> 

        <!------------------- TAB 1 ------------------------>

        <div style="display: block;" class="tab-content default-tab" id="tab1">
            <?php
            $ERROR_MSG = isset($_SESSION["errormsg"]) ? $_SESSION["errormsg"] : '';
            $MSG = isset($_SESSION["msg"]) ? $_SESSION["msg"] : '';
            if ($ERROR_MSG != "") {
                ?>
                <div class="notification error png_bg"> <a class="close" href="javascript:showDetails('msgError');"><img alt="close" title="Close this notification" src="<?php echo base_path ?>MyCP/images/cross.png"></a>
                    <div><?php echo $ERROR_MSG; ?></div>
                </div>
            <?php } elseif ($MSG != "") {
                ?>
                <div class="notification success png_bg"> <a class="close" href="javascript:showDetails('msgOk');"><img alt="close" title="Close this notification" src="<?php echo base_path ?>MyCP/images/cross.png"></a>
                    <div><?php echo $MSG; ?></div>
                </div>
                <?php
            }

            unset($_SESSION["errormsg"]);
            unset($_SESSION["msg"]);
            ?>

            <table>

                <thead>
                    <th width="5%"><div align="left">S.No</div></th>
                    <th width="10%"><div align="left">Username</div></th>
                    <th width="15%"><div align="left">Email</div></th>
                    <th width="15%"><div align="left">Photo Caption</div></th>
                    <th width="15%"><div align="left">Report</div></th>
                    <th width="8%"><div align="left">Report Date</div></th>
                    <th width="8%"><div align="left">Delete</div></th>
                </thead>

                <?php
                if ($resultCount > 0) 
                {
                    $result = $db->query($sql);

                    $i = $pagesize * ($pageindex - 1);
                    while ($row = $resultB->fetch()) 
                    {
                        $i++;
                        if ($i % 2)
                            $bgcolor = $row_over_alternate_color;
                        else
                            $bgcolor = "#FBFCFC";
						
						$sql8="select username,email from jos_users where id='".$row['userid']."'";
						$result8 = mysql_query($sql8);
						$userinfo=mysql_fetch_assoc($result8);
						
                       
                        ?>
                       
                    <tbody>
                        <tr id="Row<?php echo $i ?>" bgcolor="<?php echo $bgcolor ?>" onMouseOver="this.bgColor = '<?php echo $row_over_color ?>'" onMouseOut="this.bgColor = '<?php echo $bgcolor ?>'" height="25px">
                            <td align="cente"><?php echo $i ?></td>
                            <td class="text-red" align='right'><?php echo $userinfo['username']; ?></td>
                            <td class="text-red" align='right'><?php echo $userinfo['email']; ?></td>
                            <td class="text-red" align='right'><?php echo ucfirst($row['caption']); ?></td>
                            <td class="text-red" align='right'><?php
                                $MSG = $row['msg'];
                                echo shortString($MSG, 80);
                                ?>
                            </td>
                            <td class="text-red" align='right'><?php echo FormatDate($row['dtdate']); ?></td>
                            <td class="text-red" align='right'>
                                <a onclick="return confirmDelete();" href="?id=<?php echo $row['id'] ?>&mode=delete"  ><img src="<?php echo base_path ?>images/dd.gif" /></a>
                            </td>
                        </tr>
                    </tbody>
                        
                <?php
                    }
                }else {
                    ?>
                    <tbody>
                        <tr>
                            <td colspan="10"><center>No record found.</center></td>
                    </tr>
                    </tbody>

                <?php } ?>
                <?php if ($resultCount > 0) { ?>
                    <tfoot>
                        <tr>
                            <td colspan="8"><div class="pagination">
                                    <div  class="align-left"><span>View</span>
                                        <select name="pagesize" onchange="Changepagesize('<?php echo $qStrPageSize ?>', this.value)">
                                            <?php
                                            $i = $PAGE_SIZE;
                                            while ($i <= $resultCount + $PAGE_SIZE) {
                                                if (($i % $PAGE_SIZE) == 0) {
                                                    if ($i == $pagesize) {
                                                        ?>
                                                        <option value="<?php echo($i); ?>" selected><?php echo($i); ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option value="<?php echo($i); ?>" ><?php echo($i); ?></option>
                                                        <?php
                                                    }
                                                }
                                                $i = $i + $PAGE_SIZE;
                                            } // end of for loop
                                            ?>
                                        </select>
                                        <abbr>Row(s) per page</abbr>
                                    </div>
                                    <div class="align-right">
                                        <?php
                                        $qry_string = $_SERVER['QUERY_STRING'];
                                        $url = $_SERVER['PHP_SELF'] . "?" . $qry_string;
                                        echo getPagingHtml($resultCount, $pagesize, $pageindex, $url)
                                        ?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tfoot>
                <?php } ?>
            </table>
        </div>


    </div>
</div>

<script langauge="javascript">

    function confirmDelete()
    {
        var agree = confirm("Are you sure you want to delete this?");
        if (agree)
            return true;
        else
            return false;
    }
    function Changepagesize(url, pagesize)
    {
        var url = url + "&pagesize=" + pagesize;
        window.location = url;
    }


</script>

<?php include("footer.php"); ?>
