<?php
$incr=1;
$globalUse	=	"";

function getEncrypt($plaintext, $salt = '',$show_encrypt = false){
	$salt = getSalt($salt, $plaintext);
	
	$encrypted = ($salt) ? md5($plaintext.$salt) : md5($plaintext);
	return ($show_encrypt) ? '{MD5}'.$encrypted : $encrypted;
}

function getSalt($seed = '', $plaintext = '')
	{
		$salt = '';
		if ($seed) {
			$salt = $seed;
		}
		return $salt;
	}
function checkAllowedName($uname)
{
	global $db;
	
	$restricted_nm = array("instanine","admin","ninehertz","insta","team");
	
	$uname = strtolower($uname);
	
	$allowed = true;
	
	foreach($restricted_nm as $rest)
	{
		$rest = strtolower($rest);
		if(strpos($uname,$rest)!==false)
		{
			$allowed = FALSE;
			
		}
	}
	//echo $allowed; die;
	return $allowed;

}
function specialCharRemove($text)
{
	$text = str_replace("<?", "", $text);
	$text = str_replace("<!", "", $text);
	return escapeChars($text);
}
function genRandomPassword($length = 8)
	{
		$salt = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$len = strlen($salt);
		$makepass = '';

		$stat = @stat(__FILE__);
		if(empty($stat) || !is_array($stat)) $stat = array(php_uname());

		mt_srand(crc32(microtime() . implode('|', $stat)));

		for ($i = 0; $i < $length; $i ++) {
			$makepass .= $salt[mt_rand(0, $len -1)];
		}

		return $makepass;
	}
	
function checkTableInDatabase()
{
	$sql	=	"CREATE TABLE IF NOT EXISTS `comment_collage` ( `id` int(10) NOT NULL AUTO_INCREMENT,`userid` int(10) NOT NULL, `albumid` int(10) NOT NULL,`comment` varchar(250) NOT NULL,`dtdate` datetime NOT NULL, PRIMARY KEY (`id`))";
	mysql_query($sql);
	return true;
}

function checkTableUserConfirm()
{
	$sql	=	"CREATE TABLE IF NOT EXISTS `user_confirm` ( `userid` int(10) DEFAULT NULL, `code` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL, `dtdate` datetime DEFAULT NULL )";
	mysql_query($sql);
	return true;
}

function checkTableLastSeen()
{
	$sql	=	"CREATE TABLE IF NOT EXISTS `last_seen` ( `id` int(8) NOT NULL AUTO_INCREMENT, `userid` int(8) NOT NULL, `totallastseen` int(6) NOT NULL, `lastseendate` date NOT NULL, PRIMARY KEY (`id`) )";
	mysql_query($sql);
	return true;
}

function checkAlbumDetailTable()
{
	$sql	=	"CREATE TABLE IF NOT EXISTS `albumdetail` (`albumid` int(10) NOT NULL,`userid` int(10) NOT NULL) ";
	mysql_query($sql);
	return true;
}

function userInfo($userid)
{
	$user	=	array();
	$sql="select JCU.avatar AS userimagebig, JCU.thumb as userimagethumb, JU.id as userid, JU.name,JU.username,JU.email,followpermission from jos_users AS JU left join jos_community_users AS JCU on JU.id=JCU.userid where JU.id='".$userid."'";
	$result = mysql_query($sql);
	if (mysql_num_rows($result) > 0)
	{
		$rs=mysql_fetch_assoc($result);
		
		$sqlma="select `value` from jos_community_fields_values where user_id ='".$userid."' AND field_id ='2' ";
		$resultma = mysql_query($sqlma);
		$rsma=mysql_fetch_assoc($resultma);
		$rs['gender']	=	strtoupper($rsma['value']);
		if($rs['userimagebig'] == '')
			$rs['userimagebig']		=	'images/default.png';
		if($rs['userimagethumb'] == '')
			$rs['userimagethumb']		=	'images/default.png';
		$user	=	$rs;
	}
	return $user;
}

function getUserId($username)
{
	$userid	=	0;
	$sql="select id as userid from jos_users where UPPER(username)='".strtoupper($username)."'";
	$result = mysql_query($sql);
	if (mysql_num_rows($result) > 0)
	{
		$rs=mysql_fetch_assoc($result);
		$userid	=	$rs['userid'];
	}
	return $userid;
}

function getTimeInfo($oldTime, $newTime, $timeType)
{	
	$timeCalc = strtotime($newTime) - strtotime($oldTime);        
	if ($timeType == "x") 
	{
		if ($timeCalc > 60) 
		{
			$timeType = "m";
		}
		if ($timeCalc > (60*60))
		 {
			$timeType = "h";
		}
		if ($timeCalc > (60*60*24)) 
		{
			$timeType = "d";
		}
	}        
	if ($timeType == "s") {
	   
	}
	if ($timeType == "m") {
		$timeCalc = round($timeCalc/60) . "m ago";
	}        
	elseif ($timeType == "h") {
		$timeCalc = round($timeCalc/60/60) . "h ago";
	}
	elseif ($timeType == "d") {
		$timeCalc = round($timeCalc/60/60/24) . "d ago";
	}else
	{
		 $timeCalc .= "s ago";
	}
	return $timeCalc;
}

function nowDateTime()
{
	return date("Y-m-d H:i:s");
}

function getAlbumdetial($albumid,$userid,$loginid){
	global $mainlink;
	
	//$marmition	=	" AND permissions = '0' ";
	//if($userid == $loginid)
		$marmition	=	"";
	
	$getalbum	=	"SELECT * FROM jos_community_photos_albums WHERE id='".$albumid."' ".$marmition." ";
	
	$result 	= 	mysql_query($getalbum);
	$arr=array();
	if(mysql_num_rows($result) > 0)
	while ($rs=mysql_fetch_assoc($result)){
			$userinfo	=	userInfo($rs['creator']);	
			
			$rs['userfullname']		=	$userinfo['name'];
			$rs['username']			=	$userinfo['username'];	
			$rs['userimagebig']		=	$userinfo['userimagebig'];
			$rs['userimagethumb']	=	$userinfo['userimagethumb'];
			$userid					=	$rs['creator'];
			
			//$rs['allow_follower']	=	1;
			$show	=	"YES";
			
			
			if($userid != $loginid)
			{
				if($rs['userid'] != $userid && $rs['permissions'] == 40) 
				{
					$selesql	=	mysql_query("select * from albumdetail where userid = '".$loginid."' AND albumid = '".$rs['id']."'");
					if(mysql_num_rows($selesql) <= 0)
					{
						$rs['allow_follower']	=	0;
						$show	=	"NO";
					}
				}
			}

			
		if($show == "YES"){
			$getcomment = getAlbumComment($albumid);
			$totcomment = totAlbumcomment($albumid);
			$rs['totallike']	=	getTotAlPhotoLike($albumid,$userid);
			$rs['likestatus']	=	getLikeStatus($userid,$albumid,'album');
			$rs['time_text']	=	getTimeInfo($rs['created'], date("Y-m-d H:i:s"), "x");
			$rs['totalbumcomment']	=$totcomment;
			$rs['albumcomment']	=	$getcomment;
			$rs['albumurl']		=	$mainlink.$rs['username'].'/photos/'.adddash($albumid);
			
				$arr[] =  $rs; 
		}		
	}

	return $arr;
	} 
function getAlbumPhotoImage($albumid)
{
	$getalbum	=	"SELECT CP.creator,CA.permissions,CP.image AS smalimage, CP.thumbnail, CP.original FROM jos_community_photos_albums AS CA inner join jos_community_photos CP on CA.photoid=CP.id WHERE CA.id='".$albumid."'";
	$result 	= 	mysql_query($getalbum);
	$arr=array();
		if(mysql_num_rows($result) > 0)
		{
			while ($rs=mysql_fetch_assoc($result))
			{
				$arr[] =  $rs; 
			}		
		}

	return $arr;
	} 	
function getAlbumUserId($albumid)
{
	
	$getalbum	=	"SELECT creator FROM jos_community_photos_albums WHERE id='".$albumid."'";
	$result 	= 	mysql_query($getalbum);
	
	$rs=mysql_fetch_assoc($result);
	return $rs['creator'];
} 

function getAlbumDetailNew($albumid)
{
	
	$getalbum	=	"SELECT * FROM jos_community_photos_albums WHERE id='".$albumid."'";
	$result 	= 	mysql_query($getalbum);
	
	$rs=mysql_fetch_assoc($result);
	return $rs;
} 
	
	
function getLastComment($postid){
		global $db;
		$arr=array();
		$limit	=	'';
		
		$limit	=	'LIMIT 3';
			
		$sql="select U.id as userid,U.name,U.username,C.comment, C.id as commentid from jos_community_wall C inner join jos_users U on C.post_by=U.id where C.contentid=".$postid." order by C.id DESC ".$limit;
		$result=mysql_query($sql);
		if (mysql_num_rows($result)>0){
			while ($rs=mysql_fetch_assoc($result)){
				$arr[]=$rs;
			}
		}
		return $arr;
	}
	function checkUserPermission($albumid,$userid){
		$sql	=	"select * from albumdetail where userid=".$userid." and albumid=".$albumid;
	
		$result = mysql_query($sql);
		if (mysql_num_rows($result)>0)
			return true;
		else
			return false;
	}
	
	function getUserAlbum($userid,$loginid=""){
	
	$status ="false";
	$msg	=	'';
	$arr=array();
	if ($userid<=0 || $userid=="")
		$msg="Invalid user or post";
	else{
		$marmition	=	"";
		if($loginid > 0)
		{
	//		$marmition	=	" AND A.permissions = '0' ";
			if($userid == $loginid)
				$marmition	=	"";
		}
		
		$sql	=	"SELECT A.id AS albumsid, A.photoid AS coverid, A.name AS albumsname, A.description AS aldescription, A.created AS dtdate, A.path AS albumpath, B.thumbnail,A.creator AS userid,A.permissions as per 
		             FROM jos_community_photos_albums A inner 
					 JOIN jos_community_photos B ON A.photoid = B.id  
					 WHERE A.creator = '".$userid."' ".$marmition." 
					 ORDER BY A.created DESC";
		//echo $sql;die();			 

		$result = mysql_query($sql);
		
		if (mysql_num_rows($result)>0){
			while ($rs=mysql_fetch_assoc($result)){

				if ($userid!=$loginid){
					if ($rs['per']==40){
						if (checkUserPermission($rs['albumsid'],$loginid))
							$arr[] =  $rs;
					}
					else
						$arr[] =  $rs;
				}
				else
						$arr[] =  $rs;
				}
				
			
			$msg	=	'Successfully';
			$status="true";
		}
		else
			$msg="No record found on this post";
	}
		
		return $arr;
	
	
	}
	
	
function getPhotoComment($photoid){
	
	$status ="false";
	$msg	=	'';
	$arr	=	array();
	if ($photoid<=0 || $photoid=="")
		$msg="Invalid user or post";
	else{
	//	$sql	=	"SELECT id AS commentid, contentid AS photoId, post_by AS creator, comment, date AS commentdate FROM jos_community_wall WHERE contentid = '".$photoid."' AND UPPER(type) = 'PHOTOS' ORDER BY date DESC LIMIT 3";
	$sql	=	"SELECT JW.id AS commentid, contentid AS photoId, post_by AS creator, comment, date AS commentdate FROM jos_community_wall JW inner join jos_users U on JW.post_by=U.id WHERE contentid = '".$photoid."' AND UPPER(type) = 'PHOTOS' ORDER BY date DESC LIMIT 3";

		$result = mysql_query($sql);
		
		if (mysql_num_rows($result)>0){
			while ($rs=mysql_fetch_assoc($result)){
				
			$userinfo	=	userInfo($rs['creator']);	
			
			$rs['name']	=	$userinfo['name'];	
			$rs['username']	=	$userinfo['username'];
			$rs['userimagebig']		=	$userinfo['userimagebig'];
			$rs['userimagethumb']	=	$userinfo['userimagethumb'];	
			$rs['dtdate']	=	getTimeInfo($rs['commentdate'], date("Y-m-d H:i:s"), "x");
			$arr[] =  $rs;
				
				}
			$msg	=	'Successfully';
			$status="true";
		}
		else
			$msg="No record found on this post";
	}
		
		return $arr;
	}
	function getAlbumComment($albumid){
	
	$status ="false";
	$msg	=	'';
	$arr	=	array();
	if ($albumid<=0 || $albumid=="")
		$msg="Invalid user or post";
	else{
		//$sql	=	"SELECT id AS commentid, contentid AS photoId, post_by AS creator, comment, date AS commentdate FROM jos_community_wall JW WHERE contentid = '".$albumid."' AND UPPER(type) = 'ALBUMS' ORDER BY date DESC LIMIT 3";
		$sql	=	"SELECT JW.id AS commentid, contentid AS photoId, post_by AS creator, comment, date AS commentdate FROM jos_community_wall JW inner join jos_users U on JW.post_by=U.id WHERE contentid = '".$albumid."' AND UPPER(type) = 'ALBUMS' ORDER BY date DESC LIMIT 3";

		$result = mysql_query($sql);
		
		if (mysql_num_rows($result)>0){
			while ($rs=mysql_fetch_assoc($result)){
				
			$userinfo	=	userInfo($rs['creator']);	
			
			$rs['name']	=	$userinfo['name'];	
			$rs['username']	=	$userinfo['username'];
			$rs['userimagebig']		=	$userinfo['userimagebig'];
			$rs['userimagethumb']	=	$userinfo['userimagethumb'];	
			$rs['dtdate']	=	getTimeInfo($rs['commentdate'], date("Y-m-d H:i:s"), "x");
			$arr[] =  $rs;
				
				}
			$msg	=	'Successfully';
			$status="true";
		}
		else
			$msg="No record found on this post";
	}
		
		return $arr;
	
	
	
	
	}
	function totAlbumcomment($albumid){
	
	$sql	=	"SELECT count(contentid) as totcomment FROM jos_community_wall WHERE contentid = '".$albumid."' AND UPPER(type) = 'ALBUMS'";

		$result = mysql_query($sql);
		
		$rs	=	mysql_fetch_assoc($result);
		if (!is_null($rs['totcomment']))
			$count=$rs['totcomment'];
		
		return "".intval($count)."";
	}	
	
	function getAlbumId($photoid){
	
	$sql	=	"SELECT albumid,creator,caption,permissions,image AS smalimage, thumbnail, original, parentid FROM jos_community_photos WHERE id = '".$photoid."' ";

		$result = mysql_query($sql);
		$count	=	array();
		$rs	=	mysql_fetch_assoc($result);
		if (intval($rs['albumid']) > 0)
		{
			$count['albumid']=$rs['albumid'];
			$count['creator']=$rs['creator'];
			$count['caption']=$rs['caption'];
			$count['adduserid']=$rs['creator'];
			$count['permissions']=$rs['permissions'];
			$count['smalimage']=$rs['smalimage'];
			$count['thumbnail']=$rs['thumbnail'];
			$count['original']=$rs['original'];
		}
		
		return $count;
		
	
	}	
	function getPhotoImage($photoid,$type='PHOTO'){
	

	if (strtoupper($type)=="ALBUM")
		$sql	=	"SELECT albumid,id,image,thumbnail,original FROM jos_community_photos WHERE id = (select photoid from jos_community_photos_albums where id=".$photoid.")";
	else
		$sql	=	"SELECT albumid,id,image,thumbnail,original FROM jos_community_photos WHERE id = '".$photoid."' ";

		$result = mysql_query($sql);
		$count	=	array();
		$rs	=	mysql_fetch_assoc($result);
		if (intval($rs['id']) > 0)
		{
			/*$count['image'] = (!empty($count['image']) ? $rs['image'] : "");
			
			$count['thumbnail'] = (!empty($count['thumbnail']) ? $rs['thumbnail'] : "");
			
			$count['original'] = (!empty($count['original']) ? $rs['original'] : "");
			*/
			///*
			$count['albumid'] = $rs['albumid'];
			if($rs['image'] == NULL || $rs['image'] == '')
				$rs['image']	=	'';
			else
				$count['image']=$rs['image'];
				
			if($rs['thumbnail'] == NULL || $rs['thumbnail'] == '')
				$rs['thumbnail']	=	'';
			else
				$count['thumbnail']=$rs['thumbnail'];
				
			if($rs['original'] == NULL || $rs['original'] == '')
				$rs['original']	=	'';
			else
				$count['original']=$rs['original'];
			//*/
			//$count['creator']=$rs['creator'];
		}
		
		return $count;
		
	
	}
	function getTotAlbum($userid){
	
	$sql	=	"SELECT id FROM jos_community_photos_albums WHERE photoid!=0 AND creator = '".$userid."'";

		$result = mysql_query($sql);
		
		return "".intval(mysql_num_rows($result))."";
		
	
	}	
	function getTotAlbumPhoto($albumid){
	
		$sql	=	"SELECT id FROM jos_community_photos WHERE albumid = '".$albumid."'";

		$result = mysql_query($sql);
		
		return "".intval(mysql_num_rows($result))."";
		
	
	}	
	function getPhotoPosition($albumid,$photoid){
		$sql	=	"SELECT id FROM jos_community_photos WHERE albumid = '".$albumid."' order by id asc";
		$result = mysql_query($sql);
		$photoposi	=	0;
		while($rs	=	mysql_fetch_assoc($result))
		{
			$photoposi++;
			if($rs['id'] == $photoid)
				return "".intval($photoposi)."";
		}
		return "0";
	}

	function getLikeStatus($userid,$photoid,$type){
		$sql	=	"SELECT id FROM jos_community_likes WHERE uid = '".$photoid."' AND UPPER(element) = '".strtoupper($type)."' AND `like` like '%".$userid."%' ";
		$result = mysql_query($sql);
		$likeStatus = intval(mysql_num_rows($result));
		// if ($likeStatus == 0)
		// {
		// 	$sql	=	"SELECT id FROM jos_community_likes WHERE uid = '".$photoid."' AND UPPER(element) = '".strtoupper($type)."' AND `dislike` like '%".$userid."%' ";
		// 	$result = mysql_query($sql);
		// 	$likeStatus = -intval(mysql_num_rows($result));
		// }
		return "".$likeStatus."";	
	}	

	function gettotcomment($photoid){
		$sql	=	"SELECT id FROM jos_community_wall WHERE contentid = '".$photoid."' AND UPPER(type) = 'PHOTOS'";
		$result = mysql_query($sql);
		return "".intval(mysql_num_rows($result))."";
	}
	function getTotLike($photoid,$type){
		$sql	=	"SELECT SUM( CHAR_LENGTH( `like` ) - CHAR_LENGTH( REPLACE( `like` , ',', '' ) ) +1 ) cnt FROM `jos_community_likes` WHERE uid = '".$photoid."' AND UPPER(element) = '".strtoupper($type)."' AND `like` != ''";
		$result = mysql_query($sql);
		$rs	=	mysql_fetch_assoc($result);
		if (!is_null($rs['cnt']))
			$count=$rs['cnt'];
		return "".intval($count)."";
	}	
	
	function getTotAlPhotoLike($albamid,$userid){
		$sql	=	"SELECT SUM( CHAR_LENGTH( `like` ) - CHAR_LENGTH( REPLACE( `like` , ',', '' ) ) +1 ) cnt FROM `jos_community_likes` WHERE uid in (select id from jos_community_photos where albumid = '".$albamid."' ) AND UPPER(element) = 'PHOTO' AND `like` != ''";
		$result = mysql_query($sql);
		$rs	=	mysql_fetch_assoc($result);
		if (!is_null($rs['cnt']))
			$count=$rs['cnt'];
		return "".intval($count)."";
	}
	
	function getTotDisLike($photoid,$type){
		return 0;
	}
	
	function getAllLikeAlbum($userid,$loginid = ''){
		$status ="false";
		$msg	=	'';
		$pageSize	=	15;
		$total_rows	=	0;
		$comment	=	array();	
		if ($userid<=0 || $userid=="")
			$msg="Invalid user or post";
		else{
			$photoid	=	"";
			$sqlall	=	mysql_query("select uid,`like` from jos_community_likes where 1 AND element ='photo' AND `like` != ''");
			$i	=	0;
			while($proid = mysql_fetch_assoc($sqlall))
			{
		//print_r($proid);
			$arr1=array();
		
		
		
			$uid	=	$proid['uid'];
			//echo $proid['like']."<br>";
			$arr=explode(",",($proid['like']));
			
			foreach($arr as $val){
				
				if ($val==$userid){$i++;
					//echo $val." ".$userid." ".$uid."<br>";
					if($i == 1)
						$photoid .=	$uid;
					else
						$photoid .=	",".$uid;
					
				}
			}
			//$arr['vendour']=(($proids['vendorlist_id']));
			//$arr1[]=$arr;
		
			}
			
			$orcon	=	"in ('-1')";
			if($i > 0)
			$orcon	=	" in (".$photoid.") ";
		
			//echo $sql	=	"SELECT A.id AS albumsid, A.photoid AS coverid, A.name AS albumsname, A.description AS aldescription, A.created AS dtdate, A.path AS albumpath, A.creator AS userid,B.thumbnail FROM jos_community_photos_albums A where id in(select distinct(albumid) from jos_community_photos where id ".$orcon.") ORDER BY A.created DESC";
			
			$marmition	=	"";
			if($loginid > 0)
			{
				$marmition	=	" AND A.permissions = '0' ";
				if($userid == $loginid)
					$marmition	=	"";
			}
			
			$sql	=	"SELECT A.id AS albumsid, A.photoid AS coverid, A.name AS albumsname, A.description AS aldescription, A.created AS dtdate, A.path AS albumpath, A.creator AS userid FROM jos_community_photos_albums A where id in(select distinct(albumid) from jos_community_photos where id ".$orcon.") ".$marmition." ORDER BY A.created DESC";

			$result = mysql_query($sql);
			
			if (mysql_num_rows($result)>0){
				while ($rs=mysql_fetch_assoc($result))
				{
					
					$sqlall1	=	mysql_query("select thumbnail from jos_community_photos where 1 AND id ='".$rs['coverid']."'");
					
					$proid = mysql_fetch_assoc($sqlall1);
					
					
					$totalLike	=	getTotAlPhotoLike($rs['albumsid'],$rs['userid']);
					if($totalLike > 0)
					{
						$rs['thumbnail']	=	$proid['thumbnail'];
						$rs['totallike']	=	$totalLike;
						$comment[] =  $rs;
						
					}
				}
				
			}
			else
				$msg="No record found on this post";
		}
		return $comment;
	}

function getTotLikedAlbum($userid){
	$status ="false";
	$msg	=	'';
	$pageSize	=	15;
	$total_rows	=	0;
	$totli	=	0;
	$comment	=	array();	
	if ($userid<=0 || $userid=="")
		$msg="Invalid user or post";
	else{
		$sql	=	"SELECT A.id AS albumsid, A.photoid AS coverid, A.name AS albumsname, A.description AS aldescription, A.created AS dtdate, A.path AS albumpath, A.creator AS userid,B.thumbnail FROM jos_community_photos_albums A inner JOIN jos_community_photos B ON A.photoid = B.id  WHERE A.creator = '".$userid."'  ORDER BY A.created DESC";

		$result = mysql_query($sql);
		
		if (mysql_num_rows($result)>0){
			while ($rs=mysql_fetch_assoc($result))
			{
				
				$totalLike	=	getTotAlPhotoLike($rs['albumsid'],$rs['userid']);
				if($totalLike > 0)
				{
					$totli++;
					
				}
			}
			
		}
		else
			$msg="No record found on this post";
	}
		return $totli;
}
function removeSlesh($string)
{
	  $string = str_replace ( "'\'", '', $string );
	  $string = str_replace ( "'\\'", '', $string );
	  $string = str_replace ( "n  ", '', $string );
	  $string	=	trim($string);
	  return $string;
}

function adddash($string)
{
	  $string = str_replace ( " ", '-', $string );
	  $string = str_replace ( "  ", '-', $string );
	  $string = str_replace ( "   ", '-', $string );
	  return $string;
}
function saveAlbumurl($newurl,$oldurl){
	if ($newurl =='' && $oldurl=="") {
		
		$msg	= 'Invalid url';
		
	}else{
		
		$sql	=	"INSERT INTO jos_redirection SET ".
					" cpt	=	'0', ".
					" rank	=	'0', ".
					" oldurl=	'".$oldurl."', ".
					" newurl=	'".$newurl."', ".
					" dateadd=	NOW()";
		
		$result = mysql_query($sql);
		}
	
	
	}
function saveAlbumurlphoto($newurlp,$oldurlp){
	if ($newurl =='' && $oldurl=="") {
		
		$msg	= 'Invalid url';
		
	}else{
		
		$sql	=	"INSERT INTO jos_redirection SET ".
					" cpt	=	'0', ".
					" rank	=	'0', ".
					" oldurl=	'".$oldurlp."', ".
					" newurl=	'".$newurlp."', ".
					" dateadd=	NOW()";
		
		$result = mysql_query($sql);
		}
	
	
	}
	function saveAlbumurlwall($newurlw,$oldurlw){
	if ($newurl =='' && $oldurl=="") {
		
		$msg	= 'Invalid url';
		
	}else{
		
		$sql	=	"INSERT INTO jos_redirection SET ".
					" cpt	=	'0', ".
					" rank	=	'0', ".
					" oldurl=	'".$oldurlw."', ".
					" newurl=	'".$newurlw."', ".
					" dateadd=	NOW()";
		
		$result = mysql_query($sql);
		}
	
	
	}	
function getCollageAlbumComment($albumid){
	
	$status ="false";
	$msg	=	'';
	$arr	=	array();
	if ($albumid<=0 || $albumid=="")
		$msg="Invalid user or post";
	else{
		$sql	=	"SELECT id AS commentid, albumid, userid, comment, dtdate  FROM comment_collage WHERE albumid = '".$albumid."'";

		$result = mysql_query($sql);
		
		if (mysql_num_rows($result)>0){
			while ($rs=mysql_fetch_assoc($result)){
				
			$userinfo	=	userInfo($rs['userid']);	
			
			$rs['name']	=	$userinfo['name'];	
			$rs['username']	=	$userinfo['username'];
			$rs['userimagebig']		=	$userinfo['userimagebig'];
			$rs['userimagethumb']	=	$userinfo['userimagethumb'];	
			$rs['dtdate']	=	getTimeInfo($rs['commentdate'], date("Y-m-d H:i:s"), "x");
			$arr[] =  $rs;
				
				}
			
		}
		else
			$msg="No record found on this post";
	}
		
		return $arr;
	}
	
function addNotification($userid,$mainid,$ref_id,$msg,$type)
{
	global $db;
	
		if ($type=='FOLLOW'){
			if($msg == 77)
			{
			$sql_del1 = "delete from notification_other WHERE userid ='".$mainid."' AND mainid ='".$userid."' AND type = '".$type."' and msg = '66' ";
				$result_del = $db->query($sql_del1);
			}
			$query	=   "SELECT * FROM notification_other WHERE userid ='".$userid."' AND mainid ='".$mainid."' AND type = '".$type."' AND msg = '' ";
			$result 	= $db->query($query);
			if($result->size()> 0){ 
				$sql_del = "delete from notification_other WHERE userid ='".$userid."' AND mainid ='".$mainid."' AND type = '".$type."' and  msg != '66'";
				$result_del = $db->query($sql_del);
				
				
			}
			else{
				$sql="insert into notification_other(userid,mainid,ref_id,msg,type,dtdate)VALUES(".
				"'".$userid."',".
				"'".$mainid."',".
				"'".$ref_id."',".
				"'".$msg."',".
				"'".$type."',".
				" '".nowDateTime()."'".
				")";	
				$db->query($sql);
			}
		}else{
		$sql="insert into notification_other(userid,mainid,ref_id,msg,type,dtdate)VALUES(".
		"'".$userid."',".
		"'".$mainid."',".
		"'".$ref_id."',".
		"'".$msg."',".
		"'".$type."',".
		" '".nowDateTime()."'".
		")";
		$db->query($sql);
		}
		//if($type="FOLLOW")
		//	followNotification($userid,$mainid,"FOLLOWING");
	
	return true;
}

function addPostNotification($userid,$mainid,$ref_id,$msg,$type,$posttype,$allow_follower=0,$creatorid=0)
{
	global $db;
	
	
	if ($type=="LIKE"){
		$query	=   "SELECT * FROM notification_post WHERE userid ='".$userid."' AND mainid ='".$mainid."' AND type = '".$type."' ";
		$result 	= $db->query($query);
		if($result->size()> 0) 
		{
			$row	=	$result->fetch();
			
			if($row['status'] > 0)
			{
				$sql	=	"UPDATE notification_post SET `dtdate` = '".nowDateTime()."',`allow_follower` = '".$allow_follower."', status = '0' WHERE userid ='".$userid."' AND mainid ='".$mainid."' AND type = '".$type."' ";
			}else
			{
				$sql	=	"UPDATE notification_post SET `dtdate` = '".nowDateTime()."',`allow_follower` = '".$allow_follower."',ref_id = '".$ref_id."', status = '1' WHERE userid ='".$userid."' AND mainid ='".$mainid."' AND type = '".$type."' ";
			}
			
			$db->query($sql);
		}else
		{
			$sql="insert into notification_post(userid,creatorid,allow_follower,mainid,ref_id,msg,type,posttype,dtdate)VALUES(".
			"'".$userid."',".
			"'".$creatorid."',".
			"'".$allow_follower."',".
			"'".$mainid."',".
			"'".$ref_id."',".
			"'".$msg."',".
			"'".$type."',".
			"'".$posttype."',".
			" '".nowDateTime()."' ".
			")";
	
			$db->query($sql);
			
			likeNotification($userid,$mainid);
		}
	}
	else{
		$sql="insert into notification_post(userid,creatorid,allow_follower,mainid,ref_id,msg,type,posttype,dtdate)VALUES(".
		"'".$userid."',".
		"'".$creatorid."',".
		"'".$allow_follower."',".
		"'".$mainid."',".
		"'".$ref_id."',".
		"'".$msg."',".
		"'".$type."',".
		"'".$posttype."',".
		" '".nowDateTime()."'".
		")";

		$db->query($sql);
		//likeNotification($userid,$mainid);
	}

	
	return true;
}

function addCommentNotification($userid,$mainid,$ref_id,$msg,$type,$posttype,$commentid=0)
{
	global $db;
	
	
		$sql="insert into notification_comment(userid,mainid,ref_id,msg,type,posttype,dtdate,commentid)VALUES(".
		"'".$userid."',".
		"'".$mainid."',".
		"'".$ref_id."',".
		"'".$msg."',".
		"'".$type."',".
		"'".$posttype."',".
		" '".nowDateTime()."',".
		"".$commentid."".
		")";

		$db->query($sql);
	
	return true;
}

function addMergeNotification($userid, $aluserid, $postid, $albumid)
{
	global $db;
	$sql="insert into notification_merge(userid, albumid, postid, dtdate)VALUES(".
		"'".$userid."',".
		"'".$albumid."',".
		"'".$postid."',".
		"'".nowDateTime()."'".
		")";

	$db->query($sql);
	postNotificationMerge($userid, $aluserid, $postid, $albumid);
	return true;
}

function getAlbumPermission($albumId){
		global $db;
		$sql	=	"select permissions from jos_community_photos_albums where id=".$albumId;
		$result = mysql_query($sql);
		
		if (mysql_num_rows($result)>0){
			$rs=mysql_fetch_assoc($result);
			return $rs['permissions'];
		}
		else
			return 0;

}

function checkFriendAccountActive($friend_id){
	global $db;
	$sql="select block from jos_users where id=".$friend_id;
	$result=$db->query($sql);
	if ($result->size()>0){
		$rs=$result->fetch();
		if ($rs['block']==1)
			return 0;
		else
			return 1;
	}
	else
		return 0;
}

function mackUserName($username)
{
	$oriName=$username;
	if($incr==0)
		$incr	+=	1;
	$sql="select id from jos_users where UPPER(username) = '".strtoupper($username)."'";
	$result = mysql_query($sql);
	if (mysql_num_rows($result) > 0)
	{
		$useremail		=	$useremail.$incr;
		mackUserName($username);
	}
	return $username;
}


function likeDislikePost($postid,$userid){
	$sql="select count(*) as cnt from jos_community_likes where uid in('".$postid."') and (find_in_set('".$userid."',`like`) or find_in_set('".$userid."',`dislike`)) ";
	//echo $sql; 
	$result = mysql_query($sql);
	if (mysql_num_rows($result) > 0){
		
		$rs=mysql_fetch_assoc($result);
		if($rs['cnt'] > 0){
			
			return "1";
		}
		return "0";
		//echo  $rs['cnt'];die();
		
	}
	return  "0";
	
	
}

function getUserFollowPermission($userid,$postuser,$albumid)
{
	global $db;
		$sql="select followpermission from jos_users WHERE id ='".$postuser."'";

		$query	=	$db->query($sql);
		if ($query->size() > 0)
		{
			$rs=$query->fetch();
			if($rs['followpermission'] > 0)
			{
				$sqlchk	=	"select * from albumdetail where userid=".$userid." and albumid=".$albumid;
				$resultc = mysql_query($sqlchk);
				if (mysql_num_rows($resultc)>0)
				{
					$sqlchk	=	"select * from view_friends where userid=".$postuser." and friend_id=".$userid;
					$resultc = mysql_query($sqlchk);
					if (mysql_num_rows($resultc)>0)
						return true;
				}
				else{
					$sqlchk	=	"select * from jos_community_connection where connect_to=".$postuser." and connect_from=".$userid." and status=1";
					$resultc = mysql_query($sqlchk);
					if (mysql_num_rows($resultc)>0)
						return true;
				}
			
			}else
				return true;
		}
		
	return false;
}

function send_mail($mailto, $mailubject, $mailbody, $mailfrom='') 
	{	
	$mailbody	=	stripslashes($mailbody);
	$mailfrom	=	"info@instanine.com";
	$fromname	=	"Instanine";
			
		$headers = "MIME-Version: 1.0" . "\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1" . "\n";

        $headers .= "X-Priority: 1 (Higuest)\n";
        $headers .= "X-MSMail-Priority: High\n";
        $headers .= "Importance: High\n";
		
		$headers .= "From: $fromname <$mailfrom>" . "\n";	
		$headers .= "Return-Path: <$mailfrom>" . "\n";
		$headers .= "Reply-To: <$mailfrom>";
		
		
		//echo "mail to : ".$mailto;
//		echo "<br>mail from : ".$mailfrom;
//		echo "<br>mail sub : ".$mailubject;
//		echo "<br>mail body : ".$mailbody;
//		echo "<br><br><br><br>";

		@mail($mailto, $mailubject, $mailbody, $headers);	
		
}	
?>