<?php
	include("config.php");
	include("image_functions.php");
	include("functions.php");
	include("classes/push-notification.class.php");
	include("classes/follow.class.php");
	
	///for add space by post method
	//ws-follow.php?type=add&data=[{"userid":"1","following_id":"2","status":"1"}]
	///ws-follow.php?type=ADDMULTIPLE&data=[{"userid":"1","following":"2","status":"1"}]
	///ws-follow.php?type=followerlist&data=[{"userid":"1"}]
	
	///ws-follow.php?type=followinglist&data=[{"userid":"1"}]
	///ws-follow.php?type=followlist&data=[{"loginid":"1","userid":"1"}]
	//web-services/ws-follow.php?type=followreuqestlist&data=[{"userid":"1"}]
	//web-services/ws-follow.php?type=accept&data=[{"userid":"1","connectionid":"1","followstatus":"0"}]
	//web-services/ws-follow.php?type=decline&data=[{"userid":"1","connectionid":"1"}]
	//http://mingleloop.com/qa/web-services/ws-follow.php?type=add&data=[{"userid":"880","status":"1","following_id":"952"}]
	//web-services/ws-user-profile.php?type=get&data=[{"userid":"880"}]
	//web-services/ws-follow.php?type=FOLLOW&data=[{"loginid":"880","userid":"880","followtype":"FOLLOWER"}]
	//followtype	=	FOLLOWER / FOLLOWING 
	//status	=	1/0

function getOtherUserProfile($loginid,$userid){
	global $db;
	$USER 	= new USER_CLASS;
	$FOLLOW	= new FOLLOW_CLASS;
	$userArray=array();
	$userPostArray	=	array();
	$status="false";
	if (($userid=="" && $userid<=0) || ($loginid=="" && $loginid<=0))
		$msg="Invalid Login id or User id";
	else
		$sql="select userid,is_private from users where userid=".$userid;
		
		$result=$db->query($sql);
		if ($result->size()>0){
			$rs=$result->fetchAsso();
			
					
			$userArray=	$USER->getUserInfo($userid);
			$userArray["is_private"] = $rs['is_private'];
			$userArray["total_follower"]=$USER->getTotalFollower($userid);
			$userArray["total_following"]=$USER->getTotalFollowing($userid);
			$userArray["total_post"]=$USER->getTotalPost($userid);
			if ($rs['is_private']<0){ /// profile is for only friend
				if ($FOLLOW->getUserStatus($loginid,$userid))	// Check these are friends
					$userArray["postdata"]=$USER->getAllPost($userid);
				else
					$userArray["postdata"]=array();
			}
			elseif ($rs['is_private']==1) {
				$userArray["postdata"]=array();
			}
			else
				$userArray["postdata"]=$USER->getAllPost($userid);

			$msg="Successfully";
			$status="true";
		}
		
		
		$arr=array("message"=>$msg,"detail"=>$userArray,"follow_status" =>"". $FOLLOW->getUserStatus($loginid,$userid)."","status"=>$status);
		return $arr;
		
}
function getFollowReuqest($userid){
	global $db;
	$userArray1=array();
	$status="false";
	$msg="";
	if ($userid=="" && $userid<=0)
		$msg="Invalid User id";
	else
		$sql="select connection_id,connect_from AS follower_id,connect_to AS following_id,status,created from jos_community_connection where status = '99' AND connect_to=".$userid;
		
		$result=$db->query($sql);
		if ($result->size()>0)
		{
			while($rs=$result->fetchAsso())
			{
				$userArray=	userInfo($rs['follower_id']);
				$rs['name']		=	$userArray['name'];
				$rs['username']	=	$userArray['username'];
				$rs['userimagebig']		=	$userArray['userimagebig'];
				$rs['userimagethumb']	=	$userArray['userimagethumb'];
				$userArray1[]	=	$rs;
			}
			$msg="Successfully";
			$status="true";
		}else
		$msg="Record not found.";
		
		
		$arr=array("message"=>$msg,"detail"=>$userArray1,"status"=>$status);
		return $arr;
		
}
function followReuqestAccept($userid,$connectionid){
	global $db;
	$status="false";
	$msg="";
	if ($userid <= 0 && $connectionid<=0)
		$msg="Invalid User id or connection id";
	else
	{
		$sql	=	" UPDATE jos_community_connection SET ".
					" status		=	'1'".
					" where connection_id	=".$connectionid;
			mysql_query($sql);
			
			
			
			$sqlse	=	"select * from jos_community_connection where connection_id='".$connectionid."'";
			$resse	=	$db->query($sqlse);
			$rows	=	$resse->fetch();
			
			$photocomm	=	"{actor} and {target} are now friends";
			$sql	=	"INSERT INTO jos_community_activities SET ".
						" actor			=	'".$userid."',".
						" target		=	'".$rows['connect_to']."',".
						" title			=	'".$photocomm."',".
						" app			=	'friends',".
						" created		=	'".nowDateTime()."'";
			
			mysql_query($sql);
			
			$sql_del = "delete from notification_other WHERE userid ='".$rows['connect_from']."' AND mainid ='".$rows['connect_to']."' AND type = 'FOLLOW' AND msg ='99' ";
			$db->query($sql_del);
			
			$sql_del = "delete from notification_other WHERE userid ='".$rows['connect_to']."' AND mainid ='".$rows['connect_from']."' AND type = 'FOLLOW' AND msg ='66' ";
			$db->query($sql_del);
			
			followNotification($rows['connect_from'],$rows['connect_to'],"FOLLOWING","666");	
			addNotification($rows['connect_to'],$rows['connect_from'],$rows['connect_from'],'66',"FOLLOW");		
		
		$msg="Successfully";
		$status="true";
		
	}
		$arr=array("message"=>$msg,"status"=>$status);
		return $arr;
		
}
function followReuqestDecline($userid,$connectionid){
	global $db;
	$status="false";
	$msg="";
	if ($userid <= 0 && $connectionid<=0)
		$msg="Invalid User id or connection id";
	else
	{
		mysql_query("DELETE FROM `jos_community_connection` WHERE `connection_id` = '".$connectionid."'");
		$msg="Successfully";
		$status="true";
		
	}
		$arr=array("message"=>$msg,"status"=>$status);
		return $arr;
		
}

function checkBlockUser($userid,$blockid){
	global $db;
	$status = false;
		$sql = "select * from jos_community_blocklist where userid=".$userid." and blocked_userid=".$blockid;

	$result = $db->query($sql);
	if ($result->size()>0)
		$status =true;
	unset($result);
	return $status;
}

$arr=array();
$FOLLOW = new FOLLOW_CLASS();
$FOLLOW->notify=true;



if (strtoupper($_REQUEST['type'])=="ADD"){
		$data=json_decode(str_replace("\\","",urldecode($_GET['data'])));
		if (!checkBlockUser($data[0]->following_id,$data[0]->userid))
			$arr=$FOLLOW->addFollow($data[0]);
		else
			$arr=array("message"=>"User blocked","status"=>"true");
			
		echo json_encode($arr);
}
if (strtoupper($_REQUEST['type'])=="ADDMULTIPLE"){
		//$data=json_decode(str_replace("\\","",$_GET['data']));
		//print_r($_POST);die;
		$arr=$FOLLOW->addMutipleFollow($_POST);
		echo json_encode($arr);
}
if (strtoupper($_REQUEST['type'])=="FOLLOWERLIST"){
		$data=json_decode(str_replace("\\","",urldecode($_GET['data'])));
		
		$arr=$FOLLOW->getFollowerList($data[0]);
		
		echo json_encode($arr);
} 
if (strtoupper($_REQUEST['type'])=="FOLLOWINGLIST"){
		$data=json_decode(str_replace("\\","",urldecode($_GET['data'])));
		$arr=$FOLLOW->getFollowingList($data[0]);
		echo json_encode($arr);
} 
if (strtoupper($_REQUEST['type'])=="FRIENDFOLLOWER"){
		$data=json_decode(str_replace("\\","",urldecode($_GET['data'])));
		$arr=$FOLLOW->getFollowerList($data[0],true);
		echo json_encode($arr);
} 
if (strtoupper($_REQUEST['type'])=="FRIENDFOLLOWING"){
		$data=json_decode(str_replace("\\","",urldecode($_GET['data'])));
		
		$arr=$FOLLOW->getFollowingList($data[0],true);
		echo json_encode($arr);
} 
if (strtoupper($_REQUEST['type'])=="FOLLOWLIST"){
		$data=json_decode(str_replace("\\","",urldecode($_GET['data'])));
		$userid = $data[0]->userid;
		$arr=$FOLLOW->getFollowList($userid);
		echo json_encode($arr);
} 

if (strtoupper($_REQUEST['type'])=="FOLLOW"){
		$data=json_decode(str_replace("\\","",urldecode($_GET['data'])));
		$loginid = $data[0]->loginid;
		$userid = $data[0]->userid;
		
		$followtype = $data[0]->followtype;
		
		if($followtype == "FOLLOWER")
		{
			if($loginid == $userid)
				$arr=$FOLLOW->getFollowerList($data[0],$loginid);
			else
				$arr=$FOLLOW->getFollowerList($data[0],$loginid,true);
		}
		else
		{
			if($loginid == $userid)
				$arr=$FOLLOW->getFollowingList($userid,$loginid);
			else
				$arr=$FOLLOW->getFollowingList($userid,$loginid,true);
		}

		echo json_encode($arr);
} 

if (strtoupper($_REQUEST['type'])=="GETOTHER"){
		$data		=	json_decode(str_replace("\\","",urldecode($_GET['data'])));
		$loginid	=	$data[0]->loginid;
		$userid		=	$data[0]->userid;
		$arr		=	getOtherUserProfile($loginid,$userid);
		
		echo json_encode($arr);
	}

if (strtoupper($_REQUEST['type'])=="FOLLOWREUQESTLIST"){
		$data		=	json_decode(str_replace("\\","",urldecode($_GET['data'])));
		$userid		=	intval($data[0]->userid);
		$arr		=	getFollowReuqest($userid);
		
		echo json_encode($arr);
	}

if (strtoupper($_REQUEST['type'])=="ACCEPT"){
		$data			=	json_decode(str_replace("\\","",urldecode($_GET['data'])));
		$userid			=	intval($data[0]->userid);
		$connectionid	=	intval($data[0]->connectionid);
		$followstatus	=	intval($data[0]->followstatus);
		if($followstatus > 0)
			$arr			=	followReuqestAccept($userid,$connectionid);
		else
			$arr			=	followReuqestDecline($userid,$connectionid);
			
		echo json_encode($arr);
	}
	
if (strtoupper($_REQUEST['type'])=="DECLINE"){
		$data			=	json_decode(str_replace("\\","",urldecode($_GET['data'])));
		$userid			=	intval($data[0]->userid);
		$connectionid	=	intval($data[0]->connectionid);
		$arr			=	followReuqestDecline($userid,$connectionid);
		
		echo json_encode($arr);
	}