<?php
define ("MAXMEM", 5*1024*1024);
ini_set('memory_limit', '-1');
//ini_set('max_execution_time', 3600);

//ini_set('upload_max_filesize', '100M');
//ini_set('post_max_size', '100M');
class UploadImage {
 
   var $image;
   var $image_type;
   public $validate=true;
   function load($filename) {
 	
      $image_info = getimagesize($filename);

      $this->image_type = $image_info[2];
      if( $this->image_type == IMAGETYPE_JPEG ) {
		  $this->image = imagecreatefromjpeg($filename);
      } elseif( $this->image_type == IMAGETYPE_GIF ) {
          $this->image = imagecreatefromgif($filename);
      } elseif( $this->image_type == IMAGETYPE_PNG ) {
          $this->image = imagecreatefrompng($filename);
      }
	  	else
			$this->validate=false;
   }
   function save($filename, $compression=95,$image_type=IMAGETYPE_JPEG, $permissions=null) {
      if( $image_type == IMAGETYPE_JPEG ) {
	  	imagejpeg($this->image,$filename,$compression);
      } elseif( $image_type == IMAGETYPE_GIF ) {
 
         imagegif($this->image,$filename);
      } elseif( $image_type == IMAGETYPE_PNG ) {
 
         imagepng($this->image,$filename);
      }
	  
      if( $permissions != null) {
 
         chmod($filename,$permissions);
      }
   }
   function output($image_type=IMAGETYPE_JPEG) {
 
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image);
      } elseif( $image_type == IMAGETYPE_GIF ) {
 
         imagegif($this->image);
      } elseif( $image_type == IMAGETYPE_PNG ) {
 
         imagepng($this->image);
      }
   }
   function getWidth() {
 
      return imagesx($this->image);
   }
   function getHeight() {
 
      return imagesy($this->image);
   }
   function resizeToHeight($height) {
 
      $ratio = $height / $this->getHeight();
      $width = $this->getWidth() * $ratio;
      $this->resize($width,$height);
   }
 
   function resizeToWidth($width) {
      $ratio = $width / $this->getWidth();
      $height = $this->getheight() * $ratio;
      $this->resize($width,$height);
   }
 
   function scale($scale) {
      $width = $this->getWidth() * $scale/100;
      $height = $this->getheight() * $scale/100;
      $this->resize($width,$height);
   }
 
   function resize($width,$height) {
      $new_image = imagecreatetruecolor($width, $height);
      imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
      $this->image = $new_image;
   }
   
   function enoughmem ($x, $y, $rgb=3) {
    return ( $x * $y * $rgb * 1.7 < MAXMEM - memory_get_usage() );
}

function TextOnImage($img,$image_type)
{

	if( $image_type == IMAGETYPE_JPEG ) {
		  $im = imagecreatefromjpeg($img);
      } elseif($image_type == IMAGETYPE_GIF ) {
          $im = imagecreatefromgif($img);
      } elseif($image_type == IMAGETYPE_PNG ) {
          $im = imagecreatefrompng($img);
      }

// Create the image

// Create some colors
$white = imagecolorallocate($im, 255, 255, 255);
$grey = imagecolorallocate($im, 128, 128, 128);
$black = imagecolorallocate($im, 0, 0, 0);
//imagefilledrectangle($im, 0, 0, 399, 29, $white);

// The text to draw
$text = 'Testing...';
// Replace path by your own font path
$font = 'fonts/monofont.ttf';

// Add some shadow to the text
imagettftext($im, 200, 0, 500, 500, $white, $font, $text);

// Add the text
////imagettftext($im, 100, 0, 500, 500, $black, $font, $text);

// Using imagepng() results in clearer text compared with imagejpeg()
if( $image_type == IMAGETYPE_JPEG ) {
	return imagejpeg($im,"a.jpg");
  } elseif( $image_type == IMAGETYPE_GIF ) {
	 return imagegif($im);
  } elseif( $image_type == IMAGETYPE_PNG ) {
	 return imagepng($im);
  }
imagedestroy($im);
}
     
function CreateWaterMark($img,$userdir)
	{
	$stamp = imagecreatefrompng('demo.png');
	$img_name=basename($img);
	$im = imagecreatefromjpeg($img);
	list ($ox,$oy)=@getimagesize($img);
	
	
	
	$marge_right = 500;
	$marge_bottom = 500;
	$sx = imagesx($stamp);
	$sy = imagesy($stamp);

	$w=round($ox/2);
	$h=round($oy/2);
	$stx=round($w/$sx * $sy);
	$sty=$h;

//	imagecopy($im, $stamp,imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));
	imagecopy($im, $stamp, 0, 0, 0, 0, imagesx($stamp), imagesy($stamp));

	// Output and free memory
	//header('Content-type: image/png');
	//echo $im;
	$target="{$userdir}{$img_name}";
	imagejpeg($im,$target,100);
	imagedestroy($im);
	imagedestroy($stamp);
	}

}

?>