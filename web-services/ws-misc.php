<?php
include("config.php");
include("functions.php");
	$dateformat="%m-%d-%Y";
	///for add space by post method
	
		//url :web-services/ws-misc.php?type=forgot&data=[{"email":"test"}]
		//url : web-services/ws-misc.php?type=change&data=[{"oldpass":"123456789","newpass":"123456789","userid":"867"}]


function escapeChars($value){
	if (is_array($value))
		$value = array_map('security', $value);
	else
		$value = mysql_real_escape_string($value);
	
	return $value;
}

function unEscapeChars($value){
	if (is_array($value))
		$value = array_map('security', $value);
	else
		$value = stripslashes($value);
	
	return $value;
}

function changePassword($userid,$pass,$oldpass){
	global $db;
	$status="false";
	$msg="";
	if ($userid =="" || $userid<=0 || $pass=="" || $oldpass=="")
		$msg="Invalid crediential";
	else
	{
		if ($oldpass!="1FORGOT"){
			$sql="select * from jos_users where id = '".$userid."'"; 
			$query	=	mysql_query($sql);
			if (mysql_num_rows($query)>0)
			{
				$rs		=	mysql_fetch_array($query);
				$parts	= explode( ':', $rs['password'] );
				$crypt	= $parts[0];
				$salt	= @$parts[1];
			
				$testcrypt 	=	getEncrypt($oldpass, $salt);
				$new_pass	=	getEncrypt($pass, $salt);
				$new_pass	=	$new_pass.':'.$salt; 
				if ($crypt == $testcrypt) {
					$sql="update jos_users set password='".$new_pass."' where id=".$userid;
					
					mysql_query($sql);
					$msg="Your password change successfully";
					$status="true";
				}
				else
					$msg="Enter valid old password.";
			}
			else
				$msg="Not a valid user id.";
		}else{
			$salt		=	genRandomPassword(32);
			$new_pass	=	getEncrypt($pass, $salt);
			$new_pass	=	$new_pass.':'.$salt; 
			$sql="update jos_users set password='".$new_pass."' where id=".$userid;
			mysql_query($sql);
			$msg="Your password change successfully";
			$status="true";
		}
	
	}
		$arr=array("message"=>$msg,"status"=>$status);
		return $arr;
}

function forgotPassword($username){
	global $db;
	$status="false";
	$msg="";
	
	$sql="select id,email from jos_users where UPPER(email)='".strtoupper($username)."'";
	$query	=	mysql_query($sql);
	
	if (mysql_num_rows($query)>0){
			$row	=	mysql_fetch_array($query);
			$userid=$row['id'];
			$pass		=	rand(0000000,9999999);
			if($row['email']!='')
			{
				changePassword($userid,$pass,"1FORGOT");
				$body ="Hello ". $row['name']."<br/><br/><br/>";
				$body .= "Your password has been reset and your new password is <br/><br/>";
				$body .=$pass."<br/><br/>";
				$body .="Thanks & Regards<br/>";
				$body .="Instanine Team<br/><br/>";
					
				send_mail($row['email'],"Password Info",$body,ADMIN_EMAIL);
				$msg="Your password has been changed. Please check your mail";
				$status="true";
			}
			else
				$msg="Email id does not exist.";
	}
	else
		$msg="Email is not valid";
		
		$arr=array("message"=>$msg,"userid"=>"".$userid."","status"=>$status);
		return $arr;
}

function updateBedge($deviceid){
	global $db;
	$status="false";
	if ($deviceid =="")
		$msg="Invalid credentials";
	else
	{
		$sql="update user_token set bedge=0 where deviceid='".$deviceid."'";
		$query	=	mysql_query($sql);
		if ($query)
		{
			$msg="Successfully";
			$status="true";
		}
	}
	$arr=array("message"=>$msg,"token"=>"".$deviceid."","status"=>$status);
	return $arr;
	
}

function deleteUser($userid){
	global $db;
	$status="false";
	if ($userid =="" || $userid<=0)
		$msg="Invalid credentials";
	else{
		$sql="delete  from user_token where user_id=".$userid;
		$query	=	mysql_query($sql);
		
		$sql1="delete  from jos_users where id=".$userid;
		$query1	=	mysql_query($sql1);
		if ($query1){
			$status="true";
			$msg  ="Successfully deleted";
		}
	}
	$arr=array("message"=>$msg,"userid"=>"".$userid."","status"=>$status);
	return $arr;
		
}

$arr=array();

if (strtoupper($_REQUEST['type'])=="FORGOT")
{
	$data	=	json_decode(str_replace("\\","",urldecode($_GET['data'])));
	$username	=	escapeChars($data[0]->email);
	$arr	=	forgotPassword($username);
	echo json_encode($arr);
}
if (strtoupper($_REQUEST['type'])=="CHANGE")
{
	$data		=	json_decode(str_replace("\\","",urldecode($_GET['data'])));
	$userid		=	escapeChars($data[0]->userid);
	$pass		=	escapeChars($data[0]->newpass);
	$oldpass	=	escapeChars($data[0]->oldpass);
	$arr		=	changePassword($userid,$pass,$oldpass);
	echo json_encode($arr);
}
if (strtoupper($_REQUEST['type'])=="RESET")
{
	$data		=	json_decode(str_replace("\\","",urldecode($_GET['data'])));
	$token		=	escapeChars($data[0]->token);
	$arr		=	updateBedge($token);
	echo json_encode($arr);
}
if (strtoupper($_REQUEST['type'])=="DELETE")
{
	$data		=	json_decode(str_replace("\\","",urldecode($_GET['data'])));
	$userid		=	escapeChars($data[0]->userid);
	$arr		=	deleteUser($userid);
	echo json_encode($arr);
}
