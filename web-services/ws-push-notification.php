<?php
include("config.php");
	include("image_functions.php");
	include("functions.php");



//web-services/ws-push-notification.php?type=set&data=[{"userid":"946","type":"[like,post,comment,follow]","staus":"0"}]

function escapeChars($value){
	if (is_array($value))
		$value = array_map('security', $value);
	else
		$value = mysql_real_escape_string($value);
	
	return $value;
}

function unEscapeChars($value){
	if (is_array($value))
		$value = array_map('security', $value);
	else
		$value = stripslashes($value);
	
	return $value;
}

function updateSetting($userid,$type,$st){
	global $db;
	$status="false";

	if ($userid!="" and $type!=""){
		if (strtoupper($type)=="LIKES")
			$sql="update user_token set `likes`=".$st." where user_id=".$userid;
		elseif (strtoupper($type)=="POST")
			$sql="update user_token set post=".$st." where user_id=".$userid;
		elseif (strtoupper($type)=="FOLLOW")
			$sql="update user_token set follow=".$st." where user_id=".$userid;
		elseif (strtoupper($type)=="COMMENT")
			$sql="update user_token set comment=".$st." where user_id=".$userid;
		elseif (strtoupper($type)=="FACEBOOK")
			$sql="update user_token set facebook=".$st." where user_id=".$userid;
		elseif (strtoupper($type)=="MENTION")
			$sql="update user_token set mention=".$st." where user_id=".$userid;
		
		$result = $db->query($sql);
		$status="true";
	}
	else
		$msg="Invalid action";
		
		$arr=array("message"=>$msg,"status"=>$status);
		return $arr;
		
}

function getSetting($userid){
	global $db;
	$status="false";
	$data=array();
	if ($userid!=""){
		$sql="select user_id,likes,post,comment,follow,facebook,mention from user_token where user_id=".$userid;
		$result =$db->query($sql);
		if ($result->size()>0){
			$rs=$result->fetch();
			$data[]=$rs;
			$status="true";
		}else{
			$rs['like']		=	"1";
			$rs['post']		=	"1";
			$rs['comment']	=	"1";
			$rs['follow']	=	"1";
			$rs['mention']	=	"1";
		}
	}
	else
		$msg="Invalid user id" ;
		
		$arr=array("message"=>$msg,"data"=>$data,"status"=>$status);
		return $arr;
}

$arr=array();

if (isset($_REQUEST['type']) && $_REQUEST['type']!=""){
	$data=array();
	if (strtoupper($_REQUEST['type'])=="SET"){
		$data			=	json_decode(str_replace("\\","",urldecode($_GET['data'])));

		$userid			=	intval($data[0]->userid);
		$type			=	escapeChars($data[0]->type);
		$status			= 	intval($data[0]->status);
		$arr=updateSetting($userid,$type,$status);
		echo json_encode($arr);
	}
	if (strtoupper($_REQUEST['type'])=="GET"){
		$data			=	json_decode(str_replace("\\","",urldecode($_GET['data'])));
		$userid			=	intval($data[0]->userid);
		$arr=getSetting($userid);
		echo json_encode($arr);
	}
}