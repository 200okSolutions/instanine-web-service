<?php
include("config.php");
include("functions.php");
include("classes/push-notification.class.php");
include("classes/follow.class.php");
	$dateformat="%m-%d-%Y";
//$URL=web-services/ws-comment.php?type=add&data=[{"userid":"869","photoid":"66","comment":"hello","posttype":"albums"}]

//web-services/ws-comment.php?type=get&data=[{"postid":"66","userid":"869","posttype":"albums"}]


function addComment($userid,$postid,$comment,$type,$hashtag,$inviteuser){
	global $db;
	$status ="false";
	
	if (($userid=="" || $userid<=0) || ($postid<=0 || $postid==""))
		$msg="Invalid user or post";
	else{
		
			
		if(strtoupper($type) == "COLLAGE")
		{
			$sql="insert into comment_collage (userid,albumid,comment,dtdate)VALUES(".
			"".$userid.",".
			"".$postid.",".
			"'".$comment."',".
			"'".nowDateTime()."')";
			mysql_query($sql);
			$commentid		=	mysql_insert_id();
			//commentNotification($userid,$postid,"RANK");
			$aluserid	=	getAlbumUserId($postid);
			addCommentNotification($userid,$postid,$aluserid,$comment,"COMMENT","COLLAGE",$commentid);
		}
		else
		{
			$sql="insert into jos_community_wall (post_by,contentid,comment,type,ip,published,date)VALUES(".
			"".$userid.",".
			"".$postid.",".
			"'".$comment."',".
			"'".$type."',".
			"'".$_SERVER["REMOTE_ADDR"]."',".
			"'1',".
			"'".nowDateTime()."')";
			mysql_query($sql);
			$commentid		=	mysql_insert_id();
			
			
			
			$liketype	=	'';
			if($type == "photos")
			{
				$photoInfo	=	getAlbumId($postid);
				$userInfo	=	userInfo($photoInfo['creator']);
				
				$perms	=	"photoid=".$postid."action=wall
wallid=".$commentid."
url=/instanine/".$userInfo['username']."/photos/".$photoInfo['albumid']."#photoid=".$postid;
				$liketype	=	'photos.wall.create';
				
				$titlecomm	=	"Comment on the photo <a href=_QQQ_/instanine/".$userInfo['username']."/photos/".$photoInfo['albumid']."#photoid=".$postid."_QQQ_>".$photoInfo['caption']."</a>";
			}
			elseif($type == "albums")
			{
				$perms	=	"photoid=".$postid."action=wall wallid=".$commentid." url=index.php?option=com_community&view=photos&task=album&albumid=".$postid."&userid=".$userid;
				$albumInfo	=	getAlbumDetailNew($postid);
				$liketype	=	'photos.wall.create';
				$titlecomm	=	"Comment on the album <a href=_QQQ_{url}_QQQ_>".$albumInfo['name']."</a>";
			}
			
			if($type == "photos" || $type == "albums")
			{
				$cid	=	$commentid;
				if($type == "photos")
				$cid	=	$postid;
				$sql1	=	"INSERT INTO jos_community_activities SET ".
							" actor			=	'".$userid."',".
							" title			=	'".$titlecomm."',".
							" content		=	'".$comment."',".
							" groupid		=	'0',".
							" eventid		=	'0',".
							" params		=	'".$perms."',".
							" app			=	'".$type."',".
							" cid			=	'".$cid."',".
							" comment_id	=	'".$commentid."',".
							" like_id		=	'".$commentid."',".
							" comment_type	=	'".$liketype."',".
							" like_type		=	'".$liketype."',".
							" created		=	'".nowDateTime()."'";
				
				
				
				mysql_query($sql1);
			}
			
			if($type == "photos")
			{
				$photoinfo	=	getAlbumId($postid);
				saveHashTagOnPhoto($postid,intval($photoinfo['albumid']),$userid,$hashtag,'PHOTOS');
				addCommentNotification($userid,$postid,intval($photoinfo['creator']),$comment,"COMMENT","PHOTO",$commentid);
			}
			else
			{
				$aluserid	=	getAlbumUserId($postid);
				saveHashTagOnPhoto('-1',$postid,$userid,$hashtag,'ALBUMS');
				addCommentNotification($userid,$postid,$aluserid,$comment,"COMMENT","ALBUM",$commentid);
			}
		}
		if(count($inviteuser) > 0)
		{
			if($type == "photos"){
				$str="PHOTO";
				$str1	=	"photo";
			}elseif(strtoupper($type) == "COLLAGE"){
				$str="COLLAGE";
				$str1	=	"rank board";
			}else{
				$str="ALBUM";
				$str1	=	"album";
			}
			foreach($inviteuser as $val)
			{
				addCommentNotification($userid,$postid,$val,$comment,"TAG",$str,$commentid);
			}
		}

		if($type == "photos") {
			$photoinfo = getAlbumId($postid);
			if ($photoinfo['creator'] != $userid)
				commentNotificationNew($userid,$postid,$commentid);
		}else
			commentNotificationNew($userid,$postid,$commentid);
		$status="true";
		$msg="Successfully Added comment on post";
	}
	$arr=array("message"=>$msg,"status"=>$status,"commentid"=>"".$commentid."");
	return $arr;
}
function escapeChars($value){
	if (is_array($value))
		$value = array_map('security', $value);
	else
		$value = mysql_real_escape_string($value);
	
	return $value;
}
function saveHashTagOnPhoto($photoid,$albumID,$userid,$hashArr,$type="PHOTOS"){
	global $db;
	if (count($hashArr)>0){
		foreach($hashArr as $val)
			{
			if($val	!=	'')
			{
				$val =str_replace("(","",$val);
				$val =str_replace(")","",$val);
				$sql1	=	" INSERT INTO photohashtag SET ".
				" photoid		=	'".$photoid."',".
				" userid		=	'".$userid."',".
				" albumid		=	'".$albumID."',".
				" hashtag		=	'".trim($val)."',".
				" type			=	'".$type."'";
				$db->query($sql1);
			}
		}
	}
}
function getAllComment($loginid,$postid,$userid,$type){
	global $db;
	$status ="false";
	$FOLLOW	=	new FOLLOW_CLASS;
	$msg	=	'';
	$comment = array();
	$latestcomment	=array();
	if ($postid<=0 || $postid=="" || $userid<=0 || $userid=="")
		$msg="Invalid post";
	else{
		if(strtoupper($type) == "COLLAGE")
		{
			$sql="select id as commentid, comment,dtdate, userid, albumid from comment_collage where albumid='".$postid."' ";
			$result = mysql_query($sql);
			if (mysql_num_rows($result)>0){
				while ($rs=mysql_fetch_assoc($result)){
					$userInfo			=	userInfo($rs['userid']);
					$rs['name']			=	$userInfo['name'];
					$rs['username']		=	$userInfo['username'];
					$rs['userimagebig']	=	$userInfo['userimagebig'];
					$rs['userimagethumb']	=	$userInfo['userimagethumb'];
					$rs['time_text']	=	getTimeInfo($rs['dtdate'], date("Y-m-d H:i:s"), "x");
					$comment[] =  $rs;
			
				}
				
				$msg	=	'Successfully';
				$status="true";
			}
			else
				$msg="No one comment found on this post";
		}else
		{
			$sql="select id as commentid, comment,date, post_by AS userid, contentid AS postid from jos_community_wall where contentid='".$postid."' AND type = '".$type."'";
			$result = mysql_query($sql);
			if (mysql_num_rows($result)>0){
				while ($rs=mysql_fetch_assoc($result)){
					$userInfo			=	userInfo($rs['userid']);
					$rs['name']			=	$userInfo['name'];
					$rs['username']		=	$userInfo['username'];
					$rs['userimagebig']	=	$userInfo['userimagebig'];
					$rs['userimagethumb']	=	$userInfo['userimagethumb'];
					$rs['time_text']	=	getTimeInfo($rs['date'], date("Y-m-d H:i:s"), "x");
					$comment[] =  $rs;
			
				}
				
				$msg	=	'Successfully';
				$status="true";
			}
			else
				$msg="No one comment found on this post";
		}
	}
		$arr=array("message"=>$msg,"data"=>$comment,"following"=>$FOLLOW->getFollowingListNew($loginid),"status"=>$status);
		return $arr;
}

$arr=array();

if (isset($_REQUEST['type']) && $_REQUEST['type']!=""){
	$data=array();
	
	if (strtoupper($_REQUEST['type'])=="ADD"){
		$data=json_decode(urldecode($_GET['data']));
		//echo json_encode($data);die;
		$userid		=	(trim($data[0]->userid));
		$postid		=	(trim($data[0]->photoid));
		$type		=	(trim($data[0]->posttype));
		$comment	=	specialCharRemove(trim($data[0]->comment));
		$hashtag	=	$data[0]->hashtag;
		$inviteuser	=	$data[0]->inviteuser;
		$arr=addComment($userid,$postid,$comment,$type,$hashtag,$inviteuser);
		echo json_encode($arr);
	}
	
	if (strtoupper($_REQUEST['type'])=="GET"){
		$data=json_decode(str_replace("\\","",urldecode($_GET['data'])));
		$postid		=	(trim($data[0]->postid));
		$userid		=	(trim($data[0]->userid));
		$loginid	=	intval(trim($data[0]->loginid));
		$type		=	(trim($data[0]->posttype));
		$arr=getAllComment($loginid,$postid,$userid,$type);
		//echo "<pre>";
		//print_r($arr);die;
		echo json_encode($arr);
	}
}

