<?php
define ("MAXMEM", 5*1024*1024);
ini_set('memory_limit', '-1');
class UploadImage {
 
   var $image;
   var $image_type;
   public $validate=true;
   function load($filename) {
 	
		if (!file_exists($filename)){
			exit;
		}
      $image_info = getimagesize($filename);
	 
      $this->image_type = $image_info[2];
	 
      if( $this->image_type == IMAGETYPE_JPEG ) {
		  $this->image = imagecreatefromjpeg($filename);
      } elseif( $this->image_type == IMAGETYPE_GIF ) {
          $this->image = imagecreatefromgif($filename);
      } elseif( $this->image_type == IMAGETYPE_PNG ) {
          $this->image = imagecreatefrompng($filename);
      } elseif( $this->image_type == IMAGETYPE_PNG ) {
          $this->image = imagecreatefrompng($filename);
      }
	  	else
			$this->validate=false;
   }
   
   function loadfile($filename) {
 	
		if (!file_exists($filename)){
			exit;
		}
      $image_info = getimagesize($filename);
      $this->image_type = $image_info[2];
	  
      if( $this->image_type == IMAGETYPE_JPEG ) {
		  $this->image = imagecreatefromjpeg($filename);
      } elseif( $this->image_type == IMAGETYPE_GIF ) {
          $this->image = imagecreatefromgif($filename);
      } elseif( $this->image_type == IMAGETYPE_PNG ) {
          $this->image = imagecreatefrompng($filename);
      }
	  	else
			$this->validate=false;
   }
   
   function save($filename, $compression=95,$image_type=IMAGETYPE_JPEG, $permissions=null) {
		
		
      if( $image_type == IMAGETYPE_JPEG ) {
	  	imagejpeg($this->image,$filename,$compression);
		
      } elseif( $image_type == IMAGETYPE_GIF ) {
 
         imagegif($this->image,$filename);
      } elseif( $image_type == IMAGETYPE_PNG ) {
 
         imagepng($this->image,$filename);
      }
      if( $permissions != null) {
 
         chmod($filename,$permissions);
      }

   }
   function output($image_type=IMAGETYPE_JPEG) {
 
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image);
      } elseif( $image_type == IMAGETYPE_GIF ) {
 
         imagegif($this->image);
      } elseif( $image_type == IMAGETYPE_PNG ) {
 
         imagepng($this->image);
      }
   }
   function getWidth() {
 
      return imagesx($this->image);
   }
   function getHeight() {
 
      return imagesy($this->image);
   }
   function resizeToHeight($height) {
 
      $ratio = $height / $this->getHeight();
      $width = $this->getWidth() * $ratio;
      $this->resize($width,$height);
   }
 
   function resizeToWidth($width) {
      $ratio = $width / $this->getWidth();
      $height = $this->getheight() * $ratio;
      $this->resize($width,$height);
	  
   }
 
   function scale($scale) {
      $width = $this->getWidth() * $scale/100;
      $height = $this->getheight() * $scale/100;
      $this->resize($width,$height);
   }
 
   function resize($width,$height) {
      $new_image = imagecreatetruecolor($width, $height);
      imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
      $this->image = $new_image;
   }
   
   function enoughmem ($x, $y, $rgb=3) {
    return ( $x * $y * $rgb * 1.7 < MAXMEM - memory_get_usage() );
}

    

}
?>