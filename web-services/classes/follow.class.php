<?php
class FOLLOW_CLASS{
	public $notify = false;
	private $user_follow = "jos_community_connection";
	public $follower_id = "connect_from";
	public $following_id = "connect_to";
	private $dtdate = "created";
	
	function followPermission($following_id){
		global $db;
		$flStatus=="0";
		$sqlper		=	"select followpermission from jos_users WHERE id ='".$following_id."'";
		$queryper	=	$db->query($sqlper);
		$rowper		=	$queryper->fetch();
		if ($rowper['followpermission']>0)
			$flStatus="0";
		else
			$flStatus="1";
		
		return $flStatus;
	}
	
	function addFollow1($rs){
		global $db;
		$status="false";
		
		$follower_id	=	intval(trim($rs->userid));
		$following_id	=	intval(trim($rs->following_id));
		$__status		=	intval(trim($rs->status));
		if ($follower_id=="") {
			$status="false";
			$msg	=	"User not found.";
		}
		else{
			
			if ($__status==1 || $__status==0){
				
					$flStatus=$this->followPermission($following_id);
					
					$sql="select * from $this->user_follow where $this->follower_id='".$follower_id."' AND $this->following_id=".$following_id;
					$res=$db->query($sql);
					
					if ($res->size()<=0)
					{
						$sql="insert into $this->user_follow($this->follower_id,$this->following_id,status,$this->dtdate)VALUES(".
						"'".$follower_id."',".
						"'".$following_id."',".
						"'".$__status."',".
						" '".nowDateTime()."'".
						")";
						$result=$db->query($sql);
						$followid		=	$result->insertID ();
						followNotification($following_id,$follower_id,"FOLLOWING",$__status);
						addNotification($follower_id,$following_id,$followid,'',"FOLLOW");
						$status="true";
						$msg ="Successfully added";
						
					}
			}else{
				
					$db->query("DELETE FROM albumdetail WHERE albumid in (select id from jos_community_photos_albums where creator ='".intval($following_id)."') AND userid ='".intval($follower_id)."'");
					
					$db->query("DELETE FROM notification_post WHERE mainid in (select id from jos_community_photos_albums where creator ='".intval($following_id)."') AND type in ('PRIVATE') AND userid ='".intval($follower_id)."'");
					
					$db->query("DELETE FROM notification_post WHERE mainid in (select id from jos_community_photos where creator ='".intval($following_id)."') AND type in ('LIKE') AND userid ='".intval($follower_id)."'");
					
					$db->query("DELETE FROM notification_comment WHERE ref_id ='".intval($following_id)."' AND type != 'TAG' AND userid ='".intval($follower_id)."'");
					
					$sqlupdatede	=	" DELETE FROM $this->user_follow WHERE $this->follower_id ='".intval($following_id)."' AND $this->following_id='".intval($follower_id)."'";
					$db->query($sqlupdatede);
					//addNotification($follower_id,$following_id,$followid,'',"FOLLOW");
					$status="true";
					$msg ="Successfully updated";
			}
			$arr=array("message"=>$msg,"status"=>$status);
			return $arr;
	}
}
	
	function addFollow($rs){
		global $db;
		$status="false";
		
		$follower_id	=	intval(trim($rs->userid));
		$following_id	=	intval(trim($rs->following_id));
		$status			=	intval(trim($rs->status));
		
			$sqlper		=	"select followpermission from jos_users WHERE id ='".$following_id."'";
			$queryper	=	$db->query($sqlper);
			$rowper		=	$queryper->fetch();
			
			if($rowper['followpermission'] > 0){
				if ($status==1)
					$status	=	"99";
			}
		if ($follower_id=="") {
			$status="false";
			$msg	=	"User not found.";
		}
		else{
			
			if($status==1)
			{
				$photocomm	=	"{actor} and {target} are now friends";
				$sql	=	"INSERT INTO jos_community_activities SET ".
							" actor			=	'".$follower_id."',".
							" target		=	'".$following_id."',".
							" title			=	'".$photocomm."',".
							" app			=	'friends',".
							" created		=	'".nowDateTime()."'";
				
				mysql_query($sql);
			}elseif($status==0)
			{
				$sqlComment2="delete from jos_community_activities where app ='friends' AND actor =".$follower_id." AND target =".$following_id;
				
				mysql_query($sqlComment2);
			}
				$sql="select * from $this->user_follow where $this->follower_id='".$follower_id."' AND $this->following_id=".$following_id;
				$res=$db->query($sql);
				if ($res->size()<=0)
				{	
					$sql="insert into $this->user_follow($this->follower_id,$this->following_id,status,$this->dtdate)VALUES(".
					"'".$follower_id."',".
					"'".$following_id."',".
					"'".$status."',".
					" '".nowDateTime()."'".
					")";
					$result=$db->query($sql);
					$followid		=	$result->insertID ();
					followNotification($following_id,$follower_id,"FOLLOWING",$status);
					if($this->notify)
					{
						if($status == "99")
						{
							addNotification($follower_id,$following_id,$followid,'99',"FOLLOW");
						}
						else
							addNotification($follower_id,$following_id,$followid,'',"FOLLOW");
					}
						
					$msg="You have successfully registered";
				}
				else
				{
					if($status > 0)
					{
					$row	=	$res->fetch();
						$sqlupdatede	=	" UPDATE $this->user_follow set status = ".$status."  WHERE $this->follower_id ='".intval($follower_id)."' AND $this->following_id='".intval($following_id)."'";
					//echo $sqlupdatede;
					$db->query($sqlupdatede);
					if($this->notify)
					{
						addNotification($follower_id,$following_id,$row['connection_id'],'77',"FOLLOW");
					}
					
					}else
					{
					
						$sqlupdatede	=	" DELETE FROM $this->user_follow WHERE $this->follower_id ='".intval($follower_id)."' AND $this->following_id='".intval($following_id)."'";
						$db->query($sqlupdatede);
						
						$db->query("DELETE FROM albumdetail WHERE albumid in (select id from jos_community_photos_albums where creator ='".intval($following_id)."') AND userid ='".intval($follower_id)."'");
					
					$sql="DELETE FROM notification_post WHERE ref_id ='".intval($following_id)."' AND type in ('PRIVATE') AND userid ='".intval($follower_id)."'";
					
					//$db->query($sql);
					
					//$db->query("DELETE FROM notification_post WHERE mainid in (select id from jos_community_photos where creator ='".intval($following_id)."') AND type in ('LIKE') AND userid ='".intval($follower_id)."'");
					
					//$db->query("DELETE FROM notification_comment WHERE ref_id ='".intval($following_id)."' AND type != 'TAG' AND userid ='".intval($follower_id)."'");
					
					
					
					
					$db->query("DELETE FROM albumdetail WHERE albumid in (select id from jos_community_photos_albums where creator ='".intval($follower_id)."') AND userid ='".intval($following_id)."'");
					
					$db->query("DELETE FROM notification_post WHERE ref_id ='".intval($follower_id)."' AND type in ('PRIVATE') AND userid ='".intval($following_id)."'");
					
					$db->query("DELETE FROM notification_post WHERE mainid in (select id from jos_community_photos where creator ='".intval($follower_id)."') AND type in ('LIKE') AND userid ='".intval($following_id)."'");
					
					$db->query("DELETE FROM notification_comment WHERE ref_id ='".intval($follower_id)."' AND type != 'TAG' AND userid ='".intval($following_id)."'");
					
					
						
						
						$sql_del = "delete from notification_other WHERE userid ='".intval($follower_id)."' AND mainid ='".intval($following_id)."' AND type = 'FOLLOW' and msg != '66' ";
						$db->query($sql_del);
						
						$sql_del1 = "delete from notification_other WHERE userid ='".$following_id."' AND mainid ='".$follower_id."' AND type = 'FOLLOW' and msg = '66' ";
				$result_del = $db->query($sql_del1);
					}
					$msg			=	"Updated successfully";
				}
				$status="true";
		}
			$arr=array("message"=>$msg,"status"=>$status);
			return $arr;
	}
	
	
	function addMutipleFollow($rs){
		global $db;
		$status="false";
		
		$follower_id	=	intval(trim($rs['userid']));
		
		
		$following		=	json_decode(stripslashes($rs['userlist']));
		//print_r($following);die;
		
		if ($follower_id=="") {
			$status="false";
			$msg	=	"User not found.";
		}
		else{
			foreach($following as $follow)
			{
				$following_id	=	intval($follow->userid);
				$status			=	intval($follow->followstatus);
				$sqlper		=	"select followpermission from jos_users WHERE id ='".$following_id."'";
				$queryper	=	$db->query($sqlper);
				$rowper		=	$queryper->fetch();
				
				if($rowper['followpermission'] > 0)
					$status	=	"99";
				
				if($following_id > 0)
				{
				$sql="select * from $this->user_follow where $this->follower_id='".$follower_id."' AND $this->following_id=".$following_id;
				
				$res=$db->query($sql);
				if ($res->size()<=0)
				{	
					$sql="insert into $this->user_follow($this->follower_id,$this->following_id,status,$this->dtdate)VALUES(".
					"'".$follower_id."',".
					"'".$following_id."',".
					"'1',".
					" NOW() ".
					")";
					$result=$db->query($sql);
					$followid		=	$result->insertID ();
					followNotification($following_id,$follower_id,"FOLLOWING",$status);
					if($this->notify)
					{
						addNotification($follower_id,$following_id,$followid,'',"FOLLOW");
					}
						
					$msg="You have successfully registered";
				}
				else
				{
					$row	=	$res->fetch();
						$sqlupdatede	=	" UPDATE $this->user_follow set status = ".$status."  WHERE $this->follower_id ='".intval($follower_id)."' AND $this->following_id='".intval($following_id)."'";
					$db->query($sqlupdatede);
					if($this->notify)
					{
						addNotification($follower_id,$following_id,$row['connection_id'],'',"FOLLOW");
					}
					$msg			=	"Updated successfully";
					}
				$status="true";
				}
			}
		}
			$arr=array("message"=>$msg,"status"=>$status);
			return $arr;
	}
	
	function getFollowerList($data,$loginid=0,$friend=false){
		global $db;
		$status				=	"false";
		$msg				=	'';
		$usercommntArray	=	array();
		//if($loginid > 0)
		//	$user_id		=	$loginid;
	//	else
		
			$user_id		=	intval(trim($data->userid));
		//$user_id		=	intval($data);
		
		if($user_id > 0)
		{
			
			//$sql="select * from user_follow AS UF where UF.following_id=".$user_id." and follower_id";
			$sql="select * from $this->user_follow  where status=1 AND connect_to=".$user_id."  and connect_from in(select id from jos_users where block=0)";

			$result=$db->query($sql);
			if ($result->size()>0)
			{
				while($rs=$result->fetchAsso())
				{
					
					$userInfo	=	userInfo($rs['connect_from']);
				
					if ($friend){
						if ($userInfo['userid']==$loginid){
							$userInfo["follow_status"]="-1";
						}
						else{
							$userInfo["follow_status"] 		=	$this->getUserStatus($rs['connect_from'],$loginid,"FOLLOWING");
						}
					}
					else{
						if($loginid == $rs['connect_from'])	
							$followerStatus		=	"-1";
						else
						if($loginid	==	$user_id)
							$followerStatus		=	$this->getUserStatus($user_id,$rs['connect_from'],"FOLLOWING");
						else
							$followerStatus		=	$this->getUserStatus($loginid,$rs['connect_from'],"FOLLOWING");
						
						
							$userInfo["follow_status"] = "".$followerStatus."";
					}
					
					$usercommntArray[]=$userInfo;
				}
				
				$status			=	"true";
				$msg			=	"Get successfully";
			}else
				$msg			=	"No record found.";
			
		}else
			$msg			=	'User not found.';
			
		$arr=array("message"=>$msg,"status"=>$status,"data"=>$usercommntArray);
		return $arr;
	}
	
	function getFollowingList($data,$loginid=0,$friend=false){
		global $db;
		$status				=	"false";
		$msg				=	'';
		$usercommntArray	=	array();
		
		//$user_id		=	intval(trim($data->userid));
		$user_id		=	intval(trim($data));
		
		if($user_id > 0)
		{
			//$sql="select * from user_follow AS UF where UF.connect_from=".$user_id." and UF.status=1";
			$sql="select * from $this->user_follow AS UF where UF.status > 0 AND UF.connect_from=".$user_id." and connect_to in(select id from jos_users where block=0)";
			
			
			$result=$db->query($sql);
			if ($result->size()>0)
			{
				while($rs=$result->fetchAsso())
				{
					
					$userInfo	=	userInfo($rs['connect_to']);
					if ($friend){
						if ($userInfo['userid']==$loginid){
							$userInfo["follow_status"]="-1";
						}
						else{
							$userInfo["follow_status"] = "".$this->getUserStatus($userInfo['userid'],$loginid,"FOLLOWING")."";
						}
					}else{
						$userInfo["follow_status"] = "".$this->getUserStatus($userInfo['userid'],$user_id,"FOLLOWING")."";
					}
					
					$usercommntArray[]=$userInfo;
				}
				
				
				$status			=	"true";
				$msg			=	"Get successfully";
			}else
				$msg			=	"No record found.";
			
		}else
			$msg			=	'User not found.';
			
		$arr=array("message"=>$msg,"status"=>$status,"data"=>$usercommntArray);
		return $arr;
	}
	
	function getFollowingListNew($data,$loginid=0,$friend=false){
		global $db;
		$status				=	"false";
		$msg				=	'';
		$usercommntArray	=	array();
		
		//$user_id		=	intval(trim($data->userid));
		$user_id		=	intval(trim($data));
		
		if($user_id > 0)
		{
			//$sql="select * from user_follow AS UF where UF.connect_from=".$user_id." and UF.status=1";
			$sql="select * from $this->user_follow AS UF where UF.connect_from=".$user_id." and UF.status=1 and connect_to in(select id from jos_users where block=0)";
			
			$result=$db->query($sql);
			if ($result->size()>0)
			{
				while($rs=$result->fetchAsso())
				{
					$userInfo	=	userInfo($rs['connect_to']);
					if ($friend){
						if ($userInfo['userid']==$loginid){
							$userInfo["follow_status"]="-1";
						}
						else{
							$userInfo["follow_status"] = "".$this->getUserStatus($loginid,$userInfo['userid'],"FOLLOWING")."";
						}
					}else{
						$userInfo["follow_status"] = "".$this->getUserStatus($user_id,$userInfo['userid'],"FOLLOWING")."";
					}
					
					$usercommntArray[]=$userInfo;
				}
				
				
				$status			=	"true";
				$msg			=	"Get successfully";
			}else
				$msg			=	"No record found.";
			
		}else
			$msg			=	'User not found.';
			
		//$arr=array("message"=>$msg,"status"=>$status,"data"=>$usercommntArray);
		return $usercommntArray;
	}
	
	function getFollowList($userid,$friend=false){
		$status = "false";
		$user_id		=	intval($userid);
				
		if($user_id > 0 || $userid!="")
		{
			$following 			= $this->getFollowingList($user_id,$friend);
			
			$data = (object) array("userid"=>$userid);	
			$follower			= $this->getFollowerList($data,$friend);
			$arr['follower'] 	= $follower['data'];
			$arr['following'] 	= $following['data'];
			$status = "true";
			$msg			=	"Get successfully";
		}
		else
			$msg			=	'User not found.';
			
		 
		
		$arr=array("message"=>$msg,"status"=>$status,"data"=>$arr);
		return $arr;
	}
	function getFollowListOther($userid,$loginid){
		$status = "false";
		$user_id		=	intval($userid);
		$data = (object) array("userid"=>$userid);
		if($user_id > 0 || $userid!="")
		{
			$following 			= $this->getFollowingList($user_id,$loginid,true);
			
			$follower			= $this->getFollowerList($data,$loginid,true);
			$arr['follower'] 	= $follower['data'];
			$arr['following'] 	= $following['data'];
			$status = "true";
		}
		else
			$msg			=	'User not found.';
			
		
		
		//$arr=array("message"=>$msg,"status"=>$status,"data"=>$arr);
		return $arr;
	}
	
	function getUserStatus($Loginid,$userid,$tp=""){
		global $db;
		$status=0;
		if ($tp=="FOLLOWING")
			$sql="select * from $this->user_follow where connect_to='".$Loginid."' and connect_from='".$userid."'"; 
		elseif ($tp=="FOLLOWER")
			$sql="select * from $this->user_follow where connect_to='".$Loginid."' and connect_from='".$userid."'";
		elseif ($tp=="BLOCK")
			$sql="select * from jos_community_blocklist where userid='".$Loginid."' and blocked_userid='".$userid."'";
		else
			$sql="select * from $this->user_follow where (connect_from='".$Loginid."' and connect_to='".$userid."') or (connect_to='".$Loginid."' and connect_from='".$userid."')";

		$result=$db->query($sql);
		if ($result->size()>0)
		{
			$rs=$result->fetch();
			if ($tp=="BLOCK")
				$status="1";
			else
				$status=$rs['status'];
		}
		return $status;
	}
	
	function getTotalFollower($userid){
		global $db;
		$count=0;
		$sql="select count(connect_from) as cnt from $this->user_follow where connect_to=".$userid." AND status = 1 and connect_from in(select id from jos_users where block=0)";
		
		$result=$db->query($sql);
		$rs=$result->fetch();
		if (!is_null($rs['cnt']))
			$count=$rs['cnt'];
		
		return $count;
	}

	function getTotalFollowing($userid){
		global $db;
		$count=0;
		$sql="select count(connect_to) as cnt from $this->user_follow where connect_from =".$userid." and status=1 and connect_to in(select id from jos_users where block=0)";
		$result=$db->query($sql);
		$rs=$result->fetch();
		if (!is_null($rs['cnt']))
			$count=$rs['cnt'];
		
		return $count;
	}
	
}
	
	

?>