<?php

		function sendAlertMsg($device_tokens,$msg1="", $merge=""){
		//$device_token1 	= '43693eb1a8562a909ca0a8d486bbc6a860afd82605bb7feb0bd3240f14cf4d5d';
		//$device_token 		= '8c801202f8a23830894af4389721334968da53d2e4284b47fe820d2e43b92031';
		$passphrase 	= '';
		$message = $msg1;
//		echo print_r($device_tokens);
		$ctx = stream_context_create();
		//stream_context_set_option($ctx, 'ssl', 'local_cert', '/home/rlegette/public_html/qa/web-services/classes/apns-dev.pem');
		//stream_context_set_option($ctx, 'ssl', 'local_cert', 'classes/apns-dev.pem');
		stream_context_set_option($ctx, 'ssl', 'local_cert', 'classes/apns-product.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		// Open a connection to the APNS server 
		// for live url
		$apns_url = 'gateway.push.apple.com';
		//$apns_url = 'gateway.sandbox.push.apple.com';
		$fp = stream_socket_client('ssl://'.$apns_url.':2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		
	
		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);
		
		//echo 'Connected to APNS' . PHP_EOL;
		
		// Create the payload body
		
		
		// Build the binary notification
		
		foreach($device_tokens as $device_token)
		{
			$body['aps'] = array(
			'alert' => $device_token['msg'],
			'sound' => 'default',
			'badge'=>updateBedge($device_token['deviceid']),
			'merge' => $merge
			);
	
			// Encode the payload as JSON
			$payload = json_encode($body);
			
			$device_token=str_replace(" ","",$device_token['deviceid']);
			$msg = chr(0) . pack('n', 32) . pack('H*', $device_token) . pack('n', strlen($payload)) . $payload;

			//$msg=pack("C", 1) . pack("N", $apple_identifier) . pack("N", $apple_expiry) . pack("n", 32) . pack('H*', str_replace(' ', '', $deviceToken)) . pack("n", strlen($payload)) . $payload;
			$result = fwrite($fp, $msg,strlen($msg));
		}


		
		/*
		$msg = chr(0) . pack('n', 32) . pack('H*', $device_token) . pack('n', strlen($payload)) . $payload;

			$result = fwrite($fp, $msg,strlen($msg));
		
		
		if (!$result)
			echo 'Message not delivered' . PHP_EOL;
		else
			echo 'Message successfully delivered' . PHP_EOL;
		exit;
		// Close the connection to the server
		*/
		fclose($fp);
	}
	
	function updateBedge($deviceID){
		global $db;
		$bedge=0;
		$sql="select user_id,bedge from user_token where UPPER(deviceid)='".strtoupper($deviceID)."'";
		$result=$db->query($sql);
		if ($result->size()>0){
			$rs=$result->fetch();
			$bedge = $rs['bedge']+1;
			$sql_updt = "update user_token set bedge=".$bedge." where user_id=".$rs['user_id'];
			$result_updt = $db->query($sql_updt);
		}
		else
			$bedge=1;
			
			return $bedge;
	}
	
	function checkSetting($userid,$type)
	{
		global $db;
		$sql="select user_id,likes,post,comment,follow,facebook,mention from user_token where user_id=".$userid;
		$result =$db->query($sql);
		if ($result->size()>0){
			$rs=$result->fetch();
			if (strtoupper($type)=="LIKE"){
				if ($rs['likes']==1)
					return true;
			}
			elseif (strtoupper($type)=="COMMENT"){
				if ($rs['comment']==1) 
					return true;
			}
		
			elseif (strtolower($type)=="follow"){
				if ($rs['follow']==1)
					return true;
			}
			
			elseif (strtolower($type)=="post"){
				if ($rs['post']==1)
					return true;
			}
			
			elseif (strtolower($type)=="facebook"){
				if ($rs['facebook']==1)
					return true;
			}
			elseif (strtolower($type)=="mention"){
				if ($rs['mention']==1)
					return true;
			}		
		}
		else
			return false;
	}
	
	
	function likeNotification($userid,$postid){
		global $db;
		$qry="select creator from jos_community_photos where id=".$postid;
		
		$result_qry=$db->query($qry);
		if ($result_qry->size()>0){
			$rs_qry=$result_qry->fetch();
			
			if ($rs_qry['creator'] == $userid)
				return;
			if (checkSetting($rs_qry['creator'],"LIKE")){
				
				$sql = "select deviceid from jos_users as JU inner join user_token as UT on JU.id=UT.user_id where UT.islogin='1' AND JU.id=".$rs_qry['creator'];

				$result = $db->query($sql);
				if ($result->size()>0){
					$rs=$result->fetch();
					$userdetail=userInfo($userid);
					
					$rs['msg']=$userdetail['username']." liked your post";
					//$msg = "Vijay started following you";
					//$msg="Vijay left a comment on your photo/album/rankboard";
					//$msg="Vijay left a comment on your photo/album/rankboard";
					$arr[]=$rs;

					sendAlertMsg($arr);
				}
			}
		}
	}
	
	function commentNotificationNew($userid,$postid,$commentid){
		global $db;
		$sql="select count(id) as cnt,ref_id,type,posttype from notification_comment where mainid=".$postid."  AND commentid = ".$commentid." group by ref_id order by count(id)";
		$result = $db->query($sql);
		if ($result->size()>0){
			while($rs=$result->fetch()){
				if (!checkSetting($rs['ref_id'],"COMMENT"))
				return false;
				$sql1="select deviceid from user_token where islogin='1' AND user_id=".$rs['ref_id'];
				$result1 = $db->query($sql1);
				if ($result1->size()>0){
					while($rs1=$result1->fetch()){
						$typetxt	=	"post";
						$text	=	" has mentioned you in a post comment";
						if($rs['posttype'] == "PHOTO")
						{
							$typetxt	=	"photo";
							$text	=	" has mentioned you in a comment";
						}
						elseif($rs['posttype'] == "COLLAGE")
						{
							$text	=	" has mentioned you in a rank board comment";
							$typetxt	=	"rank board";
						}
						
						$userdetail=userInfo($userid);
						$rs['cnt'];
						if ($rs['cnt']>=2){
							if($rs['posttype'] == "ALBUM")
								$typetxt	=	'';
							$rs1['msg'] 	=	$userdetail['username']." has mentioned you in a ".$typetxt." comment.";
							$arr[]			=	$rs1;
						}
						else{
							if ($rs['type']=="TAG"){
								
								$rs1['msg']=$userdetail['username'].$text.".";
								$arr[]			=	$rs1;
							}
							elseif ($rs['type']=="COMMENT"){
								$rs1['msg'] 	=	$userdetail['username']." has commented on your ".$typetxt.".";
								$arr[]			=	$rs1;
							}
						}
							
					}
				}
			}
			
		}
		sendAlertMsg($arr);
	}
	
	function commentNotification($userid,$postid,$subType){
		global $db;
		
		if (strtoupper($subType)=="PHOTOS")
			$qry="select creator from jos_community_photos where id=".$postid;
		elseif (strtoupper($subType)=="ALBUMS")
			$qry="select creator from jos_community_photos_albums where id=".$postid;
		elseif (strtoupper($subType)=="RANK")
			$qry="select creator from jos_community_photos_albums where id=".$postid;
		else
			return false;
		


		$result_qry=$db->query($qry);

		if ($result_qry->size()>0){
			$rs_qry=$result_qry->fetch();

			if (!checkSetting($rs_qry['creator'],"COMMENT"))
				return false;
				
				$sql = "select deviceid from jos_users as JU inner join user_token as UT on JU.id=UT.user_id where UT.islogin='1' AND JU.id=".$rs_qry['creator'];
 
				$result = $db->query($sql);
				if ($result->size()>0){
					$rs=$result->fetch();
					$userdetail=userInfo($userid);
					if (strtoupper($subType)=="PHOTOS")
						$msg=$userdetail['username']." left a comment on your photo.";
					elseif (strtoupper($subType)=="ALBUMS")
						$msg=$userdetail['username']." left a comment on your post.";
					elseif (strtoupper($subType)=="RANK")
						$msg=$userdetail['username']." left a comment on your rankboard.";


					//$msg = "Vijay started following you";
					//$msg="Vijay left a comment on your photo/album/rankboard";
					//$msg="Vijay left a comment on your photo/album/rankboard";
					$arr[]=$rs;
					
					sendAlertMsg($arr,$msg);
				}
			
			
		}
	}
	
	function postNotificationAlbum($mainid,$userid,$allow_follower = 0,$nonfolowerlist=array())
	{   
		global $db;
		$sql="select count(id) as cnt,ref_id,type,allow_follower from notification_post where mainid=".$mainid." group by ref_id order by count(id)";
		$result = $db->query($sql);
	
		if ($result->size()>0){
			while($rs=$result->fetch()){
				if (!checkSetting($rs['ref_id'],"post"))
				return false;
				
				$sql1="select deviceid from user_token where islogin='1' AND user_id=".$rs['ref_id'];
				$result1 = $db->query($sql1);
				
				if ($result1->size()>0){
					
					while($rs1=$result1->fetch()){
						
						$userdetail=userInfo($userid);
						$rs['cnt'];
						if ($rs['cnt']>=2){
							if (checkSetting($rs['ref_id'],"mention")){
								$rs1['msg'] 	=	$userdetail['username']." has mentioned you and invited you to a private Post.";
								$arr[]			=	$rs1;
							}
						}
						else{
							if ($rs['type']=="TAG"){
								if (checkSetting($rs['ref_id'],"mention")){
									$rs1['msg']=$userdetail['username']." mentioned you in a post.";
									$arr[]			=	$rs1;
								}
							}
							elseif ($rs['type']=="PRIVATE"){
								if($rs['allow_follower'] > 0)
									$rs1['msg'] 	=	$userdetail['username']." has invited you to add pictures to a private post.";
								else
									$rs1['msg'] 	=	$userdetail['username']." has invited you to a private post.";
								$arr[]			=	$rs1;
							}
						}
							
					}
				}
			}
			
		}
		if(count($arr) > 0)
			sendAlertMsg($arr);
			
		
		if(count($nonfolowerlist) > 0)
		{
			foreach($nonfolowerlist as $val)
			{
				$sql1="select deviceid from user_token where islogin='1' AND user_id=".intval($val);
				$result1 = $db->query($sql1);
				
				if ($result1->size()>0)
				{
					while($rs1=$result1->fetch())
					{
						$userdetail=userInfo($userid);
						
						$rs1['msg']=$userdetail['username']." mentioned you in a post.";
						$arr1[]			=	$rs1;
					}
					
					if(count($arr1) > 0)
						sendAlertMsg($arr1);
				}
			}
		}
	}
	
	function postNotificationPic($userid,$subType,$aluserid=0,$albumid=0)
	{
		global $db;
		if (strtoupper($subType)=="PHOTOS")
			$sql = "select deviceid from user_token where islogin='1' AND user_id = '".$aluserid."'";
		elseif (strtoupper($subType)=="ALBUM")
		{
			//if($aluserid == 0)
				$sql = "select deviceid from user_token where islogin='1' AND user_id in (select connect_to from jos_community_connection WHERE connect_from = '".$userid."' and status=1)";
			//else
			//	$sql = "select deviceid from user_token where user_id in (select userid from albumdetail WHERE albumid = '".$albumid."')";
		}
		else
			return true;
			
		$result = $db->query($sql);
		if ($result->size()>0)
		{
			while($rs=$result->fetch())
			{
				$userdetail=userInfo($userid);
				if (strtoupper($subType)=="PHOTOS")
					$rs['msg']=$userdetail['username']." added a picture to your post.";
				elseif (strtoupper($subType)=="ALBUM")
				{
					//if($aluserid == 0)
						$rs['msg']=$userdetail['username']." added new album.";
					//else
						//$msg=$userdetail['username']." has invited you for viewing/adding pictures to a private post";
				}
				$arr[]=$rs;
			}
			sendAlertMsg($arr);
		}
	}

	function postNotificationMerge($userid, $aluserid, $postid, $albumid)
	{
		global $db;
		$sql = "select deviceid from user_token where islogin='1' AND user_id = '".$aluserid."'";
		$result = $db->query($sql);

		if ($result->size()>0)
		{
			while($rs = $result->fetch())
			{
				$userdetail = userInfo($userid);
				$rs['msg'] = $userdetail['username']." merged his photo with your photo.";
				$arr[] = $rs;
			}
			$merge = "u=".$aluserid."&a=".$albumid."&p=".$postid;
			sendAlertMsg($arr, "", $merge);
		}
	}
	
	function signUpNotification($userid,$fbidget)
	{
		global $db;
			$sql = "select deviceid from user_token where islogin='1' AND facebook = '1' AND user_id in (select id from jos_users WHERE fb_id in(".$fbidget."))";
			
			
		$result = $db->query($sql);
		if ($result->size()>0)
		{
			$userdetail=userInfo($userid);
			while($rs=$result->fetch())
			{
				$rs['msg']="Your facebook friend ".$userdetail['name']." joined instanine as ".$userdetail['username'].".";
				$arr[]=$rs;
			}
				
			sendAlertMsg($arr);
			
		}
	}
	
	function followNotification($userid,$loginid,$subType,$status = 0)
	{
		global $db;
		$sql = "select deviceid from user_token where islogin='1' AND user_id  = '".$userid."'";
		
		$result = $db->query($sql);
		if ($result->size()>0)
		{
			
			if (!checkSetting($userid,"follow"))
				return false;
			$userdetail=userInfo($loginid);
			while($rs=$result->fetch())
			{ 
				if (strtoupper($subType)=="FOLLOW")
				{
					
						$rs['msg']=$userdetail['username']." started follow you.";
				}
				elseif (strtoupper($subType)=="FOLLOWING")
				{
					if($status == 99)
						$rs['msg']=$userdetail['username']." has requested to follow you.";
					elseif($status == 666)
						$rs['msg']=$userdetail['username']." has accepeted your follow request.";
					else
						$rs['msg']=$userdetail['username']." started following you.";
				}
					
				$arr[]=$rs;
				
			}
			sendAlertMsg($arr);
			
		}
	}
?>