<?php
function GetMonthNo($monthname)
	{
		switch ($monthname) {
			case "Jan":
				return "01";
				break;
			case "Feb":
				return "02";
				break;
			case "Mar":
				return "03";
				break;
			case "Apr":
				return "04";
				break;
			case "May":
				return "05";
				break;
			case "Jun":
				return "06";
				break;
			case "Jul":
				return "07";
				break;
			case "Aug":
				return "08";
				break;
			case "Sep":
				return "09";
				break;
			case "Oct":
				return "10";
				break;
			case "Nov":
				return "11";
				break;
			case "Dec":
				return "12";
				break;
			}
	}
function sqlDate($date){
	$dtDate=explode("-",$date);
	$dtDate=$dtDate[2]."-".$dtDate[0]."-".$dtDate[1];
	return $dtDate;
}
function FormatDate($date)
{
	global $date_format;
	return date($date_format, strtotime($date));
}

function FormatDateMonth($date)
{
	global $date_format_month;
	return date($date_format_month, strtotime($date));
}
function FormatTime($date)
{
	global $time_format;
	return date($time_format, strtotime($date));
}

function FormatDateTime($date)
{
	global $date_time_format;
	return date($date_time_format, strtotime($date));
}

function FormatNumber($val)
{
	//string number_format ( float $number , int $decimals , string $dec_point , string $thousands_sep )
	return number_format($val,2,'.',',');
}

function dateAdd($tp,$interval,$today){

	if (empty($today))
		$today_day		=	date("Y-m-d");
	else
		$today_day		=	$today;
		switch ($tp) {
			case "d":
				$by=$interval." days";
				break;
			case "m":
				$by=$interval." month";
				break;
			case "y":
				$by=$interval." year";
				break;
			case "w":
				$by=$interval." week";
				break;
		}
	$date=strtotime(date("Y-m-d", strtotime($today_day)) . $by);
	$date=date('Y-m-d',$date);
	return $date;
}

function make_page($total_items, $items_per_page, $p)
{
				if (($total_items % $items_per_page) != 0)
				{
								$maxpage = ($total_items) / $items_per_page + 1;
				}
				else
				{
								$maxpage = ($total_items) / $items_per_page;
				}
				$maxpage = (int)$maxpage;
				if ($maxpage <= 0)
				{
								$maxpage = 1;
				}
				if ($p > $maxpage)
				{
								$p = $maxpage;
				} elseif ($p < 1)
				{
								$p = 1;
				}
				$start = ($p - 1) * $items_per_page;
				return Array($start, $p, $maxpage);
}

function randomcode($len = "6")
{
				$code = NULL;
				for ($i = 0; $i < $len; $i++)
				{
								$char = chr(rand(48, 122));
								while (!ereg("[a-zA-Z0-9]", $char))
								{
												if ($char == $lchar)
												{
																continue;
												}
												$char = chr(rand(48, 90));
								}
								$pass .= $char;
								$lchar = $char;
				}
				return $pass;
}

function randomNum($len = "6")
{
				$code = NULL;
				for ($i = 0; $i < $len; $i++)
				{
								$char = chr(rand(48, 122));
								while (!ereg("[0-9]", $char))
								{
												if ($char == $lchar)
												{
																continue;
												}
												$char = chr(rand(48, 90));
								}
								$pass .= $char;
								$lchar = $char;
				}
				return $pass;
}


function is_email_address($email)
{
				$regexp = "/^[a-z0-9]+([_\\.-][a-z0-9]+)*@([a-z0-9]+([\.-][a-z0-9]+)*)+\\.[a-z]{2,}$/i";
				if (!preg_match($regexp, $email))
				{
								return false;
				}
				else
				{
								return true;
				}
}

function validate_username($v_username)
{
   if(eregi('[^a-zA-Z0-9_]', $v_username))
   {
      // The username contains an inavlid character
      return FALSE;
   }
   else
   {
      return TRUE;
   } 
}

if (!function_exists('str_ireplace'))
{
				function str_ireplace($search, $replace, $subject)
				{
								$search = preg_quote($search, "/");
								return preg_replace("/" . $search . "/i", $replace, $subject);
				}
}
if (!function_exists('htmlspecialchars_decode'))
{
				function htmlspecialchars_decode($text, $ent_quotes = "")
				{
								$text = str_replace("&quot;", "\"", $text);
								$text = str_replace("&#039;", "'", $text);
								$text = str_replace("&lt;", "<", $text);
								$text = str_replace("&gt;", ">", $text);
								$text = str_replace("&amp;", "&", $text);
								return $text;
				}
}
if (!function_exists('str_split'))
{
				function str_split($string, $split_length = 1)
				{
								$count = strlen($string);
								if ($split_length < 1)
								{
												return false;
								} elseif ($split_length > $count)
								{
												return array($string);
								}
								else
								{
												$num = (int)ceil($count / $split_length);
												$ret = array();
												for ($i = 0; $i < $num; $i++)
												{
																$ret[] = substr($string, $i * $split_length, $split_length);
												}
												return $ret;
								}
				}
}
function security($value)
{
				if (is_array($value))
				{
								$value = array_map('security', $value);
				}
				else
				{
								if (!get_magic_quotes_gpc())
								{
												$value = htmlspecialchars($value, ENT_QUOTES);
								}
								else
								{
												$value = htmlspecialchars(stripslashes($value), ENT_QUOTES);
								}
								$value = str_replace("\\", "\\\\", $value);
				}
				return $value;
}



/// Pagging funtion 

function getPagingHtml($count, $limit, $currentPage, $url)
{
	if ($count > 0) {
	$p;
	$maxDisplayedPages = 7;
	//var n = 1;
	
	$strPaging = "";
	$doneDots = false;
	$lastPage = ceil($count / $limit);
	$firstDisplayedPage = min($lastPage - ($maxDisplayedPages - 2), max($currentPage - ($maxDisplayedPages / 2) + 1, 1));
	$lastDisplayedPage;
	if($firstDisplayedPage == 1)
		$lastDisplayedPage = min($maxDisplayedPages - 1, $lastPage);
	else
		$lastDisplayedPage = min($firstDisplayedPage + $maxDisplayedPages - 2, $lastPage);
	
	if ($currentPage > 1){
		$strPaging.="<a href='".$url."&pageindex=1'><img src='images/pre2.png' border='0' /></a>&nbsp;"; 
		$strPaging.="<a href='".$url."&pageindex=".($currentPage-1)."'><img src='images/pre.png' border='0' /></a>&nbsp;"; 
		
	}
	else{
		
		$strPaging.="&nbsp;";
	}
	for($p = 1; $p <= $lastPage; $p++)
	{
		if ($currentPage == $p)
		{
			$strPaging.= "<span class='text-red'><b>$p</b></span> ";
		}
		else
		{
			if($p == 1 || $p == $lastPage || ($p >= $firstDisplayedPage && $p <= $lastDisplayedPage))
			{
				$strPaging.= "<a class='page' href='".$url."&pageindex=".$p."'>$p</a> ";
				$doneDots = false;
			}
			else if(!$doneDots)
			{
				$strPaging.= "<span class='page'>... </span>";
				$doneDots = true;
			}
		}
	}	
	
	if ($currentPage < ($count / $limit))
	{ 
		$strPaging.= "<a href='".$url."&pageindex=".($currentPage + 1)."'><img src='images/next.png' border='0' /></a>"; 
		
		$strPaging.= "<a href='".$url."&pageindex=".($lastPage)."'><img src='images/next2.png' border='0' /></a>"; 
	}
	else
	{
		$strPaging.= "&nbsp;";
	}
	}
	return $strPaging;
}

function getFrontEndPagingHtml($count, $limit, $currentPage, $url)
{
	if ($count > 0) {
	$p;
	$maxDisplayedPages = 7;
	//var n = 1;
	
	$strPaging = "";
	$doneDots = false;
	$lastPage = ceil($count / $limit);
	$firstDisplayedPage = min($lastPage - ($maxDisplayedPages - 2), max($currentPage - ($maxDisplayedPages / 2) + 1, 1));
	$lastDisplayedPage;
	if($firstDisplayedPage == 1)
		$lastDisplayedPage = min($maxDisplayedPages - 1, $lastPage);
	else
		$lastDisplayedPage = min($firstDisplayedPage + $maxDisplayedPages - 2, $lastPage);
	
	if ($currentPage > 1){
		$strPaging.="<a href='".$url."&pageindex=1'><img src='images/pre2.png' border='0' /></a>&nbsp;"; 
		$strPaging.="<a href='".$url."&pageindex=".($currentPage-1)."'><img src='images/pre.png' border='0' /></a>&nbsp;"; 
		
	}
	else{
		
		$strPaging.="&nbsp;";
	}
	for($p = 1; $p <= $lastPage; $p++)
	{
		if ($currentPage == $p)
		{
			$strPaging.= "<span class='text-red'><b>$p</b></span> ";
		}
		else
		{
			if($p == 1 || $p == $lastPage || ($p >= $firstDisplayedPage && $p <= $lastDisplayedPage))
			{
				$strPaging.= "<a class='page' href='".$url."&pageindex=".$p."'>$p</a> ";
				$doneDots = false;
			}
			else if(!$doneDots)
			{
				$strPaging.= "<span class='page'>... </span>";
				$doneDots = true;
			}
		}
	}	
	
	if ($currentPage < ($count / $limit))
	{ 
		$strPaging.= "<a href='".$url."&pageindex=".($currentPage + 1)."'><img src='images/next.png' border='0' /></a>"; 
		
		$strPaging.= "<a href='".$url."&pageindex=".($lastPage)."'><img src='images/next2.png' border='0' /></a>"; 
	}
	else
	{
		$strPaging.= "&nbsp;";
	}
	}
	return $strPaging;
}

function create_tag($string)
{
	  $string = str_replace ( '&amp;', '&', $string );
	  $string = str_replace ( '&#039;', '\'', $string );
	  $string = str_replace ( '&quot;', '"', $string );
	  $string = str_replace ( '&lt;', '<', $string );
	  $string = str_replace ( '&gt;', '>', $string );
	  $string = str_replace ( '&uuml;', '?', $string );
	  $string = str_replace ( '&Uuml;', '?', $string );
	  $string = str_replace ( '&auml;', '?', $string );
	  $string = str_replace ( '&Auml;', '?', $string );
	  $string = str_replace ( '&ouml;', '?', $string );
	  $string = str_replace ( '&Ouml;', '?', $string );   
	  return $string;
}

function getSecureFormat($str)
{
	$result="";
	for ($i=0; $i<strlen($str); $i++)
	 	$result.="*";	
	
	return $result;
}

function MonthDays($someMonth, $someYear)    
{        
	return date("t", strtotime($someYear . "-" . $someMonth . "-01"));    
}

function shortString($str, $num=50)
{
	if (strlen($str)>$num)
		return substr($str,0,$num)."...";
	else
		return substr($str,0,$num);
}

function IsInt($int){
       
        // First check if it's a numeric value as either a string or number
        if(is_numeric($int) === TRUE){
           
            // It's a number, but it has to be an integer
            if((int)$int == $int){

                return TRUE;
               
            // It's a number, but not an integer, so we fail
            }else{
           
                return FALSE;
            }
       
        // Not a number
        }else{
       
            return FALSE;
        }
		


}

function time_diffrence($starttime,$endtime,$auction_days){

	$diff 		= 	(strtotime($endtime) - strtotime($starttime)); 
	if($diff>0)
	{
	$years   	= 	floor($diff / (365*60*60*24)); 
	$months  	= 	floor(($diff - $years * 365*60*60*24) / (30*60*60*24)); 
	$days    	= 	floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
	$hours    	= 	floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24- $days*60*60*24 )/ (60*60));
	$mintues   	= 	floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24- $days*60*60*24 - $hours*60*60 )/ (60));
	$seconds   	= 	floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24- $days*60*60*24 - $hours*60*60- $mintues*60 ));
		//$years2	=	intval($years);
		//$months2	=	intval($months);
		//$days2	=	intval($days);
		if($years==0 && $months==0 && $days==0)
		{
			if($hours<10)
				$hours	=	'0'.$hours;
			if($mintues<10)
				$mintues	=	'0'.$mintues;
			if($seconds<10)
				$seconds	=	'0'.$seconds;
			$dh	=	$hours.":".$mintues.":".$seconds;
			
				
		}else
		{
			$days	=	floor($diff/(60*60*24));	
			$dh		= 	$days." ".$auction_days ;
	
		}
	}else
		$dh='CLOSE';
	
	return $dh;

}
?>