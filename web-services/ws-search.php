<?php
	include("config.php");
	include("image_functions.php");
	include("functions.php");

	///for add space by post method
	
	// web-services/ws-search.php?type=search&data=[{"userid":"880","search_text":"ra","type":"NAME"}]
	// web-services/ws-search.php?type=searchtag&data=[{"userid":"880","search_text":"app"}]
	//type  = NAME  / HASHTAG



function escapeChars($value){
	if (is_array($value))
		$value = array_map('security', $value);
	else
		$value = mysql_real_escape_string($value);
	
	return $value;
}

function unEscapeChars($value){
	if (is_array($value))
		$value = array_map('security', $value);
	else
		$value = stripslashes($value);
	
	return $value;
}

function getData($userid,$search_text,$type)
{
	global $db;
	$userArray=	array();
	$msg=	'';
	$status="false";
	if (($userid=="" || $userid<=0))
		$msg="Invalid User Id";
	else{
			if(strtoupper($type) == "NAME")
			{
			$sql		=	"select JCU.avatar AS userimagebig, JCU.thumb as userimagethumb, JU.id as userid, JU.name,JU.username,JU.email from jos_users AS JU left join jos_community_users AS JCU on JU.id=JCU.userid where (UPPER(JU.name) like '%".strtoupper($search_text)."%' or UPPER(JU.username) like '%".strtoupper($search_text)."%') AND JU.id not in(select userid from jos_community_blocklist where blocked_userid=".$userid.") and JU.block=0 ORDER BY CASE WHEN JU.username LIKE '".strtoupper($search_text)."' THEN 0
              WHEN JU.username LIKE '".strtoupper($search_text)."%' THEN 1
			  WHEN JU.username LIKE '%".strtoupper($search_text)."%' THEN 2
			  WHEN JU.name LIKE '".strtoupper($search_text)."' THEN 3
			  WHEN JU.name LIKE '".strtoupper($search_text)."%' THEN 4
			  WHEN JU.name LIKE '%".strtoupper($search_text)."%' THEN 5
              ELSE 6
         END ";
				
				$result		=	$db->query($sql);
				if($result->size()>0)
				{
					while($rs			=	$result->fetch())
					{
						if($rs['userimagebig'] == '')
							$rs['userimagebig']		=	'images/default.png';
						if($rs['userimagethumb'] == '')
							$rs['userimagethumb']		=	'images/default.png';
							
						$userArray[]	=	$rs;
					}
					$msg="Successfully";
					$status="true";
				}else
					$msg="Users not found.";
				
			}else
			{
				
				$getphoto	=	"select hashtag,type,userid,albumid from photohashtag PT where TYPE IN ('PHOTOS', 'ALBUMS') AND UPPER(hashtag) like '".strtoupper($search_text)."%' AND (permission = '0' or userid='".$userid."' or albumid in(select albumid from albumdetail where userid=".$userid.")) AND albumid not in(select id FROM jos_community_photos_albums WHERE photoid = 0)  and userid in(select id from jos_users where block=0) and userid not in(select userid from jos_community_blocklist where blocked_userid=".$userid.") group by trim(hashtag) ORDER BY trim(hashtag) ASC";
		
				
				
				$result2 = mysql_query($getphoto);
				$check	=	0;
				if(mysql_num_rows($result2) > 0)
				{ 
					while ($rs2=mysql_fetch_assoc($result2))
					{
					
						$totalpost	=	getTotalHashPost($userid,$rs2['hashtag']);
						if($totalpost > 0)
						{
							$check++;
							$rs2['totalpost']	=	"".$totalpost."";
							$userArray[] 		=  $rs2;
						}
					}
					if($check > 0)
					{
						$msg="Successfully";
						$status="true";
					}

				}
				else
					$msg ="Hash Tag not found";
			}
		}
	
		$arr=array("message"=>$msg,"detail"=>$userArray,"status"=>$status);
		return $arr;
		
}



function getTotalHashPost($userid,$search_text){
	global $db;
	$totalpost	=	0;
	
	
	$getphoto	=	"select * from photohashtag PT where UPPER(hashtag) = '".strtoupper($search_text)."' AND (permission = '0' or userid = '".$userid."' or albumid in(select albumid from albumdetail where userid=".$userid.")) and userid in(select id from jos_users where block=0) and userid not in(select userid from jos_community_blocklist where blocked_userid=".$userid.")";
	$result2 = mysql_query($getphoto);
	if(mysql_num_rows($result2) > 0)
	{
		$countarr	=	0;
		while ($rs2=mysql_fetch_assoc($result2))
		{
			$show	=	"YES";
			if($rs2['type'] == "PHOTOS")
			{
				$photoInfo	=	getAlbumId($rs2['photoid']);
			}else
			{
				$photoInfo	=	getAlbumPhotoImage($rs2['albumid']);
				
				$photoInfo	=	$photoInfo[0];
				if($photoInfo['smalimage'] == '')
					$show	=	"NO";
				
			}
			$postuser	=	getAlbumUserId($rs2['albumid']);
			$confirm	=	getUserFollowPermission($userid,$postuser,$rs2['albumid']);
			
			if (!$confirm){
				if($photoInfo['creator'] != $userid && $photoInfo['permissions'] == 40) 
					$show	=	"NO";
				
				if($confirm == false)
					$show	=	"NO";
				
			}else
				$show	=	"YES";
			
			if($postuser == $userid)
				$show	=	"YES";
				
				
			if($show ==	"YES")
			{
				$totalpost++;
				
			}
		}
	}
					
	return $totalpost;
}

function getHashTag($userid,$search_text){
	global $db;
	$status="false";
	$msg="";
	$arr=array();
	$totalpost	=	0;
	
	
	$getphoto	=	"select * from photohashtag PT where UPPER(hashtag) = '".strtoupper($search_text)."' AND (permission = '0' or userid = '".$userid."' or albumid in(select albumid from albumdetail where userid=".$userid.")) and userid in(select id from jos_users where block=0) and userid not in(select userid from jos_community_blocklist where blocked_userid=".$userid.")";
	$result2 = mysql_query($getphoto);
	if(mysql_num_rows($result2) > 0)
	{
		$countarr	=	0;
		while ($rs2=mysql_fetch_assoc($result2))
		{
			$show	=	"YES";
			if($rs2['type'] == "PHOTOS")
			{
				$photoInfo	=	getAlbumId($rs2['photoid']);
			}else
			{
				$photoInfo	=	getAlbumPhotoImage($rs2['albumid']);
				
				$photoInfo	=	$photoInfo[0];
				if($photoInfo['smalimage'] == '')
					$show	=	"NO";
				
			}
			$postuser	=	getAlbumUserId($rs2['albumid']);
			$confirm	=	getUserFollowPermission($userid,$postuser,$rs2['albumid']);
			
			if (!$confirm){
				if($photoInfo['creator'] != $userid && $photoInfo['permissions'] == 40) 
					$show	=	"NO";
				
				if($confirm == false)
					$show	=	"NO";
				
			}else
				$show	=	"YES";
				
			if($postuser == $userid)
				$show	=	"YES";
				
			if($show ==	"YES")
			{
				$totalpost++;
				$countarr++;
				if($photoInfo['smalimage'] == NULL)
					$rs2['smalimage']	=	'';
				else
					$rs2['smalimage']		=	$photoInfo['smalimage'];
				if($photoInfo['thumbnail'] == NULL)
					$rs2['thumbnail']	=	'';
				else
					$rs2['thumbnail']		=	$photoInfo['thumbnail'];
				if($photoInfo['original'] == NULL)
					$rs2['original']	=	'';
				else
					$rs2['original']		=	$photoInfo['original'];
				if($photoInfo['creator'] == NULL)
					$rs2['userid']		=	'0';
				else	
					$rs2['userid']		=	$photoInfo['creator'];
				$rs2['totalcomment']	=	getTotComment($rs2['photoid'],$userid);
				$rs2['totallike']		=	getTotLike($rs2['photoid'],$userid,'photo');
				$rs2['likestatus']		=	getLikeStatus($userid,$rs2['photoid'],'photo');
				$rs2['photocaption']	=	removeSlesh($rs2['photocaption']);
				if($rs2['status'] == NULL)
				$rs2['status']	=	'';
				$arr[] =  $rs2;
				
				$msg="Successfully";
				$status="true";
			}
		}
		if($countarr == 0)
		$msg="You are not authorized to view this post.";
	}
	else
		$msg ="Hash Tag not found";
					
	
		
		$arr=array("message"=>$msg,"data"=>$arr,"totalpost"=>"".$totalpost."","status"=>$status);
		return $arr;
}

$arr=array();

if (strtoupper($_REQUEST['type'])=="SEARCH")
{
	$data			=	json_decode(str_replace("\\","",urldecode($_GET['data'])));
	$userid			=	intval($data[0]->userid);
	$search_text	=	escapeChars($data[0]->search_text);
	$type			=	escapeChars($data[0]->type);
	
	
	$arr			=	getData($userid,$search_text,$type);
	
			
	echo json_encode($arr);
}

if (strtoupper($_REQUEST['type'])=="SEARCHTAG")
{
	$data			=	json_decode(str_replace("\\","",urldecode($_GET['data'])));
	$userid			=	intval($data[0]->userid);
	$search_text	=	escapeChars($data[0]->search_text);
	
	$arr			=	getHashTag($userid,$search_text);
			
	echo json_encode($arr);
}

