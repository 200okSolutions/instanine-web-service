<?php
include("config.php");
include("functions.php");
include("classes/push-notification.class.php");

$dateformat="%m-%d-%Y";

function tag($userid,$string)
{
	$string = str_replace ( $userid.',', '', $string );
	$string = str_replace ( ",".$userid, '', $string );
	$string = str_replace ( $userid, '', $string );
	return $string;
}
	
function likePost($userid,$photoid,$likeStatus,$posttype){
	$status ="false";
	$msg	=	'';
	if ($userid<=0 || $userid==""){
		$msg="Invalid user or post";
	}else{
		$query = "SELECT * FROM jos_community_likes WHERE element ='".$posttype."' AND uid ='".$photoid."' ";
		$result = mysql_query($query);

		if(mysql_num_rows($result)> 0) 
		{
			$row	=	mysql_fetch_array($result);
			$ref_id = 	$row['id'];
			$like	=	explode(",",$row['like']);
			$dislike	=	explode(",",$row['dislike']);
			$key	=   array_search( $userid, $like  );
			$diskey	=   array_search( $userid, $dislike  );

			if($likeStatus == "0")
			{
				if ($key !== false) {
					unset( $like [$key] );
					$likeget	=	implode( ',', $like  );
					$sql	=	"UPDATE jos_community_likes SET `like` = '".$likeget."' WHERE element ='".$posttype."' AND uid ='".$photoid."'";
					$result = mysql_query($sql);
				}
				
				if ($diskey === false) {
					    if ($dislike[0] == '')
					{
						$val	=	$userid;
						$sql	=	"UPDATE jos_community_likes SET `dislike` = '".$val."' WHERE element ='".$posttype."' AND uid ='".$photoid."'";
						$result = mysql_query($sql);
					}else{
						$val	=	$row['dislike'].",".$userid;
						$sql	=	"UPDATE jos_community_likes SET `dislike` = '".$val."' WHERE element ='".$posttype."' AND uid ='".$photoid."'";	
						$result = mysql_query($sql);
					}
					
				}
				
				
			}else {
				if ($key === false) {
					if ($like[0] == '')
					{
						$val	=	$userid;
						$sql	=	"UPDATE jos_community_likes SET `like` = '".$val."' WHERE element ='".$posttype."' AND uid ='".$photoid."'";
						$result = mysql_query($sql);
					}else {
						$val	=	$row['like'].",".$userid;
						$sql	=	"UPDATE jos_community_likes SET `like` = '".$val."' WHERE element ='".$posttype."' AND uid ='".$photoid."'";	
						$result = mysql_query($sql);
					}
				}
			}
		}else {
			//$sql	=	"INSERT INTO jos_community_likes SET element = '".$posttype."', uid = '".$photoid."', `like` = '".$userid."'";
			//$result = mysql_query($sql);
			//$ref_id = mysql_insert_id();
			
			//for like
			
			if($likeStatus == "1"){
				  $sql	=	"INSERT INTO jos_community_likes SET element = '".$posttype."', uid = '".$photoid."', `like` = '".$userid."'";
				  $result = mysql_query($sql);
				  
				  $ref_id = mysql_insert_id();
				  
				   addPostNotification($userid,$photoid,$ref_id,"","LIKE",strtoupper($posttype));
					$msg	= 'Successfully';
					$status = "true";
			}
			//for dislike
			if($likeStatus == "0"){
				$sql	=	"INSERT INTO jos_community_likes SET element = '".$posttype."', uid = '".$photoid."', `dislike` = '".$userid."'";
				$result = mysql_query($sql);
				$msg	= 'Successfully';
				$status = "true";
				
			}
		}

		addPostNotification($userid,$photoid,$ref_id,"","LIKE",strtoupper($posttype));
		$msg	= 'Successfully';
		$status = "true";
	}
	$arr=array("message"=>$msg,"status"=>$status,"photoid"=>$photoid);
	return $arr;
}

if (strtoupper($_REQUEST['type'])=="LIKE"){
	$data=json_decode(str_replace("\\","",urldecode($_GET['data'])));
	$userid		=	intval(trim($data[0]->userid));
	$photoid	=	intval(trim($data[0]->photoid));
	$like		=	intval(trim($data[0]->like));
	$posttype	=	(trim($data[0]->posttype));
	$arr=likePost($userid,$photoid,$like,$posttype);
	
	echo json_encode($arr);
}