-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 04, 2014 at 09:19 AM
-- Server version: 5.5.37-35.1
-- PHP Version: 5.4.23

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `heminfo_instanine`
--

-- --------------------------------------------------------

--
-- Table structure for table `albumdetail`
--

CREATE TABLE IF NOT EXISTS `albumdetail` (
  `albumid` int(10) NOT NULL,
  `userid` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comment_collage`
--

CREATE TABLE IF NOT EXISTS `comment_collage` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userid` int(10) NOT NULL,
  `albumid` int(10) NOT NULL,
  `comment` varchar(250) NOT NULL,
  `dtdate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `facebook`
--

CREATE TABLE IF NOT EXISTS `facebook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dt` datetime DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `rid` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_2` (`id`),
  KEY `rid` (`rid`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_community_activities`
--

CREATE TABLE IF NOT EXISTS `jos_community_activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `actor` int(10) unsigned NOT NULL,
  `target` int(10) unsigned NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `app` varchar(200) NOT NULL,
  `verb` varchar(200) NOT NULL,
  `cid` int(10) NOT NULL,
  `groupid` int(10) DEFAULT NULL,
  `eventid` int(10) DEFAULT NULL,
  `group_access` tinyint(4) NOT NULL DEFAULT '0',
  `event_access` tinyint(4) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `access` tinyint(3) unsigned NOT NULL,
  `params` text NOT NULL,
  `points` int(4) NOT NULL DEFAULT '1',
  `archived` tinyint(3) NOT NULL,
  `location` text NOT NULL,
  `latitude` float NOT NULL DEFAULT '255',
  `longitude` float NOT NULL DEFAULT '255',
  `comment_id` int(10) NOT NULL,
  `comment_type` varchar(200) NOT NULL,
  `like_id` int(10) NOT NULL,
  `like_type` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `actor` (`actor`),
  KEY `target` (`target`),
  KEY `app` (`app`),
  KEY `created` (`created`),
  KEY `archived` (`archived`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `jos_community_activities`
--

INSERT INTO `jos_community_activities` (`id`, `actor`, `target`, `title`, `content`, `app`, `verb`, `cid`, `groupid`, `eventid`, `group_access`, `event_access`, `created`, `access`, `params`, `points`, `archived`, `location`, `latitude`, `longitude`, `comment_id`, `comment_type`, `like_id`, `like_type`) VALUES
(1, 4, 3, '{actor} and {target} are now friends', '', 'friends', '', 0, NULL, NULL, 0, 0, '2014-08-04 11:55:12', 0, '', 1, 0, '', 255, 255, 0, '', 0, ''),
(2, 4, 0, 'Comment on the photo <a href=_QQQ_/instanine/vijay123/photos/2#photoid=4_QQQ_>Nice yar</a>', 'nice image brother', 'photos', '', 4, 0, 0, 0, 0, '2014-08-04 12:16:26', 0, 'photoid=4\naction=wall\nwallid=1\nurl=/instanine/vijay123/photos/2#photoid=4', 1, 0, '', 255, 255, 1, 'photos.wall.create', 1, 'photos.wall.create'),
(3, 4, 0, 'Comment on the album <a href=_QQQ_{url}_QQQ_>Nice image brother</a>', 'hello brother nice image and album...!', 'albums', '', 2, 0, 0, 0, 0, '2014-08-04 12:17:38', 0, 'photoid=2\naction=wall\nwallid=2\nurl=index.php?option=com_community&view=photos&task=album&albumid=2&userid=4', 1, 0, '', 255, 255, 2, 'photos.wall.create', 2, 'photos.wall.create');

-- --------------------------------------------------------

--
-- Table structure for table `jos_community_activities_hide`
--

CREATE TABLE IF NOT EXISTS `jos_community_activities_hide` (
  `activity_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_community_avatar`
--

CREATE TABLE IF NOT EXISTS `jos_community_avatar` (
  `id` int(10) unsigned NOT NULL,
  `apptype` varchar(255) NOT NULL,
  `path` text NOT NULL,
  `type` tinyint(3) unsigned NOT NULL COMMENT '0 = small, 1 = medium, 2=large',
  UNIQUE KEY `id` (`id`,`apptype`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_community_blocklist`
--

CREATE TABLE IF NOT EXISTS `jos_community_blocklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `blocked_userid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`),
  KEY `blocked_userid` (`blocked_userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_community_connection`
--

CREATE TABLE IF NOT EXISTS `jos_community_connection` (
  `connection_id` int(11) NOT NULL AUTO_INCREMENT,
  `connect_from` int(11) NOT NULL,
  `connect_to` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `group` int(11) NOT NULL,
  `msg` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`connection_id`),
  KEY `connect_from` (`connect_from`,`connect_to`,`status`,`group`),
  KEY `idx_connect_to` (`connect_to`),
  KEY `idx_connect_from` (`connect_from`),
  KEY `idx_connect_tofrom` (`connect_to`,`connect_from`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `jos_community_connection`
--

INSERT INTO `jos_community_connection` (`connection_id`, `connect_from`, `connect_to`, `status`, `group`, `msg`, `created`) VALUES
(1, 4, 3, 1, 0, '', '2014-08-04 11:55:12');

-- --------------------------------------------------------

--
-- Table structure for table `jos_community_fields`
--

CREATE TABLE IF NOT EXISTS `jos_community_fields` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordering` int(11) DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `min` int(5) NOT NULL,
  `max` int(5) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tips` text COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(1) DEFAULT '0',
  `required` tinyint(1) DEFAULT '0',
  `searchable` tinyint(1) DEFAULT '1',
  `registration` tinyint(1) DEFAULT '1',
  `options` text COLLATE utf8_unicode_ci,
  `fieldcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fieldcode` (`fieldcode`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=32 ;

--
-- Dumping data for table `jos_community_fields`
--

INSERT INTO `jos_community_fields` (`id`, `type`, `ordering`, `published`, `min`, `max`, `name`, `tips`, `visible`, `required`, `searchable`, `registration`, `options`, `fieldcode`, `params`) VALUES
(1, 'group', 3, 1, 0, 0, 'Basic Information', '', 1, 0, 1, 0, '', '', ''),
(2, 'select', 4, 1, 0, 0, 'Gender', 'Select gender', 1, 0, 1, 1, 'Male\nFemale', 'FIELD_GENDER', 'size=\nstyle=\n\n'),
(3, 'birthdate', 5, 1, 0, 0, 'Birthdate', 'Enter your date of birth so other users can know when to wish you happy birthday ', 1, 0, 1, 0, '', 'FIELD_BIRTHDATE', 'display=age\nreadonly=0\nmaxrange=\nminrange=\ndate_format=\nstyle=\n\n'),
(4, 'textarea', 7, 1, 0, 4000, 'About', 'Tell us more about yourself', 1, 0, 1, 0, NULL, 'FIELD_ABOUTME', 'readonly=0\nmin_char=0\nmax_char=4000\nrows=2\ncols=2\nstyle=\n\n'),
(5, 'group', 12, 1, 0, 0, 'Contact Information', '', 1, 1, 1, 1, '', '', ''),
(6, 'text', 14, 1, 0, 0, 'Mobile Phone', 'Mobile carrier number that other users can contact you.', 1, 0, 1, 0, '', 'FIELD_MOBILE', 'readonly=0\ntype=text\nmin_char=\nmax_char=\nmaxlength=\nstyle=\n\n'),
(7, 'text', 13, 1, 0, 0, 'Phone', 'Contact number that other users can contact you.', 1, 0, 1, 0, '', 'FIELD_LANDPHONE', 'readonly=0\ntype=text\nmin_char=\nmax_char=\nmaxlength=\nstyle=\n\n'),
(8, 'textarea', 16, 1, 10, 100, 'Address', 'Your Address', 1, 0, 1, 0, '', 'FIELD_ADDRESS', ''),
(9, 'select', 18, 1, 0, 0, 'State', 'Your state', 1, 1, 1, 1, 'Alabama\nAlaska\nAmerican Samoa\nArizona\nArkansas\nCalifornia\nColorado\nConnecticut\nDelaware\nDistrict of Columbia\nFlorida\nGeorgia\nGuam\nHawaii\nIdaho\nIllinois\nIndiana\nIowa\nKansas\nKentucky\nLouisiana\nMaine\nMaryland\nMassachusetts\nMichigan\nMinnesota\nMississippi\nMissouri\nMontana\nNebraska\nNevada\nNew Hampshire\nNew Jersey\nNew Mexico\nNew York\nNorth Carolina\nNorth Dakota\nNorthern Marianas Islands\nOhio\nOklahoma\nOregon\nPennsylvania\nPuerto Rico\nRhode Island\nSouth Carolina\nSouth Dakota\nTennessee\nTexas\nUtah\nVermont\nVirginia\nVirgin Islands\nWashington\nWest Virginia\nWisconsin\nWyoming ', 'FIELD_STATE', 'size=\nstyle=color: #20b2aa;\n\n'),
(10, 'text', 17, 1, 0, 0, 'City', 'Your city or town name', 1, 1, 1, 1, '', 'FIELD_CITY', 'readonly=0\ntype=text\nmin_char=\nmax_char=\nmaxlength=\nstyle=color: #20b2aa;\n\n'),
(11, 'country', 19, 1, 10, 100, 'Country', 'Your country', 1, 1, 1, 1, 'Afghanistan\nAlbania\nAlgeria\nAmerican Samoa\nAndorra\nAngola\nAnguilla\nAntarctica\nAntigua and Barbuda\nArgentina\nArmenia\nAruba\nAustralia\nAustria\nAzerbaijan\nBahamas\nBahrain\nBangladesh\nBarbados\nBelarus\nBelgium\nBelize\nBenin\nBermuda\nBhutan\nBolivia\nBosnia and Herzegovina\nBotswana\nBouvet Island\nBrazil\nBritish Indian Ocean Territory\nBrunei Darussalam\nBulgaria\nBurkina Faso\nBurundi\nCambodia\nCameroon\nCanada\nCape Verde\nCayman Islands\nCentral African Republic\nChad\nChile\nChina\nChristmas Island\nCocos (Keeling) Islands\nColombia\nComoros\nCongo\nCook Islands\nCosta Rica\nCote D''Ivoire (Ivory Coast)\nCroatia (Hrvatska)\nCuba\nCyprus\nCzechoslovakia (former)\nCzech Republic\nDenmark\nDjibouti\nDominica\nDominican Republic\nEast Timor\nEcuador\nEgypt\nEl Salvador\nEquatorial Guinea\nEritrea\nEstonia\nEthiopia\nFalkland Islands (Malvinas)\nFaroe Islands\nFiji\nFinland\nFrance\nFrance, Metropolitan\nFrench Guiana\nFrench Polynesia\nFrench Southern Territories\nGabon\nGambia\nGeorgia\nGermany\nGhana\nGibraltar\nGreat Britain (UK)\nGreece\nGreenland\nGrenada\nGuadeloupe\nGuam\nGuatemala\nGuinea\nGuinea-Bissau\nGuyana\nHaiti\nHeard and McDonald Islands\nHonduras\nHong Kong\nHungary\nIceland\nIndia\nIndonesia\nIran\nIraq\nIreland\nIsrael\nItaly\nJamaica\nJapan\nJordan\nKazakhstan\nKenya\nKiribati\nKorea, North\nSouth Korea\nKuwait\nKyrgyzstan\nLaos\nLatvia\nLebanon\nLesotho\nLiberia\nLibya\nLiechtenstein\nLithuania\nLuxembourg\nMacau\nMacedonia\nMadagascar\nMalawi\nMalaysia\nMaldives\nMali\nMalta\nMarshall Islands\nMartinique\nMauritania\nMauritius\nMayotte\nMexico\nMicronesia\nMoldova\nMonaco\nMongolia\nMontserrat\nMorocco\nMozambique\nMyanmar\nNamibia\nNauru\nNepal\nNetherlands\nNetherlands Antilles\nNeutral Zone\nNew Caledonia\nNew Zealand\nNicaragua\nNiger\nNigeria\nNiue\nNorfolk Island\nNorthern Mariana Islands\nNorway\nOman\nPakistan\nPalau\nPanama\nPapua New Guinea\nParaguay\nPeru\nPhilippines\nPitcairn\nPoland\nPortugal\nPuerto Rico\nQatar\nReunion\nRomania\nRussian Federation\nRwanda\nSaint Kitts and Nevis\nSaint Lucia\nSaint Vincent and the Grenadines\nSamoa\nSan Marino\nSao Tome and Principe\nSaudi Arabia\nSenegal\nSeychelles\nS. Georgia and S. Sandwich Isls.\nSierra Leone\nSingapore\nSlovak Republic\nSlovenia\nSolomon Islands\nSomalia\nSouth Africa\nSpain\nSri Lanka\nSt. Helena\nSt. Pierre and Miquelon\nSudan\nSuriname\nSvalbard and Jan Mayen Islands\nSwaziland\nSweden\nSwitzerland\nSyria\nTaiwan\nTajikistan\nTanzania\nThailand\nTogo\nTokelau\nTonga\nTrinidad and Tobago\nTunisia\nTurkey\nTurkmenistan\nTurks and Caicos Islands\nTuvalu\nUganda\nUkraine\nUnited Arab Emirates\nUnited Kingdom\nUnited States\nUruguay\nUS Minor Outlying Islands\nUSSR (former)\nUzbekistan\nVanuatu\nVatican City State (Holy Sea)\nVenezuela\nViet Nam\nVirgin Islands (British)\nVirgin Islands (U.S.)\nWallis and Futuna Islands\nWestern Sahara\nYemen\nYugoslavia\nZaire\nZambia\nZimbabwe', 'FIELD_COUNTRY', ''),
(12, 'url', 20, 1, 0, 0, 'Website', 'Your website', 1, 0, 1, 0, '', 'FIELD_WEBSITE', 'readonly=0\ntype=text\nmin_char=\nmax_char=\nmaxlength=\nstyle=\n\n'),
(13, 'group', 22, 1, 10, 100, 'Education', 'Educations', 1, 0, 1, 0, '', '', ''),
(14, 'text', 23, 1, 10, 200, 'College / University', 'Your college or university name', 1, 0, 1, 0, '', 'FIELD_COLLEGE', ''),
(15, 'text', 24, 1, 5, 100, 'Graduation Year', 'Graduation year', 1, 0, 1, 0, '', 'FIELD_GRADUATION', ''),
(16, 'text', 15, 1, 0, 0, 'Zip', 'Enter your Zip Code', 1, 1, 1, 1, '', 'FIELD_ZIP', 'readonly=0\ntype=text\nmin_char=\nmax_char=5\nmaxlength=50px\nstyle=\n\n'),
(17, 'text', 8, 1, 0, 0, 'Occupation', '', 1, 0, 1, 0, '', 'FIELD_OCCUPATION', 'readonly=0\ntype=text\nmin_char=\nmax_char=\nmaxlength=\nstyle=\n\n'),
(18, 'select', 9, 1, 0, 0, 'Relationship Status', '', 1, 0, 1, 0, 'Married\nIn a Relationship\nCan''t Explain It\nSingle', 'FIELD_RELATIONSHIP', 'size=\nstyle=\n\n'),
(19, 'select', 10, 1, 0, 0, 'Interested In', '', 1, 0, 1, 0, 'Women\nMen\nBoth', 'FIELD_INTERESTED', 'size=\nstyle=\n\n'),
(20, 'textarea', 11, 1, 0, 0, 'Hobbies / Interests', '', 1, 0, 1, 0, '', 'FIELD_HOBBIES', 'readonly=0\nmin_char=0\nmax_char=140\nrows=3\ncols=3\nstyle=\n\n'),
(21, 'text', 21, 1, 0, 0, 'Email', '', 1, 0, 1, 0, '', 'FIELD_EMAIL', '\n'),
(22, 'radio', 6, 1, 0, 0, 'Display Year', '', 1, 0, 1, 0, 'Yes\nNo', 'FIELD_YEAR', 'readonly=0\nstyle=\n\n'),
(23, 'group', 25, 1, 0, 0, 'Event Information', '', 1, 0, 1, 0, '', '', ''),
(24, 'textarea', 26, 1, 0, 0, 'Type of Events', 'Type of events that you host.', 1, 0, 1, 0, '', 'FIELD_TYPEOFEVENTS', 'readonly=0\nmin_char=\nmax_char=\nrows=2\ncols=2\nstyle=\n\n'),
(25, 'textarea', 27, 1, 0, 0, 'Favorite Venues', 'Your favorite places to host events', 1, 0, 1, 0, '', 'FIELD_FAVORITEVENUES', 'readonly=0\nmin_char=\nmax_char=\nrows=2\ncols=2\nstyle=\n\n'),
(26, 'textarea', 28, 1, 0, 0, 'Types of Music', 'Music types played at your events.', 1, 0, 1, 0, '', 'FIELD_TYPESOFMUSIC', 'readonly=0\nmin_char=\nmax_char=\nrows=2\ncols=2\nstyle=\n\n'),
(27, 'text', 1, 1, 0, 0, 'First Name', '', 1, 1, 1, 1, '', 'FIELD_GIVENNAME', '\n'),
(28, 'text', 2, 1, 0, 0, 'Last Name', '', 1, 1, 1, 1, '', 'FIELD_FAMILYNAME', '\n'),
(30, 'group', 29, 1, 0, 0, 'Additional Info', '', 2, 1, 0, 1, '', '', ''),
(31, 'singleselect', 30, 1, 0, 0, 'How did you hear about us?', '', 2, 1, 0, 1, 'Friends\nSpecial Promotion\nGoogle\nFacebook\nTwitter\nOther', 'FIELD_HEAR', 'multiple=0\nsize=\nstyle=\n\n');

-- --------------------------------------------------------

--
-- Table structure for table `jos_community_fields_values`
--

CREATE TABLE IF NOT EXISTS `jos_community_fields_values` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `field_id` int(10) NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `access` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `field_id` (`field_id`),
  KEY `user_id` (`user_id`),
  KEY `idx_user_fieldid` (`user_id`,`field_id`),
  KEY `access` (`access`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=45 ;

--
-- Dumping data for table `jos_community_fields_values`
--

INSERT INTO `jos_community_fields_values` (`id`, `user_id`, `field_id`, `value`, `access`) VALUES
(20, 3, 28, 'ameta', 0),
(19, 3, 2, '', 0),
(18, 3, 16, '12345', 0),
(17, 3, 10, 'Jaipur', 0),
(16, 3, 9, 'Rajasthan', 0),
(15, 3, 11, 'India', 0),
(21, 3, 27, 'vijay', 0),
(41, 4, 28, 'jain', 0),
(40, 4, 2, 'Male', 0),
(39, 4, 16, '12345', 0),
(38, 4, 10, 'Jaipur', 0),
(37, 4, 9, 'Rajasthan', 0),
(36, 4, 11, 'India', 0),
(42, 4, 27, 'manish', 0),
(43, 5, 28, 'Joel', 0),
(44, 5, 27, 'John', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_community_likes`
--

CREATE TABLE IF NOT EXISTS `jos_community_likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `element` varchar(200) NOT NULL,
  `uid` int(10) NOT NULL,
  `like` text NOT NULL,
  `dislike` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `element` (`element`,`uid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `jos_community_likes`
--

INSERT INTO `jos_community_likes` (`id`, `element`, `uid`, `like`, `dislike`) VALUES
(1, 'photo', 4, '3,4', ''),
(2, 'photo', 3, '3,4', ''),
(3, 'photo', 5, '3', ''),
(4, 'photo', 6, '3', '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_community_photos`
--

CREATE TABLE IF NOT EXISTS `jos_community_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `albumid` int(11) NOT NULL,
  `caption` text NOT NULL,
  `published` tinyint(1) NOT NULL,
  `creator` int(11) NOT NULL,
  `permissions` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `original` varchar(255) NOT NULL,
  `filesize` int(11) NOT NULL DEFAULT '0',
  `storage` varchar(64) NOT NULL DEFAULT 'file',
  `created` datetime NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `hits` int(11) NOT NULL DEFAULT '0',
  `status` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `albumid` (`albumid`),
  KEY `idx_storage` (`storage`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `jos_community_photos`
--

INSERT INTO `jos_community_photos` (`id`, `albumid`, `caption`, `published`, `creator`, `permissions`, `image`, `thumbnail`, `original`, `filesize`, `storage`, `created`, `ordering`, `hits`, `status`) VALUES
(6, 4, 'My pic', 1, 3, '0', 'images/photos/3/4/5645image1.png', 'images/photos/3/4/thumb_5645image1.png', 'images/originalphotos/3/4/5645image1.png', 0, 'file', '2014-08-04 13:33:42', 1, 0, ''),
(5, 3, '', 1, 3, '0', 'images/photos/3/3/3985image1.png', 'images/photos/3/3/thumb_3985image1.png', 'images/originalphotos/3/3/3985image1.png', 0, 'file', '2014-08-04 13:09:55', 1, 0, ''),
(3, 2, 'Nice image', 1, 3, '0', 'images/photos/3/2/4948image1.png', 'images/photos/3/2/thumb_4948image1.png', 'images/originalphotos/3/2/4948image1.png', 0, 'file', '2014-08-04 10:59:14', 1, 0, ''),
(4, 2, 'Nice yar', 1, 3, '0', 'images/photos/3/2/3094image2.png', 'images/photos/3/2/thumb_3094image2.png', 'images/originalphotos/3/2/3094image2.png', 0, 'file', '2014-08-04 10:59:14', 2, 0, ''),
(7, 4, 'My doggy #cute #dog', 1, 3, '0', 'images/photos/3/4/8544image2.png', 'images/photos/3/4/thumb_8544image2.png', 'images/originalphotos/3/4/8544image2.png', 0, 'file', '2014-08-04 13:33:42', 2, 0, ''),
(8, 5, 'Qwerty', 1, 3, '0', 'images/photos/3/5/7289image1.png', 'images/photos/3/5/thumb_7289image1.png', 'images/originalphotos/3/5/7289image1.png', 0, 'file', '2014-08-04 13:42:10', 1, 0, ''),
(9, 5, 'My pic', 1, 3, '0', 'images/photos/3/5/433image2.png', 'images/photos/3/5/thumb_433image2.png', 'images/originalphotos/3/5/433image2.png', 0, 'file', '2014-08-04 13:42:10', 2, 0, ''),
(10, 6, '', 1, 3, '0', 'images/photos/3/6/8254image1.png', 'images/photos/3/6/thumb_8254image1.png', 'images/originalphotos/3/6/8254image1.png', 0, 'file', '2014-08-04 13:46:49', 1, 0, ''),
(11, 6, '', 1, 3, '0', 'images/photos/3/6/4750image2.png', 'images/photos/3/6/thumb_4750image2.png', 'images/originalphotos/3/6/4750image2.png', 0, 'file', '2014-08-04 13:46:49', 2, 0, ''),
(12, 6, '', 1, 3, '0', 'images/photos/3/6/9043image3.png', 'images/photos/3/6/thumb_9043image3.png', 'images/originalphotos/3/6/9043image3.png', 0, 'file', '2014-08-04 13:46:49', 3, 0, ''),
(13, 6, '', 1, 3, '0', 'images/photos/3/6/3010image4.png', 'images/photos/3/6/thumb_3010image4.png', 'images/originalphotos/3/6/3010image4.png', 0, 'file', '2014-08-04 13:46:49', 4, 0, ''),
(14, 7, '', 1, 3, '0', 'images/photos/3/7/8980image1.png', 'images/photos/3/7/thumb_8980image1.png', 'images/originalphotos/3/7/8980image1.png', 0, 'file', '2014-08-04 13:50:20', 1, 0, ''),
(15, 7, '', 1, 3, '0', 'images/photos/3/7/9110image2.png', 'images/photos/3/7/thumb_9110image2.png', 'images/originalphotos/3/7/9110image2.png', 0, 'file', '2014-08-04 13:50:20', 2, 0, ''),
(16, 8, '', 1, 3, '0', 'images/photos/3/8/7432image1.png', 'images/photos/3/8/thumb_7432image1.png', 'images/originalphotos/3/8/7432image1.png', 0, 'file', '2014-08-04 14:06:41', 1, 0, ''),
(17, 8, '', 1, 3, '0', 'images/photos/3/8/615image2.png', 'images/photos/3/8/thumb_615image2.png', 'images/originalphotos/3/8/615image2.png', 0, 'file', '2014-08-04 14:06:41', 2, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_community_photos_albums`
--

CREATE TABLE IF NOT EXISTS `jos_community_photos_albums` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photoid` int(11) NOT NULL,
  `creator` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `permissions` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `path` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `groupid` int(11) NOT NULL DEFAULT '0',
  `hits` int(11) NOT NULL DEFAULT '0',
  `location` text NOT NULL,
  `latitude` float NOT NULL DEFAULT '255',
  `longitude` float NOT NULL DEFAULT '255',
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `allow_follower` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `creator` (`creator`),
  KEY `idx_type` (`type`),
  KEY `idx_albumtype` (`id`,`type`),
  KEY `idx_creatortype` (`creator`,`type`),
  KEY `idx_groupid` (`groupid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `jos_community_photos_albums`
--

INSERT INTO `jos_community_photos_albums` (`id`, `photoid`, `creator`, `name`, `description`, `permissions`, `created`, `path`, `type`, `groupid`, `hits`, `location`, `latitude`, `longitude`, `default`, `allow_follower`) VALUES
(3, 5, 3, 'Mast pic', '', '0', '2014-08-04 13:09:55', 'images/photos/3/', 'user', 0, 0, '', 255, 255, 0, 1),
(2, 3, 3, 'Nice image brother', '', '0', '2014-08-04 10:59:13', 'images/photos/3/', 'user', 0, 0, '', 255, 255, 0, 1),
(4, 6, 3, 'Nice @manijain333 pic #brother', '', '0', '2014-08-04 13:33:42', 'images/photos/3/', 'user', 0, 0, '', 255, 255, 0, 0),
(5, 8, 3, 'Nice allbum #vijay', '', '0', '2014-08-04 13:42:10', 'images/photos/3/', 'user', 0, 0, '', 255, 255, 0, 1),
(6, 10, 3, 'Qwertyuiio', '', '0', '2014-08-04 13:46:49', 'images/photos/3/', 'user', 0, 0, '', 255, 255, 0, 1),
(7, 14, 3, 'Nice instanube album images', '', '0', '2014-08-04 13:50:19', 'images/photos/3/', 'user', 0, 0, '', 255, 255, 0, 1),
(8, 16, 3, 'Bell nice', '', '0', '2014-08-04 14:06:41', 'images/photos/3/', 'user', 0, 0, '', 255, 255, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jos_community_users`
--

CREATE TABLE IF NOT EXISTS `jos_community_users` (
  `userid` int(11) NOT NULL,
  `status` text COLLATE utf8_unicode_ci NOT NULL,
  `status_access` int(11) NOT NULL DEFAULT '0',
  `points` int(11) NOT NULL,
  `posted_on` datetime NOT NULL,
  `avatar` text COLLATE utf8_unicode_ci NOT NULL,
  `thumb` text COLLATE utf8_unicode_ci NOT NULL,
  `invite` int(11) NOT NULL DEFAULT '0',
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  `view` int(11) NOT NULL DEFAULT '0',
  `friends` text COLLATE utf8_unicode_ci NOT NULL,
  `groups` text COLLATE utf8_unicode_ci NOT NULL,
  `events` text COLLATE utf8_unicode_ci NOT NULL,
  `friendcount` int(11) NOT NULL DEFAULT '0',
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` float NOT NULL DEFAULT '255',
  `longitude` float NOT NULL DEFAULT '255',
  `profile_id` int(11) NOT NULL DEFAULT '0',
  `storage` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'file',
  `watermark_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `search_email` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `jos_community_users`
--

INSERT INTO `jos_community_users` (`userid`, `status`, `status_access`, `points`, `posted_on`, `avatar`, `thumb`, `invite`, `params`, `view`, `friends`, `groups`, `events`, `friendcount`, `alias`, `latitude`, `longitude`, `profile_id`, `storage`, `watermark_hash`, `search_email`) VALUES
(3, 'iOS application developer', 0, 0, '0000-00-00 00:00:00', 'uploads/3/file1435.png', 'uploads/3/file1435.png', 0, '', 0, '', '', '', 0, '', 255, 255, 0, 'file', '', 1),
(4, '', 0, 2, '0000-00-00 00:00:00', 'uploads/4/file1343.png', 'uploads/4/file1343.png', 0, '', 0, '', '', '', 0, '', 255, 255, 1, 'file', '', 1),
(5, '', 0, 2, '0000-00-00 00:00:00', 'uploads/5/image1.jpg', 'uploads/5/image1.jpg', 0, '', 0, '', '', '', 0, '', 255, 255, 1, 'file', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jos_community_wall`
--

CREATE TABLE IF NOT EXISTS `jos_community_wall` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contentid` int(10) unsigned NOT NULL DEFAULT '0',
  `post_by` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(45) NOT NULL,
  `comment` text NOT NULL,
  `date` varchar(45) NOT NULL,
  `published` tinyint(1) unsigned NOT NULL,
  `type` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contentid` (`contentid`),
  KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `jos_community_wall`
--

INSERT INTO `jos_community_wall` (`id`, `contentid`, `post_by`, `ip`, `comment`, `date`, `published`, `type`) VALUES
(1, 4, 4, '180.188.253.133', 'nice image brother', '2014-08-04 12:16:26', 1, 'photos'),
(2, 2, 4, '180.188.253.133', 'hello brother nice image and album...!', '2014-08-04 12:17:38', 1, 'albums');

-- --------------------------------------------------------

--
-- Table structure for table `jos_core_acl_aro`
--

CREATE TABLE IF NOT EXISTS `jos_core_acl_aro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_value` varchar(240) NOT NULL DEFAULT '0',
  `value` varchar(240) NOT NULL DEFAULT '',
  `order_value` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `jos_section_value_value_aro` (`section_value`(100),`value`(100)),
  KEY `jos_gacl_hidden_aro` (`hidden`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=892 ;

--
-- Dumping data for table `jos_core_acl_aro`
--

INSERT INTO `jos_core_acl_aro` (`id`, `section_value`, `value`, `order_value`, `name`, `hidden`) VALUES
(10, 'users', '62', 0, 'Mingleloop', 0),
(11, 'users', '63', 0, 'Ken Gibbs', 0),
(12, 'users', '64', 0, 'Darrell Coleman', 0),
(13, 'users', '65', 0, 'The Imperium Group', 0),
(129, 'users', '181', 0, 'LIFEKIT LIFESTYLE CO.', 0),
(124, 'users', '176', 0, 'Amanda Clincy', 0),
(101, 'users', '153', 0, 'Mingleloop', 0),
(128, 'users', '180', 0, 'Elizabeth Gibbs', 0),
(125, 'users', '177', 0, 'Bro Alkebulan', 0),
(126, 'users', '178', 0, 'Charles Ashley', 0),
(127, 'users', '179', 0, 'Marcus Robinson', 0),
(130, 'users', '182', 0, 'Audrey Cherry', 0),
(131, 'users', '183', 0, 'Brina', 0),
(132, 'users', '184', 0, 'Josh', 0),
(133, 'users', '185', 0, 'Adriana Cash', 0),
(134, 'users', '186', 0, 'WALK', 0),
(138, 'users', '190', 0, 'Jamie Leon', 0),
(137, 'users', '189', 0, 'Derek Woods', 0),
(141, 'users', '193', 0, 'Pop Music Entertainment', 0),
(146, 'users', '198', 0, 'Darrell Houston', 0),
(147, 'users', '199', 0, 'Ryan M Norris', 0),
(153, 'users', '205', 0, 'Omari', 0),
(156, 'users', '208', 0, 'Basil', 0),
(154, 'users', '206', 0, 'AJH', 0),
(161, 'users', '213', 0, 'Queenate Ibeto', 0),
(160, 'users', '212', 0, 'Nicole Robideau', 0),
(162, 'users', '214', 0, 'Gary D. Mattocks, Jr.', 0),
(164, 'users', '216', 0, 'Kevin Clark', 0),
(189, 'users', '241', 0, 'Ken Gibbs', 0),
(190, 'users', '242', 0, 'mary', 0),
(191, 'users', '243', 0, 'Chantel Hammonds', 0),
(268, 'users', '320', 0, 'Leland Davis', 0),
(187, 'users', '239', 0, 'Derek', 0),
(193, 'users', '245', 0, 'Renee', 0),
(182, 'users', '234', 0, 'Amanda Atchley', 0),
(192, 'users', '244', 0, 'Kewanda Merritt', 0),
(195, 'users', '247', 0, 'Ian Bryant', 0),
(197, 'users', '249', 0, 'Pia Taylor', 0),
(198, 'users', '250', 0, 'Steven', 0),
(199, 'users', '251', 0, 'Danielle', 0),
(200, 'users', '252', 0, 'Sommer Murrell', 0),
(201, 'users', '253', 0, 'Quilla Brown', 0),
(202, 'users', '254', 0, 'Alston', 0),
(203, 'users', '255', 0, 'LaKendra Phifer', 0),
(204, 'users', '256', 0, 'Carla Moreno', 0),
(205, 'users', '257', 0, 'danielle mcgowan', 0),
(206, 'users', '258', 0, 'ACT', 0),
(207, 'users', '259', 0, 'Robert Tilman', 0),
(208, 'users', '260', 0, 'Dani', 0),
(209, 'users', '261', 0, 'Sheree Williamson', 0),
(210, 'users', '262', 0, 'mark clark', 0),
(211, 'users', '263', 0, 'Leroy Wray', 0),
(212, 'users', '264', 0, 'PATRICIA S GIBBS', 0),
(213, 'users', '265', 0, 'DeAnna Taylor', 0),
(214, 'users', '266', 0, 'nathan smalls', 0),
(215, 'users', '267', 0, 'NXL21 Charlotte', 0),
(216, 'users', '268', 0, 'Kimberly c Johnson', 0),
(616, 'users', '668', 0, 'Mack Rob', 0),
(218, 'users', '270', 0, 'Harvey Cummings II', 0),
(219, 'users', '271', 0, 'Twanika Peacock', 0),
(220, 'users', '272', 0, 'Demetria pipkin', 0),
(221, 'users', '273', 0, 'Jessica Barnwell', 0),
(222, 'users', '274', 0, 'Mia Kennedy', 0),
(223, 'users', '275', 0, 'Elethia', 0),
(236, 'users', '288', 0, 'B.Riley', 0),
(225, 'users', '277', 0, 'Nicole Roseborough', 0),
(226, 'users', '278', 0, 'Kristell Engram', 0),
(227, 'users', '279', 0, 'Markia Jeter', 0),
(228, 'users', '280', 0, 'Victor Purcell', 0),
(229, 'users', '281', 0, 'Bianca Cunningham', 0),
(230, 'users', '282', 0, 'Dierre Perry', 0),
(231, 'users', '283', 0, 'Jasmine Ay', 0),
(232, 'users', '284', 0, 'Angelique Hilliard', 0),
(233, 'users', '285', 0, 'Kerri Legette', 0),
(234, 'users', '286', 0, 'Wrandell Duggins', 0),
(235, 'users', '287', 0, 'Derwin Cox', 0),
(237, 'users', '289', 0, 'Danielle Richmond', 0),
(238, 'users', '290', 0, 'Javanti Rogers', 0),
(239, 'users', '291', 0, 'CJ ARMSTRONG', 0),
(240, 'users', '292', 0, 'T.Jones', 0),
(241, 'users', '293', 0, 'Bradley Davis', 0),
(242, 'users', '294', 0, 'Jessica', 0),
(243, 'users', '295', 0, 'Lango Kids RTP', 0),
(244, 'users', '296', 0, 'Kayla', 0),
(245, 'users', '297', 0, 'Davon Harris', 0),
(246, 'users', '298', 0, 'Kenaz', 0),
(247, 'users', '299', 0, 'Twaini Little-Holt', 0),
(248, 'users', '300', 0, 'Jacqueline Purdie', 0),
(249, 'users', '301', 0, 'Zii Rio', 0),
(250, 'users', '302', 0, 'KHayes', 0),
(251, 'users', '303', 0, 'Trae', 0),
(252, 'users', '304', 0, 'Domnique Sloan', 0),
(253, 'users', '305', 0, 'Roland Reaves', 0),
(254, 'users', '306', 0, 'Krystal Braswell', 0),
(255, 'users', '307', 0, 'William Smalls', 0),
(256, 'users', '308', 0, 'Ashley Gray', 0),
(257, 'users', '309', 0, 'Angela', 0),
(258, 'users', '310', 0, 'Everett Bray', 0),
(259, 'users', '311', 0, 'Big C', 0),
(260, 'users', '312', 0, 'mike pierce', 0),
(261, 'users', '313', 0, 'Dawn Maloney', 0),
(265, 'users', '317', 0, 'Austin', 0),
(269, 'users', '321', 0, 'elethia', 0),
(270, 'users', '322', 0, 'Ashley Witherspoon', 0),
(271, 'users', '323', 0, 'Brother Randy', 0),
(278, 'users', '330', 0, 'Rose Leak', 0),
(279, 'users', '331', 0, 'Jennifer Rendon', 0),
(280, 'users', '332', 0, 'Chris Jordan', 0),
(281, 'users', '333', 0, 'Judy Carter', 0),
(282, 'users', '334', 0, 'Bill William', 0),
(283, 'users', '335', 0, 'Lashia Washington', 0),
(284, 'users', '336', 0, 'delton billups', 0),
(285, 'users', '337', 0, 'Sean Combs', 0),
(286, 'users', '338', 0, 'Rick  Ross', 0),
(287, 'users', '339', 0, 'Cassie  Leslie', 0),
(288, 'users', '340', 0, 'Kim  Kardashian', 0),
(289, 'users', '341', 0, 'Usher Raymond', 0),
(290, 'users', '342', 0, 'Kanye  West', 0),
(291, 'users', '343', 0, 'Sean Carter', 0),
(292, 'users', '344', 0, 'Rihanna Smith', 0),
(293, 'users', '345', 0, 'Gabrielle  Union', 0),
(294, 'users', '346', 0, 'Erykah Badu', 0),
(295, 'users', '347', 0, 'Beyonce Knowles', 0),
(296, 'users', '348', 0, 'Idris  Elba', 0),
(297, 'users', '349', 0, 'TERESA REID', 0),
(298, 'users', '350', 0, 'Demetrius goodwin', 0),
(619, 'users', '671', 0, 'Charlotte Greek  Picnic', 0),
(300, 'users', '352', 0, 'Stephanie Jackson', 0),
(301, 'users', '353', 0, 'Willie Johnson', 0),
(302, 'users', '354', 0, 'Preston Johnson', 0),
(303, 'users', '355', 0, 'Antonia Wilson', 0),
(304, 'users', '356', 0, 'Marcus  Ferguson', 0),
(305, 'users', '357', 0, 'Monica Jefferson', 0),
(306, 'users', '358', 0, 'Jon Jefferson', 0),
(307, 'users', '359', 0, 'alicia avant', 0),
(308, 'users', '360', 0, 'Brenda Murray', 0),
(309, 'users', '361', 0, 'Tia McKnight', 0),
(310, 'users', '362', 0, 'Aldonell Allah', 0),
(311, 'users', '363', 0, 'Luisa  Diaz', 0),
(312, 'users', '364', 0, 'jamel  donaldson', 0),
(313, 'users', '365', 0, 'Secilee Jackson', 0),
(314, 'users', '366', 0, 'Vanessa olton', 0),
(315, 'users', '367', 0, 'Don', 0),
(638, 'users', '690', 0, 'Harvey Sutton', 0),
(317, 'users', '369', 0, 'Frederick Ravin', 0),
(318, 'users', '370', 0, 'GroundBreaking  Entertainment', 0),
(319, 'users', '371', 0, 'Groundbreaking Ent', 0),
(321, 'users', '373', 0, 'joyy evans', 0),
(322, 'users', '374', 0, 'Matt Gattis', 0),
(323, 'users', '375', 0, 'Lauren Latten', 0),
(324, 'users', '376', 0, 'Darren Hart', 0),
(325, 'users', '377', 0, 'Ashley  mcleod', 0),
(326, 'users', '378', 0, 'Jessica Rodriguez', 0),
(327, 'users', '379', 0, 'Brittney Henderson', 0),
(328, 'users', '380', 0, 'Brittany Holloman', 0),
(329, 'users', '381', 0, 'Kelsey Williams', 0),
(330, 'users', '382', 0, 'Raven Quinn', 0),
(331, 'users', '383', 0, 'Gabriela Galo', 0),
(332, 'users', '384', 0, 'calvin brown', 0),
(333, 'users', '385', 0, 'Tiffany  Berry', 0),
(334, 'users', '386', 0, 'Dia Barber', 0),
(335, 'users', '387', 0, 'Lindsey  Barrett', 0),
(336, 'users', '388', 0, 'Keyna Spruiell', 0),
(337, 'users', '389', 0, 'Joshua Efik', 0),
(338, 'users', '390', 0, 'Taylar Byrd', 0),
(339, 'users', '391', 0, 'Tykesha  Alston', 0),
(340, 'users', '392', 0, 'Tykesha Alston', 0),
(341, 'users', '393', 0, 'Treyaun Fulton', 0),
(342, 'users', '394', 0, 'Alicia McDowell', 0),
(343, 'users', '395', 0, 'Damali Richburg', 0),
(344, 'users', '396', 0, 'Kamaara Lucas', 0),
(345, 'users', '397', 0, 'Nhandi Kenion', 0),
(346, 'users', '398', 0, 'Nakita Pigford', 0),
(347, 'users', '399', 0, 'Rakilli  Washington', 0),
(348, 'users', '400', 0, 'Andre Rowe', 0),
(349, 'users', '401', 0, 'Timeka Yarborough', 0),
(350, 'users', '402', 0, 'Allison Newton', 0),
(351, 'users', '403', 0, 'Lenny Gordon', 0),
(352, 'users', '404', 0, 'Erica Broughton', 0),
(353, 'users', '405', 0, 'Canitria Cook', 0),
(354, 'users', '406', 0, 'Ashley Davis', 0),
(355, 'users', '407', 0, 'Dawanas King', 0),
(356, 'users', '408', 0, 'Ashley Gaie', 0),
(357, 'users', '409', 0, 'Raven Jones', 0),
(358, 'users', '410', 0, 'Tiffany Williams', 0),
(359, 'users', '411', 0, 'Rameika Jones', 0),
(360, 'users', '412', 0, 'Meika Jones', 0),
(361, 'users', '413', 0, 'Andrea Pope', 0),
(362, 'users', '414', 0, 'Kendrick Singleton', 0),
(363, 'users', '415', 0, 'Paris Bennett', 0),
(364, 'users', '416', 0, 'Cynthia  Sharpe', 0),
(365, 'users', '417', 0, 'Cornicha  Krider', 0),
(366, 'users', '418', 0, 'Chasity Kea', 0),
(367, 'users', '419', 0, 'Erika Mercado', 0),
(368, 'users', '420', 0, 'Rakiya Denkins', 0),
(369, 'users', '421', 0, 'LaQuasha Good', 0),
(370, 'users', '422', 0, 'Cristal  Miller', 0),
(371, 'users', '423', 0, 'DC Alston', 0),
(372, 'users', '424', 0, 'Da-Neisha Thomas', 0),
(373, 'users', '425', 0, 'javairia wheeler', 0),
(374, 'users', '426', 0, 'Johnnie Ingram', 0),
(375, 'users', '427', 0, 'Byron Parker', 0),
(376, 'users', '428', 0, 'Rickell Ried', 0),
(377, 'users', '429', 0, 'Demarre Richmond', 0),
(378, 'users', '430', 0, 'Jasmine White', 0),
(379, 'users', '431', 0, 'Kiera Benson', 0),
(380, 'users', '432', 0, 'Eboni Squire', 0),
(381, 'users', '433', 0, 'Nicholas Powell', 0),
(382, 'users', '434', 0, 'Nicholas Powell', 0),
(383, 'users', '435', 0, 'Kahliah  Gordon', 0),
(384, 'users', '436', 0, 'eric laws', 0),
(385, 'users', '437', 0, 'Kimberly Moore', 0),
(386, 'users', '438', 0, 'Brianna Rhodes', 0),
(387, 'users', '439', 0, 'Ashley Simpson', 0),
(388, 'users', '440', 0, 'Kearra Brinson', 0),
(389, 'users', '441', 0, 'Ashley Simpson', 0),
(390, 'users', '442', 0, 'Ariana  Chavis', 0),
(391, 'users', '443', 0, 'Megan Stanley', 0),
(392, 'users', '444', 0, 'Norma  Enciso', 0),
(393, 'users', '445', 0, 'Ifedolapo  Aderibigbe', 0),
(394, 'users', '446', 0, 'Jeehae Nam', 0),
(395, 'users', '447', 0, 'Jacob Argent', 0),
(396, 'users', '448', 0, 'Melvin Ford', 0),
(397, 'users', '449', 0, 'Jacob Argent', 0),
(398, 'users', '450', 0, 'Kiara Harvey', 0),
(399, 'users', '451', 0, 'nasha', 0),
(400, 'users', '452', 0, 'Sloan Whiteside', 0),
(401, 'users', '453', 0, 'erin', 0),
(402, 'users', '454', 0, 'Harvey  Coleman', 0),
(403, 'users', '455', 0, 'jacatria Mitchell', 0),
(404, 'users', '456', 0, 'Corey Young', 0),
(405, 'users', '457', 0, 'Terrance Caldwell', 0),
(406, 'users', '458', 0, 'dionne hills', 0),
(407, 'users', '459', 0, 'Nicchelle Carver Mackey', 0),
(408, 'users', '460', 0, 'Arnold Hill', 0),
(409, 'users', '461', 0, 'Candice Atwater', 0),
(410, 'users', '462', 0, 'kattie mcdonald', 0),
(411, 'users', '463', 0, 'LaToyoa Taylor', 0),
(412, 'users', '464', 0, 'Marisol Rodriguez', 0),
(413, 'users', '465', 0, 'Ovie Ruffin', 0),
(414, 'users', '466', 0, 'Tameaka Sutton', 0),
(415, 'users', '467', 0, 'Takita Dowdy', 0),
(416, 'users', '468', 0, 'Thomas Blandford', 0),
(417, 'users', '469', 0, 'keith quammie', 0),
(418, 'users', '470', 0, 'Kenneth Robinson', 0),
(419, 'users', '471', 0, 'Sabrina Smith', 0),
(420, 'users', '472', 0, 'Natasha Snell', 0),
(421, 'users', '473', 0, 'Joceyn  McLean', 0),
(422, 'users', '474', 0, 'larry jonson', 0),
(423, 'users', '475', 0, 'Ushima Johnson', 0),
(424, 'users', '476', 0, 'Gerald Bagley', 0),
(425, 'users', '477', 0, 'Sheerie Collins', 0),
(426, 'users', '478', 0, 'Umar  Muhammad', 0),
(427, 'users', '479', 0, 'Danyece Allen', 0),
(428, 'users', '480', 0, 'Janina', 0),
(429, 'users', '481', 0, 'ranisha wims', 0),
(430, 'users', '482', 0, 'Myesha  Wims', 0),
(431, 'users', '483', 0, 'Myesha Wims', 0),
(432, 'users', '484', 0, 'Darius Warren', 0),
(433, 'users', '485', 0, 'stephany fields', 0),
(434, 'users', '486', 0, 'Nashala Thompson.', 0),
(435, 'users', '487', 0, 'Shannon Suggs', 0),
(436, 'users', '488', 0, 'Tiah Cooper', 0),
(437, 'users', '489', 0, 'Jasmin Sudduth', 0),
(438, 'users', '490', 0, 'Ily Ruffin', 0),
(439, 'users', '491', 0, 'Brittany Mcfadden', 0),
(440, 'users', '492', 0, 'Shannon Suggs', 0),
(441, 'users', '493', 0, 'Destinee Miller', 0),
(442, 'users', '494', 0, 'Morgan Gaines', 0),
(443, 'users', '495', 0, 'Morgan Gaines', 0),
(444, 'users', '496', 0, 'Donta Cobb', 0),
(445, 'users', '497', 0, 'Sheterra Jones', 0),
(446, 'users', '498', 0, 'Paula Goldston', 0),
(447, 'users', '499', 0, 'Tiaunna Weaver', 0),
(448, 'users', '500', 0, 'Nefertari Little', 0),
(449, 'users', '501', 0, 'Mataya  Hardy', 0),
(450, 'users', '502', 0, 'ashley boone', 0),
(451, 'users', '503', 0, 'LaShonda Bacote', 0),
(452, 'users', '504', 0, 'Brianna Ford', 0),
(453, 'users', '505', 0, 'allena watson', 0),
(454, 'users', '506', 0, 'nasha Roberts', 0),
(455, 'users', '507', 0, 'keynada  baldwin', 0),
(456, 'users', '508', 0, 'Ashanti Farrington', 0),
(457, 'users', '509', 0, 'kansas harris', 0),
(458, 'users', '510', 0, 'ashley Boone', 0),
(459, 'users', '511', 0, 'reginald Hartman', 0),
(460, 'users', '512', 0, 'Langston Hawkins', 0),
(462, 'users', '514', 0, 'Jodie Tawiah', 0),
(463, 'users', '515', 0, 'COLBY BROOKS', 0),
(464, 'users', '516', 0, 'Grover  Lett', 0),
(465, 'users', '517', 0, 'Gabryell  Cullin', 0),
(466, 'users', '518', 0, 'Chibu Anyanwu', 0),
(467, 'users', '519', 0, 'Charmaine Young', 0),
(468, 'users', '520', 0, 'Amanda Alston', 0),
(818, 'users', '870', 0, 'tomciohomcio', 0),
(470, 'users', '522', 0, 'India Perkins', 0),
(471, 'users', '523', 0, 'Sadea Ferguson', 0),
(472, 'users', '524', 0, 'Tamiko Goss', 0),
(473, 'users', '525', 0, 'Nat Jolly', 0),
(474, 'users', '526', 0, 'Frank Lucienne', 0),
(475, 'users', '527', 0, 'Monique Williams', 0),
(476, 'users', '528', 0, 'Stevii Mills', 0),
(477, 'users', '529', 0, 'C.T. Unonu', 0),
(478, 'users', '530', 0, 'Dominique Camm', 0),
(479, 'users', '531', 0, 'James Smiley', 0),
(480, 'users', '532', 0, 'Denaro Allen', 0),
(481, 'users', '533', 0, 'Brittany Broussard', 0),
(482, 'users', '534', 0, 'Vakeeta Heron', 0),
(483, 'users', '535', 0, 'Latasha McMillan', 0),
(484, 'users', '536', 0, 'John Williams', 0),
(485, 'users', '537', 0, 'Jameshia Dixon', 0),
(486, 'users', '538', 0, 'LaKeisha Watson', 0),
(487, 'users', '539', 0, 'Terrell Ray', 0),
(488, 'users', '540', 0, 'Yanese King', 0),
(489, 'users', '541', 0, 'Desmon Peaks', 0),
(490, 'users', '542', 0, 'Rahiem Klugh', 0),
(491, 'users', '543', 0, 'Marc Duchatelier', 0),
(492, 'users', '544', 0, 'jay davies', 0),
(493, 'users', '545', 0, 'Aris Morris', 0),
(494, 'users', '546', 0, 'Jennifer Sunmonu', 0),
(495, 'users', '547', 0, 'Brittany Thomas', 0),
(496, 'users', '548', 0, 'Esiyena  Abebe', 0),
(497, 'users', '549', 0, 'Joy Elizabeth', 0),
(498, 'users', '550', 0, 'Esi Abe', 0),
(499, 'users', '551', 0, 'Jarrell Hunter', 0),
(500, 'users', '552', 0, 'Paul Stovel', 0),
(501, 'users', '553', 0, 'James Dozier', 0),
(502, 'users', '554', 0, 'T Mo', 0),
(503, 'users', '555', 0, 'Connell Smallwood', 0),
(504, 'users', '556', 0, 'Dominique Burchette', 0),
(505, 'users', '557', 0, 'nikki harris', 0),
(506, 'users', '558', 0, 'Allison Wright', 0),
(507, 'users', '559', 0, 'Richard Berryman', 0),
(508, 'users', '560', 0, 'Jeremy Knox', 0),
(509, 'users', '561', 0, 'Brittany Watson', 0),
(510, 'users', '562', 0, 'Levell Exum', 0),
(511, 'users', '563', 0, 'Lynda Gerbe', 0),
(512, 'users', '564', 0, 'Olivia  Garcia', 0),
(513, 'users', '565', 0, 'tarik webb', 0),
(514, 'users', '566', 0, 'Tara Hemmerly', 0),
(515, 'users', '567', 0, 'Carolyn Martin', 0),
(516, 'users', '568', 0, 'Julie Pitts', 0),
(517, 'users', '569', 0, 'Aishia Crayton', 0),
(518, 'users', '570', 0, 'Brandy Corbett', 0),
(519, 'users', '571', 0, 'Dominique Goodmond', 0),
(520, 'users', '572', 0, 'Newton Dennis', 0),
(521, 'users', '573', 0, 'Ashley Wike', 0),
(522, 'users', '574', 0, 'Latoya Henderson', 0),
(523, 'users', '575', 0, 'Patrick Williams', 0),
(524, 'users', '576', 0, 'Ron Williams', 0),
(525, 'users', '577', 0, 'Bull City Kappas', 0),
(526, 'users', '578', 0, 'Chelsea Suitt', 0),
(527, 'users', '579', 0, 'Gabrielle Pettiford', 0),
(528, 'users', '580', 0, 'Essence Sneed', 0),
(529, 'users', '581', 0, 'Anita D Baker', 0),
(530, 'users', '582', 0, 'Renaldo Dorsey', 0),
(531, 'users', '583', 0, 'Butch Fisher', 0),
(532, 'users', '584', 0, 'Eric Evans', 0),
(533, 'users', '585', 0, 'frederick tyson', 0),
(534, 'users', '586', 0, 'ivan@serviceplusrealty.com Owens', 0),
(535, 'users', '587', 0, 'Bryan Burns', 0),
(536, 'users', '588', 0, 'STEVEN WILLIAMS', 0),
(537, 'users', '589', 0, 'Harry Leak', 0),
(538, 'users', '590', 0, 'Edward Cook', 0),
(539, 'users', '591', 0, 'Mike Weaver', 0),
(540, 'users', '592', 0, 'Michael Boone', 0),
(541, 'users', '593', 0, 'Sal Webley', 0),
(542, 'users', '594', 0, 'Elbert Avery', 0),
(543, 'users', '595', 0, 'Keisha Clark', 0),
(544, 'users', '596', 0, 'Demetrius Harvey', 0),
(545, 'users', '597', 0, 'Rosalyn Smith', 0),
(546, 'users', '598', 0, 'Jessica Nipper', 0),
(547, 'users', '599', 0, 'KWAME WOODS', 0),
(548, 'users', '600', 0, 'Philip Dunston', 0),
(549, 'users', '601', 0, 'Barbara Wynn', 0),
(550, 'users', '602', 0, 'Ricky leathers', 0),
(551, 'users', '603', 0, 'jumaine person', 0),
(552, 'users', '604', 0, 'Maggie Respass', 0),
(553, 'users', '605', 0, 'Jaren Mack', 0),
(554, 'users', '606', 0, 'Liana Lewis', 0),
(555, 'users', '607', 0, 'Urel Mattis', 0),
(556, 'users', '608', 0, 'Garius  Thomas', 0),
(557, 'users', '609', 0, 'Joseph Eley', 0),
(558, 'users', '610', 0, 'Damon Jones-Way', 0),
(559, 'users', '611', 0, 'Maggie Respass', 0),
(560, 'users', '612', 0, 'Brad Rountree', 0),
(561, 'users', '613', 0, 'Jasmine Atkinson', 0),
(562, 'users', '614', 0, 'Liana Lewis', 0),
(563, 'users', '615', 0, 'Ana Rivas', 0),
(564, 'users', '616', 0, 'Dominique James', 0),
(565, 'users', '617', 0, 'JaNell Henry', 0),
(566, 'users', '618', 0, 'Essence Sneed', 0),
(567, 'users', '619', 0, 'Jasmine Shelton', 0),
(568, 'users', '620', 0, 'Quneshia Waddell', 0),
(569, 'users', '621', 0, 'Jasmine McClain', 0),
(570, 'users', '622', 0, 'Joy Johnson', 0),
(571, 'users', '623', 0, 'Leah Oruwari', 0),
(572, 'users', '624', 0, 'Angela  Cameron', 0),
(573, 'users', '625', 0, 'Jasmine Donaie', 0),
(574, 'users', '626', 0, 'Brandi Jones', 0),
(575, 'users', '627', 0, 'Esiyena  Abebe', 0),
(576, 'users', '628', 0, 'Deion Williams', 0),
(577, 'users', '629', 0, 'Ta\\''Chelle  Morris', 0),
(578, 'users', '630', 0, 'Katrina miller', 0),
(579, 'users', '631', 0, 'Shanelle Edmonds', 0),
(580, 'users', '632', 0, 'Jessica  Acito', 0),
(581, 'users', '633', 0, 'jaimie brown', 0),
(582, 'users', '634', 0, 'Veronica Person', 0),
(583, 'users', '635', 0, 'Tyesha Carrington', 0),
(584, 'users', '636', 0, 'Alexis  Leathers', 0),
(585, 'users', '637', 0, 'Fran Hamilton', 0),
(586, 'users', '638', 0, 'Essence Sneed', 0),
(587, 'users', '639', 0, 'Mariel Huger', 0),
(588, 'users', '640', 0, 'Stasha McMillan', 0),
(589, 'users', '641', 0, 'Amber Flowers', 0),
(590, 'users', '642', 0, 'Raven Pettiford', 0),
(591, 'users', '643', 0, 'jasmyn rumph', 0),
(592, 'users', '644', 0, 'Aleia holloway', 0),
(593, 'users', '645', 0, 'Samantha Lindsay', 0),
(594, 'users', '646', 0, 'Davida Guthrie', 0),
(595, 'users', '647', 0, 'Jerry  Brown', 0),
(596, 'users', '648', 0, 'Karissa Morris', 0),
(597, 'users', '649', 0, 'dashia goodlow', 0),
(598, 'users', '650', 0, 'Ashaleigh  Owens', 0),
(599, 'users', '651', 0, 'Quintessia lawrence', 0),
(600, 'users', '652', 0, 'Lauren Sinclair', 0),
(601, 'users', '653', 0, 'Portia Brewington', 0),
(602, 'users', '654', 0, 'Kelly  Winstead', 0),
(603, 'users', '655', 0, 'Dyonna Hooper', 0),
(608, 'users', '660', 0, 'kia johnson', 0),
(605, 'users', '657', 0, 'tracy beckford', 0),
(606, 'users', '658', 0, 'quintessia chavis', 0),
(607, 'users', '659', 0, 'Jasmine Anderson', 0),
(609, 'users', '661', 0, 'chanelle hester', 0),
(610, 'users', '662', 0, 'sheka lipscomb', 0),
(611, 'users', '663', 0, 'nae roberts', 0),
(612, 'users', '664', 0, 'Latasha stuckey', 0),
(613, 'users', '665', 0, 'travis joyner', 0),
(614, 'users', '666', 0, 'fofie acon', 0),
(615, 'users', '667', 0, 'fofie acon', 0),
(618, 'users', '670', 0, 'LaToya Gilbert', 0),
(620, 'users', '672', 0, 'Nichele Briggs', 0),
(621, 'users', '673', 0, 'Sheree Austin', 0),
(623, 'users', '675', 0, 'Debbie Browne', 0),
(624, 'users', '676', 0, 'herbert carty', 0),
(625, 'users', '677', 0, 'Jazmine Howard', 0),
(626, 'users', '678', 0, 'Quillie Coath', 0),
(627, 'users', '679', 0, 'Makeda Brooks', 0),
(628, 'users', '680', 0, 'Kimberly Johnson', 0),
(629, 'users', '681', 0, 'Joelle Cirimele', 0),
(630, 'users', '682', 0, 'Jonathan Jefferson', 0),
(631, 'users', '683', 0, 'qumy21748', 0),
(632, 'users', '684', 0, 'Bryan Stypmann', 0),
(633, 'users', '685', 0, 'chasity D', 0),
(634, 'users', '686', 0, 'Lisa McClain', 0),
(635, 'users', '687', 0, 'Shonita  Richardson', 0),
(636, 'users', '688', 0, 'Sutina Richardson', 0),
(637, 'users', '689', 0, 'Test Test', 0),
(639, 'users', '691', 0, 'maurice hines', 0),
(640, 'users', '692', 0, 'Amy Villegas-McCleave', 0),
(641, 'users', '693', 0, 'Kendyll Myles', 0),
(642, 'users', '694', 0, 'Ashley Weaver', 0),
(643, 'users', '695', 0, 'Stevie Lawrence', 0),
(644, 'users', '696', 0, 'Hayward Campbell', 0),
(645, 'users', '697', 0, 'Lorraine Erhunmwunsee', 0),
(646, 'users', '698', 0, 'Krystal Parker', 0),
(647, 'users', '699', 0, 'KEVIN WALKER', 0),
(648, 'users', '700', 0, 'Kristen Hunter', 0),
(649, 'users', '701', 0, 'Michelle McIlwain', 0),
(650, 'users', '702', 0, 'Marie McGee', 0),
(651, 'users', '703', 0, 'Adonnica  Rowland', 0),
(652, 'users', '704', 0, 'Jeffrey Davidson', 0),
(653, 'users', '705', 0, 'Crystal Meaders', 0),
(654, 'users', '706', 0, 'Damarus Sanders', 0),
(655, 'users', '707', 0, 'Marquita Wood', 0),
(656, 'users', '708', 0, 'Stacei Vick', 0),
(657, 'users', '709', 0, 'Quanita Byers', 0),
(658, 'users', '710', 0, 'Romeo Gamble', 0),
(659, 'users', '711', 0, 'Kiera Fuell', 0),
(660, 'users', '712', 0, 'Ahmed  Finoh', 0),
(661, 'users', '713', 0, 'Alexcia  Harris', 0),
(662, 'users', '714', 0, 'Zulmie  Boyd', 0),
(663, 'users', '715', 0, 'tamara brown', 0),
(664, 'users', '716', 0, 'Anisha Chatman', 0),
(665, 'users', '717', 0, 'adonnica rowland', 0),
(666, 'users', '718', 0, 'Danielle Hurdle', 0),
(667, 'users', '719', 0, 'Blakelee Barker', 0),
(668, 'users', '720', 0, 'shonda flagler', 0),
(669, 'users', '721', 0, 'Joy Herring', 0),
(670, 'users', '722', 0, 'Adam Keith', 0),
(671, 'users', '723', 0, 'Brandon  Riley', 0),
(672, 'users', '724', 0, 'Sade Williams', 0),
(673, 'users', '725', 0, 'Amanda Poole', 0),
(674, 'users', '726', 0, 'Crystal Mims', 0),
(675, 'users', '727', 0, 'Citeria McCaleb', 0),
(676, 'users', '728', 0, 'dominic barnes', 0),
(677, 'users', '729', 0, 'Steven Moore', 0),
(678, 'users', '730', 0, 'Aleshia Smith', 0),
(679, 'users', '731', 0, 'Cas Hooks', 0),
(680, 'users', '732', 0, 'Dwan Wigfall', 0),
(681, 'users', '733', 0, 'Briana Yarber', 0),
(682, 'users', '734', 0, 'Shameka Rhodes', 0),
(683, 'users', '735', 0, 'Latasha  Pharr', 0),
(684, 'users', '736', 0, 'Amber Brown', 0),
(685, 'users', '737', 0, 'Shacarra Taylor', 0),
(686, 'users', '738', 0, 'Jennifer Ham', 0),
(687, 'users', '739', 0, 'Sasha Vann', 0),
(688, 'users', '740', 0, 'Portia Gause', 0),
(689, 'users', '741', 0, 'Ashley Littleton', 0),
(690, 'users', '742', 0, 'Whitney Thomas', 0),
(691, 'users', '743', 0, 'April Silvera', 0),
(692, 'users', '744', 0, 'Dana Mosley', 0),
(693, 'users', '745', 0, 'David Bailey', 0),
(694, 'users', '746', 0, 'Norman  Epps', 0),
(695, 'users', '747', 0, 'Jason Hervey', 0),
(696, 'users', '748', 0, 'Justin Briggs', 0),
(697, 'users', '749', 0, 'Markeshia Wilkins', 0),
(698, 'users', '750', 0, 'Crystal Douglas', 0),
(699, 'users', '751', 0, 'JAMES PITTS', 0),
(700, 'users', '752', 0, 'Kristin Jones', 0),
(701, 'users', '753', 0, 'Patrick Hand', 0),
(702, 'users', '754', 0, 'Tony Pulley', 0),
(703, 'users', '755', 0, 'Kirsen Hammond', 0),
(704, 'users', '756', 0, 'crystal leftdwrige', 0),
(705, 'users', '757', 0, 'Brittany Robinson', 0),
(706, 'users', '758', 0, 'Aisha Brown', 0),
(707, 'users', '759', 0, 'Jeniqueka Bradley', 0),
(708, 'users', '760', 0, 'Mya  cross', 0),
(709, 'users', '761', 0, 'Monique Young', 0),
(710, 'users', '762', 0, 'Lauren Lee', 0),
(711, 'users', '763', 0, 'Ranita Bullock', 0),
(712, 'users', '764', 0, 'LaMonica Knowlin', 0),
(713, 'users', '765', 0, 'Alicia Ruffin', 0),
(714, 'users', '766', 0, 'Ashley Mercer', 0),
(715, 'users', '767', 0, 'Latosha Phillips', 0),
(716, 'users', '768', 0, 'Avon Seymore', 0),
(717, 'users', '769', 0, 'collin morrison', 0),
(718, 'users', '770', 0, 'Brittany Watson', 0),
(719, 'users', '771', 0, 'charletta carr', 0),
(720, 'users', '772', 0, 'Reba Carlton', 0),
(721, 'users', '773', 0, 'Devin Sanders', 0),
(722, 'users', '774', 0, 'Shawn Jones', 0),
(723, 'users', '775', 0, 'Denea Nriaka', 0),
(724, 'users', '776', 0, 'Amanda Elleby', 0),
(725, 'users', '777', 0, 'Jasmine Jeffries', 0),
(726, 'users', '778', 0, 'Keith Harmon', 0),
(727, 'users', '779', 0, 'Taurus Becton', 0),
(728, 'users', '780', 0, 'Alisha Shaw', 0),
(729, 'users', '781', 0, 'Angel Blackmon', 0),
(730, 'users', '782', 0, 'Shavonda Evans', 0),
(731, 'users', '783', 0, 'Kevin Allen', 0),
(732, 'users', '784', 0, 'Chiara Fuller', 0),
(733, 'users', '785', 0, 'Eboni Chavis', 0),
(734, 'users', '786', 0, 'Mia  Parker', 0),
(735, 'users', '787', 0, 'Jewel Black', 0),
(736, 'users', '788', 0, 'Jacob Bagley', 0),
(737, 'users', '789', 0, 'Kyha Williams', 0),
(738, 'users', '790', 0, 'Christina Littles', 0),
(739, 'users', '791', 0, 'LaToya Walters', 0),
(740, 'users', '792', 0, 'Brittany Royall', 0),
(741, 'users', '793', 0, 'lisa colvin', 0),
(742, 'users', '794', 0, 'Ebony  McQueen', 0),
(743, 'users', '795', 0, 'Lakishi Stevenson', 0),
(744, 'users', '796', 0, 'ranita bullock', 0),
(745, 'users', '797', 0, 'Jamila Thomas', 0),
(746, 'users', '798', 0, 'Tamara Moore', 0),
(747, 'users', '799', 0, 'Whitley  Grant', 0),
(748, 'users', '800', 0, 'amber Credle', 0),
(749, 'users', '801', 0, 'Terry Janifer', 0),
(750, 'users', '802', 0, 'Jazmine Godbold', 0),
(751, 'users', '803', 0, 'Jazmine Godbold', 0),
(752, 'users', '804', 0, 'Eva Penny', 0),
(753, 'users', '805', 0, 'Hafiz Stokes', 0),
(754, 'users', '806', 0, 'Jamie  Lewis', 0),
(755, 'users', '807', 0, 'Ricky Leathers', 0),
(756, 'users', '808', 0, 'Andrain Horton', 0),
(757, 'users', '809', 0, 'Amber Bradshaw', 0),
(758, 'users', '810', 0, 'Josiah Fapojuwo', 0),
(759, 'users', '811', 0, 'felishia ward', 0),
(760, 'users', '812', 0, 'Ashley Mason', 0),
(761, 'users', '813', 0, 'Shaunti  Crawford', 0),
(762, 'users', '814', 0, 'Rakia  Hinnant', 0),
(763, 'users', '815', 0, 'Chinasa Nworu', 0),
(764, 'users', '816', 0, 'Joslyn Bloomfield', 0),
(765, 'users', '817', 0, 'Tiffany Honeysucker', 0),
(766, 'users', '818', 0, 'Haminat  Oladipo', 0),
(767, 'users', '819', 0, 'Kristen Bell Hughes', 0),
(768, 'users', '820', 0, 'Alexia Forte', 0),
(769, 'users', '821', 0, 'Krystal Armstrong', 0),
(770, 'users', '822', 0, 'travis  irving', 0),
(771, 'users', '823', 0, 'Veronica Fleming', 0),
(772, 'users', '824', 0, 'Christopher  Rawlinson', 0),
(773, 'users', '825', 0, 'keyon havner', 0),
(774, 'users', '826', 0, 'Danielle Haynes', 0),
(775, 'users', '827', 0, 'LaTasha Miller', 0),
(776, 'users', '828', 0, 'Janae Freeman', 0),
(777, 'users', '829', 0, 'Steven Harrison', 0),
(778, 'users', '830', 0, 'Jennifer  Green', 0),
(779, 'users', '831', 0, 'Donata smith', 0),
(780, 'users', '832', 0, 'Keith Roberson', 0),
(781, 'users', '833', 0, 'Chris Mayshack', 0),
(782, 'users', '834', 0, 'Simranjeet singh', 0),
(783, 'users', '835', 0, 'Linda Phoenix Rising Green', 0),
(784, 'users', '836', 0, 'Tamara Sherman', 0),
(785, 'users', '837', 0, 'Christine McQueen', 0),
(786, 'users', '838', 0, 'vanessa olton', 0),
(787, 'users', '839', 0, 'D Amen Garrison', 0),
(788, 'users', '840', 0, 'Juana Jackson', 0),
(789, 'users', '841', 0, 'Dennis Gavin', 0),
(790, 'users', '842', 0, 'Lawrence Reese', 0),
(791, 'users', '843', 0, 'roderick byrd', 0),
(792, 'users', '844', 0, 'Emanuel Byers', 0),
(793, 'users', '845', 0, 'Tabitha  Jasper', 0),
(794, 'users', '846', 0, 'Heru Ifagbemi', 0),
(795, 'users', '847', 0, 'Sonja Akins', 0),
(796, 'users', '848', 0, 'noob killer', 0),
(797, 'users', '849', 0, 'Tenisha Mayfield', 0),
(798, 'users', '850', 0, 'Rakeva Chavis', 0),
(799, 'users', '851', 0, 'Chasity Connelly', 0),
(800, 'users', '852', 0, 'Hannah  Irvin', 0),
(801, 'users', '853', 0, 'Seneca Clark', 0),
(802, 'users', '854', 0, 'Dominick Francois', 0),
(803, 'users', '855', 0, 'Seneca Clark', 0),
(804, 'users', '856', 0, 'collin rodgers', 0),
(805, 'users', '857', 0, 'venessa rodgers', 0),
(806, 'users', '858', 0, 'venessa rodgers', 0),
(807, 'users', '859', 0, 'Jose Hall', 0),
(808, 'users', '860', 0, 'Phil Allen', 0),
(809, 'users', '861', 0, 'Jamila Bey', 0),
(810, 'users', '862', 0, 'Maxine Miles', 0),
(811, 'users', '863', 0, 'Ronald Moses', 0),
(812, 'users', '864', 0, 'Barbara Brown', 0),
(813, 'users', '865', 0, 'Dipen Patel', 0),
(814, 'users', '866', 0, 'Nine Hertz', 0),
(815, 'users', '867', 0, 'William Kelly', 0),
(816, 'users', '868', 0, 'kapil kumar', 0),
(840, 'users', '892', 0, 'vijay ameta', 0),
(819, 'users', '871', 0, 'thomasteofilas', 0),
(820, 'users', '872', 0, 'Nefertarei  El Bey', 0),
(821, 'users', '873', 0, 'Tamara Brown', 0),
(822, 'users', '874', 0, 'Rohan Singh', 0),
(823, 'users', '875', 0, 'allen edwards', 0),
(824, 'users', '876', 0, 'Arthea   Perry', 0),
(825, 'users', '877', 0, 'Donna Howard', 0),
(826, 'users', '878', 0, 'Darrick Baldwin', 0),
(827, 'users', '879', 0, 'Titus Robinson', 0),
(828, 'users', '880', 0, 'Kim Gladden', 0),
(829, 'users', '881', 0, 'Ebony Fairwell', 0),
(830, 'users', '882', 0, 'Queen B-More', 0),
(831, 'users', '883', 0, 'Ivey M', 0),
(832, 'users', '884', 0, 'Thoth Amun El', 0),
(833, 'users', '885', 0, 'Monica Bentley', 0),
(834, 'users', '886', 0, 'dar dar', 0),
(835, 'users', '887', 0, 'Ashish Goswami', 0),
(836, 'users', '888', 0, 'Test picture', 0),
(837, 'users', '889', 0, 'test pic2', 0),
(879, 'users', '931', 0, 'qwertiudjy djhvil', 0),
(841, 'users', '893', 0, 'mark lord', 0),
(842, 'users', '894', 0, 'martin josh', 0),
(843, 'users', '895', 0, 'david hussy', 0),
(844, 'users', '896', 0, 'dev raj', 0),
(845, 'users', '897', 0, 'Da Da', 0),
(846, 'users', '898', 0, 'Dan Brown', 0),
(847, 'users', '899', 0, 'manish agrawal', 0),
(854, 'users', '906', 0, 'final mon', 0),
(855, 'users', '907', 0, 'etsy hs', 0),
(856, 'users', '908', 0, 'whsjs gz', 0),
(857, 'users', '909', 0, 'asdf asdf', 0),
(858, 'users', '910', 0, 'Da Da', 0),
(859, 'users', '911', 0, 'Ming Sup', 0),
(860, 'users', '912', 0, 'rekon test', 0),
(861, 'users', '913', 0, 'James Shepard', 0),
(862, 'users', '914', 0, 'Asr Amun', 0),
(864, 'users', '916', 0, 'Kapil Kumar', 0),
(865, 'users', '917', 0, 'Rahul Kumawat', 0),
(866, 'users', '918', 0, 'Er Devraj Gurjar', 0),
(869, 'users', '921', 0, 'Anveshan Singh', 0),
(878, 'users', '890', 0, 'Hitesh Shrimali', 0),
(875, 'users', '927', 0, 'Manish Agrawal', 0),
(876, 'users', '928', 0, 'Amit Rai', 0),
(880, 'users', '932', 0, 'ramniwas dusad', 0),
(881, 'users', '933', 0, 'ken jacops', 0),
(882, 'users', '934', 0, 'nick jacops', 0),
(883, 'users', '935', 0, 'ken jacops', 0),
(884, 'users', '936', 0, 'tester tester', 0),
(885, 'users', '937', 0, 'ken jacops', 0),
(886, 'users', '938', 0, 'kirsten Quirynen', 0),
(887, 'users', '1', 0, 'test test', 0),
(888, 'users', '2', 0, 'vijay ameta', 0),
(889, 'users', '3', 0, 'vijay ameta', 0),
(890, 'users', '4', 0, 'manish jain', 0),
(891, 'users', '5', 0, 'John Joel', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_core_acl_aro_groups`
--

CREATE TABLE IF NOT EXISTS `jos_core_acl_aro_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `jos_gacl_parent_id_aro_groups` (`parent_id`),
  KEY `jos_gacl_lft_rgt_aro_groups` (`lft`,`rgt`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `jos_core_acl_aro_groups`
--

INSERT INTO `jos_core_acl_aro_groups` (`id`, `parent_id`, `name`, `lft`, `rgt`, `value`) VALUES
(17, 0, 'ROOT', 1, 22, 'ROOT'),
(28, 17, 'USERS', 2, 21, 'USERS'),
(29, 28, 'Public Frontend', 3, 12, 'Public Frontend'),
(18, 29, 'Registered', 4, 11, 'Registered'),
(19, 18, 'Author', 5, 10, 'Author'),
(20, 19, 'Editor', 6, 9, 'Editor'),
(21, 20, 'Publisher', 7, 8, 'Publisher'),
(30, 28, 'Public Backend', 13, 20, 'Public Backend'),
(23, 30, 'Manager', 14, 19, 'Manager'),
(24, 23, 'Administrator', 15, 18, 'Administrator'),
(25, 24, 'Super Administrator', 16, 17, 'Super Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `jos_core_acl_aro_map`
--

CREATE TABLE IF NOT EXISTS `jos_core_acl_aro_map` (
  `acl_id` int(11) NOT NULL DEFAULT '0',
  `section_value` varchar(230) NOT NULL DEFAULT '0',
  `value` varchar(100) NOT NULL,
  PRIMARY KEY (`acl_id`,`section_value`,`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_core_acl_aro_sections`
--

CREATE TABLE IF NOT EXISTS `jos_core_acl_aro_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(230) NOT NULL DEFAULT '',
  `order_value` int(11) NOT NULL DEFAULT '0',
  `name` varchar(230) NOT NULL DEFAULT '',
  `hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `jos_gacl_value_aro_sections` (`value`),
  KEY `jos_gacl_hidden_aro_sections` (`hidden`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_core_acl_groups_aro_map`
--

CREATE TABLE IF NOT EXISTS `jos_core_acl_groups_aro_map` (
  `group_id` int(11) NOT NULL DEFAULT '0',
  `section_value` varchar(240) NOT NULL DEFAULT '',
  `aro_id` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `group_id_aro_id_groups_aro_map` (`group_id`,`section_value`,`aro_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_core_acl_groups_aro_map`
--

INSERT INTO `jos_core_acl_groups_aro_map` (`group_id`, `section_value`, `aro_id`) VALUES
(18, '', 887),
(18, '', 888),
(18, '', 889),
(18, '', 890),
(18, '', 891);

-- --------------------------------------------------------

--
-- Table structure for table `jos_users`
--

CREATE TABLE IF NOT EXISTS `jos_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(150) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `usertype` varchar(25) NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `gid` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `fb_id` varchar(200) NOT NULL,
  `followpermission` int(1) NOT NULL DEFAULT '0',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usertype` (`usertype`),
  KEY `idx_name` (`name`),
  KEY `gid_block` (`gid`,`block`),
  KEY `username` (`username`),
  KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `jos_users`
--

INSERT INTO `jos_users` (`id`, `name`, `username`, `email`, `password`, `usertype`, `block`, `sendEmail`, `gid`, `fb_id`, `followpermission`, `registerDate`, `lastvisitDate`, `activation`, `params`) VALUES
(1, 'test test', 'test', 'ramniwas@ninehertzindia.com', 'ecc5c3298c039ea96931df6a37e1a5cc:cKVoxVBZt9lvTBGQOAeuQCOQEmecb2Un', 'Registered', 0, 0, 18, '', 0, '2014-08-04 05:31:08', '0000-00-00 00:00:00', '9m5CZMhS2sgbKrmBAFvxUOFBOxG8Fqs0', 'update_cache_list=1'),
(2, 'vijay ameta', 'vijay1231', 'ameta.vijay7861@gmail.com', 'a28be6519d297964481529c2b0f9a4ba:Q0Rx4KZCJVUHShvefo6UVGWxbZdxCtor', 'Registered', 0, 0, 18, '', 0, '2014-08-04 05:38:25', '0000-00-00 00:00:00', 'Myp6Vkq4MacokCU1bYtueFOjkXS9kZIQ', 'update_cache_list=1'),
(3, 'vijay ameta', 'vijay123', 'ameta.vijay786@gmail.com', '62931e7e95959211281f0b2d7b3d2bcd:nsgjTsAIzwz5beuKLUQCoy1MprReThL4', 'Registered', 0, 0, 18, '', 0, '2014-08-04 05:39:40', '0000-00-00 00:00:00', 'b4d5jlDT0U3DTfnbD1AuFQCsK9YSmIYp', 'update_cache_list=1'),
(4, 'manish jain', 'manijain333', 'manijain333@gmail.com', 'c1a84f38402ecf2f07a70d5dd5f8ad66:4q77WbQBBdpihF1qhgwVas0ncovjDjCI', 'Registered', 0, 0, 18, '', 1, '2014-08-04 06:54:36', '0000-00-00 00:00:00', 'Em88ZJV0cl6xDvLCCKZ1C3PpEqQDA4C6', 'update_cache_list=1'),
(5, 'John Joel', 'johnjoel', 'testtesting308@gmail.com', '', 'Registered', 0, 0, 18, '1536233753264424', 0, '2014-08-04 12:44:10', '0000-00-00 00:00:00', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `last_seen`
--

CREATE TABLE IF NOT EXISTS `last_seen` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `userid` int(8) NOT NULL,
  `totallastseen` int(6) NOT NULL,
  `lastseendate` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `last_seen`
--

INSERT INTO `last_seen` (`id`, `userid`, `totallastseen`, `lastseendate`) VALUES
(1, 3, 5, '2014-08-04'),
(2, 4, 0, '2014-08-04');

-- --------------------------------------------------------

--
-- Table structure for table `notification_comment`
--

CREATE TABLE IF NOT EXISTS `notification_comment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mainid` int(10) NOT NULL,
  `userid` int(10) NOT NULL,
  `ref_id` int(10) NOT NULL,
  `msg` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `type` varchar(50) NOT NULL,
  `posttype` enum('PHOTO','ALBUM','COLLAGE') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'PHOTO',
  `commentid` int(10) NOT NULL,
  `dtdate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `notification_comment`
--

INSERT INTO `notification_comment` (`id`, `mainid`, `userid`, `ref_id`, `msg`, `status`, `type`, `posttype`, `commentid`, `dtdate`) VALUES
(1, 4, 4, 3, 'nice image brother', 1, 'COMMENT', 'PHOTO', 1, '2014-08-04 12:16:26'),
(2, 2, 4, 3, 'hello brother nice image and album...!', 1, 'COMMENT', 'ALBUM', 2, '2014-08-04 12:17:38');

-- --------------------------------------------------------

--
-- Table structure for table `notification_other`
--

CREATE TABLE IF NOT EXISTS `notification_other` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mainid` int(10) NOT NULL,
  `userid` int(10) NOT NULL,
  `ref_id` int(10) NOT NULL,
  `msg` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('FOLLOW','FACEBOOK') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'FOLLOW',
  `status` int(1) NOT NULL DEFAULT '1',
  `dtdate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `notification_other`
--

INSERT INTO `notification_other` (`id`, `mainid`, `userid`, `ref_id`, `msg`, `type`, `status`, `dtdate`) VALUES
(1, 3, 4, 1, '', 'FOLLOW', 1, '2014-08-04 11:55:14'),
(2, 4, 3, 2, '99', 'FOLLOW', 1, '2014-08-04 12:15:04'),
(3, 5, 5, 1, '', 'FACEBOOK', 1, '2014-08-04 12:44:11'),
(4, 5, 5, 2, '', 'FACEBOOK', 1, '2014-08-04 12:44:11'),
(5, 5, 5, 3, '', 'FACEBOOK', 1, '2014-08-04 12:44:11'),
(6, 5, 5, 4, '', 'FACEBOOK', 1, '2014-08-04 12:44:11');

-- --------------------------------------------------------

--
-- Table structure for table `notification_post`
--

CREATE TABLE IF NOT EXISTS `notification_post` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mainid` int(10) NOT NULL,
  `userid` int(10) NOT NULL,
  `ref_id` int(10) NOT NULL,
  `msg` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('LIKE','POST','TAG','PRIVATE') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'LIKE',
  `allow_follower` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1',
  `posttype` enum('PHOTO','ALBUM','COLLAGE','OTHER') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'OTHER',
  `dtdate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `notification_post`
--

INSERT INTO `notification_post` (`id`, `mainid`, `userid`, `ref_id`, `msg`, `type`, `allow_follower`, `status`, `posttype`, `dtdate`) VALUES
(1, 4, 3, 1, '', 'LIKE', 0, 1, 'PHOTO', '2014-08-04 11:51:21'),
(2, 3, 3, 2, '', 'LIKE', 0, 1, 'PHOTO', '2014-08-04 11:52:24'),
(3, 3, 4, 2, '', 'LIKE', 0, 1, 'PHOTO', '2014-08-04 12:16:01'),
(4, 4, 4, 1, '', 'LIKE', 0, 1, 'PHOTO', '2014-08-04 12:16:10'),
(5, 5, 3, 3, '', 'LIKE', 0, 1, 'PHOTO', '2014-08-04 13:10:24'),
(6, 6, 3, 4, '', 'LIKE', 0, 1, 'PHOTO', '2014-08-04 13:56:20');

-- --------------------------------------------------------

--
-- Table structure for table `photohashtag`
--

CREATE TABLE IF NOT EXISTS `photohashtag` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `photoid` int(10) NOT NULL,
  `albumid` int(10) NOT NULL,
  `userid` int(10) NOT NULL,
  `hashtag` varchar(100) DEFAULT NULL,
  `permission` int(2) NOT NULL DEFAULT '0',
  `status` varchar(20) DEFAULT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'PHOTOS',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `photohashtag`
--

INSERT INTO `photohashtag` (`id`, `photoid`, `albumid`, `userid`, `hashtag`, `permission`, `status`, `type`) VALUES
(1, -1, 4, 3, 'brother', 0, NULL, 'ALBUMS'),
(2, 7, 4, 3, 'cute', 0, NULL, 'PHOTOS'),
(3, 7, 4, 3, 'dog', 0, NULL, 'PHOTOS'),
(4, -1, 5, 3, 'vijay', 0, NULL, 'ALBUMS');

-- --------------------------------------------------------

--
-- Table structure for table `pictures`
--

CREATE TABLE IF NOT EXISTS `pictures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_confirm`
--

CREATE TABLE IF NOT EXISTS `user_confirm` (
  `userid` int(10) DEFAULT NULL,
  `code` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `dtdate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_token`
--

CREATE TABLE IF NOT EXISTS `user_token` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `user_id` int(8) NOT NULL,
  `deviceid` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `likes` int(1) NOT NULL DEFAULT '1',
  `comment` int(1) NOT NULL DEFAULT '1',
  `follow` int(1) NOT NULL DEFAULT '1',
  `facebook` int(1) NOT NULL DEFAULT '1',
  `mention` int(1) NOT NULL DEFAULT '1',
  `post` int(11) NOT NULL DEFAULT '1',
  `bedge` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_token`
--

INSERT INTO `user_token` (`id`, `user_id`, `deviceid`, `likes`, `comment`, `follow`, `facebook`, `mention`, `post`, `bedge`) VALUES
(1, 3, '998ea71ad53ca2e1c902a6fd0c29e6433c5e3fef7f44e1d8a2d9541ec985ed86', 1, 1, 1, 1, 1, 1, 0),
(2, 5, '998ea71ad53ca2e1c902a6fd0c29e6433c5e3fef7f44e1d8a2d9541ec985ed86', 1, 1, 1, 1, 1, 1, 0);


-- --------------------------------------------------------

--
-- Table structure for table `zip_code`
--

CREATE TABLE IF NOT EXISTS `zip_code` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `zip_code` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `city` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `county` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `state_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `state_prefix` varchar(2) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `area_code` varchar(3) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `time_zone` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `lat` float NOT NULL,
  `lon` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zip_code` (`zip_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure for view `view_friends`
--


CREATE ALGORITHM=UNDEFINED DEFINER=`heminfo`@`localhost` SQL SECURITY DEFINER VIEW `view_friends` AS select `jos_community_connection`.`connect_from` AS `userid`,`jos_community_connection`.`connect_to` AS `friend_id` from `jos_community_connection` where ((`jos_community_connection`.`status` = 1) and (not(`jos_community_connection`.`connect_to` in (select `jos_community_blocklist`.`blocked_userid` from `jos_community_blocklist`)))) union all select `jos_community_connection`.`connect_to` AS `userid`,`jos_community_connection`.`connect_from` AS `friend_id` from `jos_community_connection` where ((`jos_community_connection`.`status` = 1) and (not(`jos_community_connection`.`connect_from` in (select `jos_community_blocklist`.`blocked_userid` from `jos_community_blocklist`))));

create or replace view `view_photos` as SELECT A.id AS albumsid, A.photoid AS coverid,A.permissions, A.name AS albumsname, A.description AS aldescription, A.created AS dtdate, A.creator AS userid, A.path AS albumpath, B.thumbnail,allow_follower FROM jos_community_photos_albums A inner JOIN jos_community_photos B ON A.photoid = B.id;

create or replace view `view_totallikes` as SELECT uid, element,`like`,if(LENGTH(`like`)>=1,sum(LENGTH(`like`) - LENGTH( REPLACE( `like`, ',', '')) + 1),0) as total_likes FROM jos_community_likes where element in ('photo','album','photo.album') GROUP BY uid,element;



create or replace view `view_userpost` AS select id,creator as userid,"ALBUM" as type,created from jos_community_photos_albums UNION ALL select id,creator as userid,"PHOTO" as type,created from jos_community_photos;

create or replace view `view_friends` as select connect_from as userid,connect_to as friend_id from jos_community_connection  where status=1 and connect_to not in(select blocked_userid from jos_community_blocklist)   UNION ALL select  connect_to as userid,connect_from as friend_id from jos_community_connection where status=1 and connect_from not in(select blocked_userid from jos_community_blocklist);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
