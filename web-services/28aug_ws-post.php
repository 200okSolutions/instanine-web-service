<?php
include("config.php");
include("image_functions.php");
include("functions.php");
include("classes/follow.class.php");
include("classes/push-notification.class.php");
?>

<?php
///web-services/ws-post.php?type=album&data=[{"userid":"946","name":"view","desc":"hii","type":"user","hits":"0","permission":"0","allowfollower":"1"}]
//$URL=web-services/ws-post.php?type=getphoto&data=[{"userid":"1","albumid":"1","photoid":"1"}]
//web-services/ws-post.php?type=like&data=[{"userid":"881", "photoid":"363", "like":"1","posttype":"photo"}]
//web-services/ws-post.php?type=topphoto&data=[{"userid":"881"}]
//web-services/ws-post.php?type=list&data=[{"userid":"881","pageno":"1"}]
//web-services/ws-post.php?type=likeuser&data=[{"userid":"881", "photoid":"363","posttype":"photo"}]
//web-services/ws-post.php?type=delete&data=[{"userid":"881", "postid":"363","posttype":"photo"}]
//web-services/ws-post.php?type=collagecomment&data=[{"userid":"880","albumid":"296"}]
function tag($string)
{
	  $string = str_replace ( "(", ' ', $string );
	  $string = str_replace ( ')', ' ', $string );
	  $string = str_replace ( '[', '', $string );
	  $string = str_replace ( ']', '', $string );
	  $string = str_replace ( '"', '', $string );
	  $string = str_replace ( '\n', '', $string );
	  $string = str_replace ( "n  ", '', $string );
	  $string = str_replace ( '\ ', '', $string );
	  $string = str_replace ( '\\', '', $string );
	  return $string;
}

function removeCommaArr($string)
{
	  $string = str_replace ( '"', '', $string );
	  $string = str_replace ( '[', '', $string );
	  $string = str_replace ( ']', '', $string );
	 
	  return $string;
}

function createDir($path){
	return ((!is_dir($path)) ? @mkdir($path, 0777):TRUE); 
}
function escapeChars($value){
	if (is_array($value))
		$value = array_map('security', $value);
	else
		$value = mysql_real_escape_string($value);
	
	return $value;
}

function unEscapeChars($value){
	if (is_array($value))
		$value = array_map('security', $value);
	else
		$value = stripslashes($value);
	
	return $value;
}
function addNewAlbum($userid,$name,$desc,$hits,$permission,$allow_follower,$alowuser,$inviteuser,$albumhashtag){
	global $mainlink;
	global $db;
	$status			=	"false";
	/*
	$arr=array("message"=>$msg, "status"=>"true", "albumid"=>"322", "albumurl"=>$mainlink.$userName.'/photos/'.adddash($name));
	return $arr;
	*/
	
	if($userid>0){
  	

			$path = '../images/photos/'.$userid.'/';
			$path2 = 'images/photos/'.$userid.'/';
			if(!is_dir($path)){
				mkdir($path, 0777);
			}
			
			if($permission == 1)
				$permission	=	40;
			else
				$permission	=	0;
			
			$userinfo		=	userInfo($userid);
			
			$userName		=	$userinfo['username'];
			
			 $insetAlbum	=	"INSERT INTO jos_community_photos_albums SET ".
								" creator		=	'".$userid."', ".
								" name			=	'".$name."', ".
								" description	=	'".$desc."', ".
								" path			=	'".$path2."', ".
								" type			=	'user', ".
								" permissions	=	'".$permission."', ".
								" allow_follower =	'".$allow_follower."', ".
								" hits			=	'".$hits."', ".
								" updated		=	'".nowDateTime()."', ".
								" created		=	'".nowDateTime()."'";
	
			mysql_query($insetAlbum);
			$lastid = mysql_insert_id();
			//postNotification($userid,"ALBUM",$permission,$lastid);
			checkAlbumDetailTable();
			
			if(count($inviteuser) > 0)
			{
				foreach($inviteuser as $val)
				{
					addPostNotification($userid,$lastid,$val,"","TAG","ALBUM",$allow_follower);
				}
			}
			//if($allow_follower > 0)
			{
				//$alowuserInfo	=	explode(",",$alowuser);	
				if(count($alowuser) > 0)
				{
					foreach($alowuser as $val)
					{
						$user	=	intval(removeCommaArr($val));
						addPostNotification($userid,$lastid,$user,"","PRIVATE","ALBUM",$allow_follower);
						
						if($user > 0)
						{
							mysql_query("DELETE FROM `comment_collage` WHERE `albumid`= '".$lastid."' AND `userid` = '".$user."'");
							$insetuser	=	" INSERT INTO albumdetail SET ".
											" albumid		=	'".$lastid."', ".
											" userid		=	'".$user."'";
		
							mysql_query($insetuser);
						}
					}
				}
			}
			
			if(count($albumhashtag) > 0)
				saveHashTagOnPhoto('-1',$lastid,$userid,$albumhashtag,'ALBUMS');
			
			postNotificationAlbum($lastid,$userid,$allow_follower);
			$status		=	"true";
			$msg		=	"You have successfully created album";
			
			$oldurl = $userName.'/photos/'.$lastid;
			$newurl = 'index.php?option=com_community&Itemid=235&albumid='.$lastid.'&lang=en&task=photo&userid='.$userid.'&view=photos';
			saveAlbumurl($newurl,$oldurl);
			
			$oldurlp = 'photos/'.$lastid;
			$newurlp = 'index.php?option=com_community&Itemid=235&albumid='.$lastid.'&lang=en&task=album&view=photos';
			saveAlbumurlphoto($newurlp,$oldurlp);
			
			$oldurlw = 'photos/'.$lastid.'/app/walls';
			$newurlw = 'index.php?option=com_community&Itemid=235&albumid='.$lastid.'&app=walls&lang=en&task=app&view=photos';
			saveAlbumurlwall($newurlw,$oldurlw);
	}else{
		
		$msg		=	"Error please try again";
		$status="false";
		
	}
	$arr=array("message"=>$msg, "status"=>$status, "albumid"=>$lastid, "albumurl"=>$mainlink.$userName.'/photos/'.$lastid);
	return $arr;
	
	}

function saveHashTagOnPhoto($photoid,$albumID,$userid,$hashArr,$permission=0,$type="PHOTOS"){
	global $db;
	if (count($hashArr)>0){
		foreach($hashArr as $val)
			{
			if($val	!=	'')
			{
				$val =str_replace("(","",$val);
				$val =str_replace(")","",$val);
				$sql1	=	" INSERT INTO photohashtag SET ".
				" photoid		=	'".$photoid."',".
				" albumid		=	'".$albumID."',".
				" userid		=	'".$userid."',".
				" hashtag		=	'".trim($val)."',".
				" permission	=	'".$permission."',".
				" type			=	'".$type."'";
				$db->query($sql1);
			}
		}
	}
}

function addNewPost($rs,$imgName){
	
		global $mainlink;
		global $db;
		$status			=	"false";
		$msg			=	'';
		$photoId		=	'';
		
		$albumid		=	intval($rs['albumid']);
		$userid			=	intval($rs['creator']);
		$addtype		=	strtoupper($rs['addtype']);
		
		
		if ($userid=="" || $userid<=0) {
			$msg="Invalid selected user";
		}else
		{
			if($addtype == "SINGLEADD")
			{
				$aluserid	=	getAlbumUserId($albumid);
				$permission	=	getAlbumPermission($albumid);
				postNotificationPic($userid,"PHOTOS",$aluserid,'');
			}else
			{
				if(count($imgName) > 0)
				{
					$name		=	specialCharRemove(trim($rs['name']));
					$desc		=	specialCharRemove(trim($rs['desc']));
					$hits		=	(trim($rs['hits']));
					$alowuser	=	json_decode(stripslashes($rs['alowuser']));
					$permission		=	strtoupper(trim($rs['permission']));
					$allow_follower	=	strtoupper(trim($rs['allowfollower']));
					$inviteuser		=	json_decode(stripslashes($rs['inviteuser']));
					$albumhashtag	=	json_decode(stripslashes($rs['albumhashtag']));

					$mergeprivate	=	strtoupper(trim($rs['mergeprivate']));
					$allowmergeuser	=	json_decode(stripslashes($rs['allowmergeuser']));
					
					$path = '../images/photos/'.$userid.'/';
					$path2 = 'images/photos/'.$userid.'/';
					if(!is_dir($path)){
						mkdir($path, 0777);
					}
					
					if($permission == 1)
						$permission	=	40;
					else
						$permission	=	0;
					
					$userinfo		=	userInfo($userid);
					
					$userName		=	$userinfo['username'];
					
					 $insetAlbum	=	"INSERT INTO jos_community_photos_albums SET ".
										" creator		=	'".$userid."', ".
										" name			=	'".$name."', ".
										" description	=	'".$desc."', ".
										" path			=	'".$path2."', ".
										" type			=	'user', ".
										" permissions	=	'".$permission."', ".
										" allow_follower =	'".$allow_follower."', ".
										" hits			=	'".$hits."', ".
										" merge_private	=	'".$mergeprivate."', ".
										" updated		=	'".nowDateTime()."', ".
										" created		=	'".nowDateTime()."'";
			
					mysql_query($insetAlbum);
					$lastid = mysql_insert_id();
					//postNotification($userid,"ALBUM",$permission,$lastid);
					checkAlbumDetailTable();
					$nonfolowerlist	=	array();
					if(count($inviteuser) > 0)
					{
						foreach($inviteuser as $val)
						{
							$user	=	getUserId($val);
							if($user > 0)
							{
								$sqlfow="select * from jos_community_connection  where status=1 AND connect_to=".$userid."  and connect_from = ".$user;

								$resultfow=$db->query($sqlfow);
								if ($resultfow->size()>0)
								{
									addPostNotification($userid,$lastid,$user,"","TAG","ALBUM",$allow_follower);
								}else
								{
									$nonfolowerlist[] = $user;
								}
							}
						}
					}
					//print_r($nonfolowerlist);die;
					//if($allow_follower > 0)
					{
						//$alowuserInfo	=	explode(",",$alowuser);	
						if(count($alowuser) > 0)
						{
							foreach($alowuser as $val)
							{
								$user	=	intval(removeCommaArr($val));
								addPostNotification($userid,$lastid,$user,"","PRIVATE","ALBUM",$allow_follower);
								
								if($user > 0)
								{
									mysql_query("DELETE FROM `comment_collage` WHERE `albumid`= '".$lastid."' AND `userid` = '".$user."'");
									$insetuser	=	" INSERT INTO albumdetail SET ".
													" albumid		=	'".$lastid."', ".
													" userid		=	'".$user."'";
				
									mysql_query($insetuser);
								}
							}
						}

						if(count($allowmergeuser) > 0)
						{
							foreach($allowmergeuser as $val)
							{
								$user	=	intval(removeCommaArr($val));
								addPostNotification($userid,$lastid,$user,"","MERGE","ALBUM",$allow_follower);
							}
						}
					}
					
					if(count($albumhashtag) > 0)
						saveHashTagOnPhoto('-1',$lastid,$userid,$albumhashtag,$permission,'ALBUMS');
					
					postNotificationAlbum($lastid,$userid,$allow_follower,$nonfolowerlist);
					
					$oldurl = $userName.'/photos/'.$lastid;
					$newurl = 'index.php?option=com_community&Itemid=235&albumid='.$lastid.'&lang=en&task=photo&userid='.$userid.'&view=photos';
					saveAlbumurl($newurl,$oldurl);
					
					$oldurlp = 'photos/'.$lastid;
					$newurlp = 'index.php?option=com_community&Itemid=235&albumid='.$lastid.'&lang=en&task=album&view=photos';
					saveAlbumurlphoto($newurlp,$oldurlp);
					
					$oldurlw = 'photos/'.$lastid.'/app/walls';
					$newurlw = 'index.php?option=com_community&Itemid=235&albumid='.$lastid.'&app=walls&lang=en&task=app&view=photos';
					saveAlbumurlwall($newurlw,$oldurlw);
					$albumid		=	$lastid;
				}
			}
				
			$path = "../images/photos/".$userid;
		
			if(!is_dir($path))
			{
				mkdir($path, 0777);
			}
			$path = "../images/photos/".$userid.'/'.$albumid;
			if(!is_dir($path))
			{
				mkdir($path, 0777);
			}
	
			$pathorg = "../images/originalphotos/".$userid;
		
			if(!is_dir($pathorg))
			{
				mkdir($pathorg, 0777);
			}
			$org = "../images/originalphotos/".$userid.'/'.$albumid;
		
			if(!is_dir($org))
			{
				mkdir($org, 0777);
			}
		
			$dir	=	"../images/photos/".$userid.'/'.$albumid.'/';
			$dir2	=	"images/photos/".$userid.'/'.$albumid.'/';
			
			/****************orignal image upload path *************************/
			$dir3	=	"../images/originalphotos/".$userid.'/'.$albumid.'/';
			$dir4	=	"images/originalphotos/".$userid.'/'.$albumid.'/';
		//print_r($rs);
			$captionget	=	explode(",",$rs['imagecaption']);

				//echo "<pre>";
				$totlaimg	=	count($imgName);
				//print_r($captionget);
				//print_r($imgName);//die;
				$count	=	0;
				$photo = '';
				$i = 1;
				{
					//echo '<pre>';print_r($imgName['image'.$i]['tmp_name']);die;
					$rand			=	rand(0000,9999);
					$img_name 		= 	$rand.$imgName['image'.$i]['name'];
					
					$img 			=	$imgName['image'.$i]['tmp_name'];

					
					$upload_ori		=	"{$dir3}{$img_name}";
					$photo_ori		=	"{$dir4}{$img_name}";		
					
					$upload_small	=	"{$dir}{$img_name}";
					$photo_small	=	"{$dir2}{$img_name}";
					
					$upload_thumb	=	"{$dir}thumb_{$img_name}";
					$photo_thumb	=	"{$dir2}thumb_{$img_name}";

					
					if($i == 1)
						$photo	=	$photo_ori;
					$image = new UploadImage();
					$image->load($img);
					$image->save($upload_ori,100);
					
					$image->resize(300,300);
					$image->save($upload_small,95);
					
					$image->resize(150,150);
					$image->save($upload_thumb,95);

										
					//$count
					// $caption		=	stripslashes(escapeChars(tag(trim($captionget[$count]))));
					 $caption		=	specialCharRemove(tag(trim($captionget[$count])));
					 //$caption		=	escapeChars(tag($captionget[$count]));
					$img_query	=	"INSERT INTO jos_community_photos SET ".
									" albumid	 	= '".$albumid."', ".
									" caption	 	= '".$caption."', ".
									" published	 	= '1', ".
									" creator	 	= '".$userid."', ".
									" original		= '".$photo_ori."' , ".
									" image			= '".$photo_small."' , ".
									" thumbnail		= '".$photo_thumb."' , ".
									" cropped		= '".$rs['cropPoints']."' , ".
									" inverted		= '".$rs['cropInverted']."' , ".
									" permissions	=	'".$permission."', ".
									" filesize		= '".$filesize."' , ".
									" storage		= 'file' , ".
									" ordering		= '".$i."', ".
									" created		= '".nowDateTime()."'";
			//echo "<br>";
					mysql_query($img_query);
					$count++;
					$lastid = mysql_insert_id();
					$photoId[] = $lastid;
					
					$tagArray		=	json_decode(stripcslashes($_POST['hashtag']));
					
					saveHashTagOnPhoto($lastid,$albumid,$userid,$tagArray[$i-1],$permission);
					
					$getphoptoid	=	"SELECT * FROM jos_community_photos_albums WHERE id='".$albumid."' and photoid = '0'";
					
					$result 	= 	mysql_query($getphoptoid);
					
					if(mysql_num_rows($result) > 0)
					{
						$ph_query	=	" UPDATE jos_community_photos_albums SET ".
										" photoid	= '".$lastid."' ".
										" WHERE id='".$albumid."' and photoid = '0' ";
						
						mysql_query($ph_query);
					}
				}
			$msg	=	"You have successfully added photos.";
			$status="true";
		}
		//die;
	//$arr=array("message"=>$msg,"status"=>$status,"photoid"=>$photoId);
	$arr=array("message"=>$msg,"status"=>$status,"albumimageurl"=>$mainlink.$photo,"albumid"=>$lastid, "albumurl"=>$mainlink.$userName.'/photos/'.$albumid);
	return $arr;
}

function getSinglePost($userid,$albumid){
	global $mainlink;
	$FOLLOW	= new FOLLOW_CLASS;
	$status ="false";
	$msg	=	'';
	$pageSize	=	15;
	$total_rows	=	0;
	$comment	=	array();	
	if ($userid<=0 || $userid=="")
		$msg="Invalid user or post";
	else{
		
		$sql	=	"SELECT A.id AS albumsid, A.photoid AS coverid,A.permissions, A.name AS albumsname, A.description AS aldescription, A.created AS dtdate, A.creator AS userid, A.path AS albumpath, B.thumbnail,allow_follower, A.merge_private, B.cropped FROM jos_community_photos_albums A inner JOIN jos_community_photos B ON A.photoid = B.id  WHERE A.id = '".$albumid."' AND A.creator not in(select blocked_userid from jos_community_blocklist where userid=".$userid.") AND A.creator not in(select userid from jos_community_blocklist where blocked_userid=".$userid.") AND A.creator in(select id from jos_users where block=0) GROUP BY A.id ORDER BY A.created DESC";
		/*$sql	=	"select * from view_photos  WHERE (userid = '".$userid."' or userid in (select friend_id from view_friends where user_id=$friend_id) AND A.creator not in(select blocked_userid from jos_community_blocklist where userid=".$userid.") AND A.creator not in(select userid from jos_community_blocklist where blocked_userid=".$userid.") AND A.creator in(select id from jos_users where block=0) GROUP BY A.id ORDER BY A.created DESC";
	
		echo $sql;
		exit;*/
		$result1 = mysql_query($sql);
		$total_rows	= mysql_num_rows($result1);

		$lastPage = ceil($total_rows / $pageSize);
		if ($pageNo <= 1)
				$limit = " limit 0, ".$pageSize;
			else
				$limit = " limit ".($pageNo-1) * $pageSize.", ".$pageSize;
		$selectsql = $sql.$limit;
		$result = mysql_query($selectsql);

		if (mysql_num_rows($result) > 0)
		{
			while ($rs=mysql_fetch_assoc($result))
			{
				$rs['allow_follower']	=	$rs['allow_follower'];
				$rs['allow_merge']		=	'1';
				$show =	"YES";

				if($rs['userid'] != $userid && $rs['permissions'] == 40) 
				{
					checkAlbumDetailTable();
					$selesql	=	mysql_query("select * from albumdetail where userid = '".$userid."' AND albumid = '".$rs['albumsid']."'");
					if(mysql_num_rows($selesql) <= 0)
					{
						$rs['allow_follower']	=	0;
						$show	=	"NO";
					}
				}

				if ($rs['userid'] == $userid) {
					$rs['allow_merge'] = '0';
				}else{
					if ($rs['merge_private'] == 1) {
						$selesql = mysql_query("select * from notification_post where type = 'MERGE' AND posttype = 'ALBUM' AND ref_id = '".$userid."' AND userid = '".$rs['userid']."'AND mainid = '".$rs['albumsid']."' LIMIT 1");
						if(mysql_num_rows($selesql) <= 0)
							$rs['allow_merge']	= '0';
					}else{
						if ($FOLLOW->getUserStatus($rs['userid'],$userid, "FOLLOWING") == 0 || $FOLLOW->getUserStatus($userid, $rs['userid'], "FOLLOWING") == 0)
							$rs['allow_merge']	= '0';	
					}
				}

				if ($rs['allow_merge'] == 1) {
					if ($rs['cropped'] == null || $rs['cropped'] == "")
						$rs['allow_merge'] = '0';
				}

				if($show ==	"YES")
				{
					$getphoto	=	"SELECT ph.id AS photoid, ph.caption AS photocaption, ph.cropped as cropped, ph.inverted as cropInverted, ph.creator AS userid, ph.image AS smalimage, ph.thumbnail, ph.original, ph.hits AS photohits, ph.parentid, jcu.avatar AS userimagebig, jcu.thumb as userimagethumb, ju.username FROM jos_community_photos ph LEFT JOIN jos_community_users jcu ON (ph.creator = jcu.userid) LEFT JOIN jos_users ju ON (ph.creator = ju.id) WHERE ph.albumid = '".$rs['albumsid']."' AND ph.creator not in(select blocked_userid from jos_community_blocklist where userid=".$userid.")  AND ph.creator not in(select userid from jos_community_blocklist where blocked_userid=".$userid.") ORDER BY ph.id DESC";
					$result2 = mysql_query($getphoto);

					$rs['mainimg'] = "";
					$rs['postid'] = 0;
					$rs['phptodetail'] = array();

					while ($rs2=mysql_fetch_assoc($result2)){
						$photocaption	=	removeSlesh(trim(stripslashes($rs2['photocaption'])));
						//echo $rs2['photoid']." ".$photocaption."<br>";
						if($photocaption == "\\")
							$photocaption='';
						$rs2['photocaption']	=	$photocaption;
						$rs2['totalcomment']	=	gettotcomment($rs2['photoid']);
						$rs2['totallike']		=	getTotLike($rs2['photoid'],'photo');
						$rs2['likestatus']		=	getLikeStatus($userid,$rs2['photoid'],'photo');
						$rs2['comment']			=	getPhotoComment($rs2['photoid']);
						$rs2['parentid']		=	$rs2['parentid'];
						
						if ($rs2['parentid'] == '0'){
							$rs['postimg'] = $rs2['original'];
							$rs['postid'] = $rs2['photoid'];
						}
						$rs['phptodetail'][] =  $rs2;
					}

					if (count($rs['phptodetail']) == 0)
						continue;

					$userinfo		=	userInfo($rs['userid']);
					$userName		=	$userinfo['username'];
					if($userid == $rs['userid'])
						$rs['is_follow']		=	"1";
					else
						$rs['is_follow']		=	"".$FOLLOW->getUserStatus($rs['userid'],$userid,"FOLLOWING")."";
					
					$rs['totalcomment']	=	totAlbumcomment($rs['albumsid']);
					$rs['comment']		=	getAlbumComment($rs['albumsid']);
					$rs['totallike']	=	getTotAlPhotoLike($rs['albumsid'],$rs['userid']);
					$rs['likestatus']	=	getLikeStatus($userid,$rs['albumsid'],'album');
					$rs['userid']		=	$userinfo['userid'];
					$rs['name']			=	$userinfo['name'];
					$rs['username']		=	$userinfo['username'];
					$rs['email']		=	$userinfo['email'];
					$rs['userimagebig']		=	$userinfo['userimagebig'];
					$rs['userimagethumb']	=	$userinfo['userimagethumb'];
					$rs['time_text']	=	getTimeInfo($rs['dtdate'], date("Y-m-d H:i:s"), "x");
					$rs['link']			=	$mainlink.trim($userName).'/photos/'.adddash($rs['albumsid']);
					$comment[] =  $rs;
				}
			}
			$msg	=	'Successfully';
			$status="true";
		}
		else
			$msg="No record found on this post";
	}
	$arr=array("message"=>$msg,"data"=>$comment,"totalpost" => $total_rows,"totalpage" => $lastPage,"pagesize" => $pageSize,"status"=>$status);
	return $arr;
}

function getSinglePost1($albumid,$userid,$loginid){
	$FOLLOW	= new FOLLOW_CLASS;
	$status ="false";
	
	$msg	=	'';
	$comment	=	array();
	$albumarr	=	array();

	
	if ($albumid<=0 || $albumid=="" || $userid<=0 || $userid=="")
		$msg="Invalid user or post";
	else{
		/*
		if (getAlbumPermission($albumid)==40){
			if (!checkUserPermission($albumid,$userid)){
				$msg="Record not found";
				$arr=array("message"=>$msg,"status"=>$status);
				return $arr;
			}
		}
		*/
			$marmition	=	" ";
			if($userid == $loginid)
				$marmition	=	"";
			
			$sql="SELECT id AS photoid, caption AS photocaption, creator AS userid, image AS smalimage, thumbnail, original, hits AS photohits, albumid as albumsid FROM jos_community_photos WHERE albumid='".$albumid."' ".$marmition." order by id asc ";
		
			$result = mysql_query($sql);
		
			if (mysql_num_rows($result)>0){
				while ($rs=mysql_fetch_assoc($result)){
					
					$Photocomment		=	getPhotoComment($rs['photoid']);
					$rs['totcomment']	=	gettotcomment($rs['photoid']);
					$rs['totallike']	=	getTotLike($rs['photoid'],'photo');
					$rs['likestatus']	=	getLikeStatus($userid,$rs['photoid'],'photo');
					
					$rs['comment']		=	$Photocomment;
					$rs['caption']		=	removeSlesh(stripslashes($rs['photocaption']));
					
					$comment[] =  $rs;
				}
				
				$msg	=	'Successfully';
				$status="true";
			}
			else
				$msg="No found";		
			
			$albumarr	=	getAlbumdetial($albumid,$userid,$loginid);
			
			if(!empty($albumarr))
			{
				$msg	=	'Successfully';
				$status	=	"true";
				//$albumarr	=	getAlbumdetial($albumid,$userid,$loginid);
				if($userid == $loginid)
					$is_follow		=	1;
				else
					$is_follow		=	$FOLLOW->getUserStatus($userid,$loginid,"FOLLOWING");	

				$albumarr[0]['allow_merge'] = '1';

				if ($albumarr[0]['creator'] == $loginid)
					$albumarr[0]['allow_merge'] = '0';
				else if (intval($albumarr[0]['merge_private']) == 1){
					$selesql = mysql_query("select * from notification_post where type = 'MERGE' AND posttype = 'ALBUM' AND ref_id = '".$loginid."' AND userid = '".$albumarr[0]['creator']."'AND mainid = '".$albumid."' LIMIT 1");
				 	if(mysql_num_rows($selesql) <= 0)
				 		$albumarr[0]['allow_merge']	= '0';
				}

				// if ($rs['allow_merge'] == 1) {
				// 	if ($rs['cropped'] == null || $rs['cropped'] == "")
				// 		$rs['allow_merge'] = "";
				// }
			}
			else
				$status="false";
	}
	$arr=array("message"=>$msg,"albumdetail" =>$albumarr,"data"=>$comment,"is_follow"=>"".$is_follow."","status"=>$status);
	return $arr;
}
		
//function getSinglePhoto($albumid,$userid){//
//
//	$status ="false";
//	$msg	=	'';
//	if ($albumid<=0 || $albumid=="" || $userid<=0 || $userid=="")
//
//		$msg="Invlid user or post";
//
//	else{
//
//		$sql="SELECT * FROM jos_community_photos WHERE albumid='".$albumid."' AND creator = '".$userid."'";
//		
//		$result = mysql_query($sql);
//		
//		if (mysql_num_rows($result)>0){
//			while ($rs=mysql_fetch_assoc($result)){
//				
//				$comment[] =  $rs;
//			}
//			$msg	=	'Successfully';
//			$status="true";
//		}
//		else
//			$msg="No found";
//	}
//		$arr=array("message"=>$msg,"data"=>$comment,"status"=>$status);
//		return $arr;}
	
function getAlbumPhoto($albumid,$userid,$loginid){
	global $mainlink;
	$FOLLOW	= new FOLLOW_CLASS;
	$status ="false";
	$msg	=	'';

	if ($userid<=0 || $userid=="")
		$msg="Invalid user or post";
	else{
		$marmition	=	" AND a.permissions = '0' ";
		if($userid == $loginid)
			$marmition	=	"";
		
		$sql	=	"SELECT a.id AS albumsid, a.photoid AS coverid, a.creator AS userid, a.name AS albumsname, a.description AS aldescription, a.created AS dtdate, a.path AS albumpath, b.* FROM jos_community_photos_albums a LEFT JOIN jos_community_photos b ON a.photoid = b.id  WHERE a.creator = '".$userid."' ".$marmition." AND a.creator not in(select userid from jos_community_blocklist where blocked_userid=".$loginid.") ORDER BY id ASC";

		$result = mysql_query($sql);
		
		if (mysql_num_rows($result)>0){
			while ($rs=mysql_fetch_assoc($result)){

				$userinfo	=	userInfo($rs['userid']);	
				if($userid == $loginid)
					$rs['is_follow']		=	"1";
				else
				$rs['is_follow']		=	"".$FOLLOW->getUserStatus($loginid,$userid,"FOLLOWER")."";	
				$rs['username']			=	$userinfo['username'];	
				$rs['totalcomment']	=	totAlbumcomment($rs['albumsid']);
				$rs['comment']		=	getAlbumComment($rs['albumsid']);
				$rs['totallike']	=	getTotAlPhotoLike($rs['albumsid'],$rs['userid']);
				$rs['likestatus']	=	getLikeStatus($userid,$rs['albumsid'],'album');
				
				$rs['albumurl']		=$mainlink.$rs['username'].'/photos/'.adddash($rs['albumsname']);
				$comment[] =  $rs;
				
				}
			$msg	=	'Successfully';
			$status="true";
		}
		else
			$msg="No record found on this post";
	}
		$arr=array("message"=>$msg,"data"=>$comment,"status"=>$status);
		return $arr;
	
	
	}
		
function getAllPost($pageNo=1,$userid){
	global $mainlink;
	$FOLLOW	= new FOLLOW_CLASS;
	$status ="false";
	$msg	=	'';
	$pageSize	=	15;
	$total_rows	=	0;
	$comment	=	array();	
	if ($userid<=0 || $userid=="")
		$msg="Invalid user or post";
	else{
		$sql	=	"SELECT A.id AS albumsid, A.photoid AS coverid,A.permissions, A.name AS albumsname, A.description AS aldescription, A.created AS dtdate, A.creator AS userid, A.path AS albumpath, B.thumbnail,allow_follower, A.merge_private, B.cropped 
		             FROM jos_community_photos_albums A 
					 inner JOIN jos_community_photos B ON A.photoid = B.id  
					 WHERE (A.creator = '".$userid."' or A.creator in (select connect_to from jos_community_connection WHERE connect_from = '".$userid."' and status=1) ) 
					 AND A.creator not in(select blocked_userid from jos_community_blocklist where userid=".$userid.") 
					 AND A.creator not in(select userid from jos_community_blocklist where blocked_userid=".$userid.") 
					 AND A.creator in(select id from jos_users where block=0) 
					 GROUP BY A.id 
					 ORDER BY A.updated DESC";
		/*$sql	=	"select * from view_photos  WHERE (userid = '".$userid."' or userid in (select friend_id from view_friends where user_id=$friend_id) AND A.creator not in(select blocked_userid from jos_community_blocklist where userid=".$userid.") AND A.creator not in(select userid from jos_community_blocklist where blocked_userid=".$userid.") AND A.creator in(select id from jos_users where block=0) GROUP BY A.id ORDER BY A.created DESC";
	
		echo $sql;
		exit;*/
		$result1 = mysql_query($sql);
		$total_rows	= mysql_num_rows($result1);

		$lastPage = ceil($total_rows / $pageSize);
		if ($pageNo <= 1)
				$limit = " limit 0, ".$pageSize;
			else
				$limit = " limit ".($pageNo-1) * $pageSize.", ".$pageSize;
		$selectsql = $sql.$limit;
		$result = mysql_query($selectsql);

		if (mysql_num_rows($result) > 0)
		{
			while ($rs=mysql_fetch_assoc($result))
			{
				
				$rs['allow_follower']	=	$rs['allow_follower'];
				$rs['allow_merge']		=	'1';
				$show =	"YES";
                //call a function to get totalmerger of post
                            $total_merge = totmerge($rs['albumsid']);	
                            $rs['totalmerge'] = $total_merge;	

               			
				if($rs['userid'] != $userid && $rs['permissions'] == 40) 
				{
					checkAlbumDetailTable();
					$selesql	=	mysql_query("select * from albumdetail where userid = '".$userid."' AND albumid = '".$rs['albumsid']."'");
					if(mysql_num_rows($selesql) <= 0)
					{
						$rs['allow_follower']	=	0;
						$show	=	"NO";
					}
				}

				if ($rs['userid'] == $userid) {
					$rs['allow_merge'] = '0';
				}else{
					if ($rs['merge_private'] == 1) {
						$selesql = mysql_query("select * from notification_post where type = 'MERGE' AND posttype = 'ALBUM' AND ref_id = '".$userid."' AND userid = '".$rs['userid']."'AND mainid = '".$rs['albumsid']."' LIMIT 1");
						if(mysql_num_rows($selesql) <= 0)
							$rs['allow_merge']	= '0';
					}else{
						if ($FOLLOW->getUserStatus($rs['userid'],$userid, "FOLLOWING") == 0 || $FOLLOW->getUserStatus($userid, $rs['userid'], "FOLLOWING") == 0)
							$rs['allow_merge']	= '0';	
					}
				}

				if ($rs['allow_merge'] == 1) {
					if ($rs['cropped'] == null || $rs['cropped'] == "")
						$rs['allow_merge'] = '0';
				}

				if($show ==	"YES")
				{
					//$getphoto	=	"SELECT id AS photoid, caption AS photocaption, creator AS userid, image AS smalimage, thumbnail, original, hits AS photohits, parentid, jcu.avatar AS userimagebig, jcu.thumb as userimagethumb, ju.username FROM jos_community_photos LEFT JOIN jos_community_users jcu ON (creator = jcu.userid) LEFT JOIN jos_users ju ON(ju.id = creator) WHERE albumid = '".$rs['albumsid']."' AND creator not in(select blocked_userid from jos_community_blocklist where userid=".$userid.")  AND creator not in(select userid from jos_community_blocklist where blocked_userid=".$userid.") ORDER BY ordering DESC, id ASC";

					$getphoto	=	"SELECT ph.id AS photoid, ph.caption AS photocaption, ph.cropped as cropped, ph.inverted as cropInverted, ph.creator AS userid, ph.image AS smalimage, ph.thumbnail, ph.original, ph.hits AS photohits, ph.parentid, jcu.avatar AS userimagebig, jcu.thumb as userimagethumb, ju.username FROM jos_community_photos ph LEFT JOIN jos_community_users jcu ON (ph.creator = jcu.userid) LEFT JOIN jos_users ju ON (ph.creator = ju.id) WHERE ph.albumid = '".$rs['albumsid']."' AND ph.creator not in(select blocked_userid from jos_community_blocklist where userid=".$userid.")  AND ph.creator not in(select userid from jos_community_blocklist where blocked_userid=".$userid.") ORDER BY ph.created DESC";

					$result2 = mysql_query($getphoto);

					$rs['mainimg'] = "";
					$rs['postid'] = 0;
					$rs['phptodetail'] = array();
                    
					while ($rs2=mysql_fetch_assoc($result2)){
						
						//don't show like or dislike post
						
						$like_dislike_post = likeDislikePost($rs2['photoid'],$userid);
						//echo $like_dislike_post;
						if($rs2['parentid'] == '0')
						{
						
						$photocaption	=	removeSlesh(trim(stripslashes($rs2['photocaption'])));
						//echo $rs2['photoid']." ".$photocaption."<br>";
						if($photocaption == "\\")
							$photocaption='';
						$rs2['photocaption']	=	$photocaption;
						$rs2['totalcomment']	=	gettotcomment($rs2['photoid']);
						$rs2['totallike']		=	getTotLike($rs2['photoid'],'photo');
						$rs2['likestatus']		=	getLikeStatus($userid,$rs2['photoid'],'photo');
						$rs2['comment']			=	getPhotoComment($rs2['photoid']);
						$rs2['parentid']		=	$rs2['parentid'];
						
						if($rs2['parentid']==0){
							$rs2['mainimage'] = 1;
							
						}else{
							$rs2['mainimage'] = 0;
							
						}
						
						if ($rs2['parentid'] == '0'){
							$rs['postimg'] = $rs2['original'];
							$rs['postid'] = $rs2['photoid'];
							
							 
						}
						$rs['phptodetail'][] =  $rs2;
}else{
	
	if($like_dislike_post==0){
		$photocaption	=	removeSlesh(trim(stripslashes($rs2['photocaption'])));
						//echo $rs2['photoid']." ".$photocaption."<br>";
						if($photocaption == "\\")
							$photocaption='';
						$rs2['photocaption']	=	$photocaption;
						$rs2['totalcomment']	=	gettotcomment($rs2['photoid']);
						$rs2['totallike']		=	getTotLike($rs2['photoid'],'photo');
						$rs2['likestatus']		=	getLikeStatus($userid,$rs2['photoid'],'photo');
						$rs2['comment']			=	getPhotoComment($rs2['photoid']);
						$rs2['parentid']		=	$rs2['parentid'];
						
						if($rs2['parentid']==0){
							$rs2['mainimage'] = 1;
							
						}else{
							$rs2['mainimage'] = 0;
							
						}
						
						if ($rs2['parentid'] == '0'){
							$rs['postimg'] = $rs2['original'];
							$rs['postid'] = $rs2['photoid'];
							
							 
						}
						$rs['phptodetail'][] =  $rs2;
		
		
		
	}
	
	
}
						
					}

					if (count($rs['phptodetail']) == 0)
						continue;

					$userinfo		=	userInfo($rs['userid']);
					$userName		=	$userinfo['username'];
					if($userid == $rs['userid'])
						$rs['is_follow']		=	"1";
					else
						$rs['is_follow']		=	"".$FOLLOW->getUserStatus($rs['userid'],$userid,"FOLLOWING")."";
					
					$rs['totalcomment']	=	totAlbumcomment($rs['albumsid']);
					$rs['comment']		=	getAlbumComment($rs['albumsid']);
					$rs['totallike']	=	getTotAlPhotoLike($rs['albumsid'],$rs['userid']);
					$rs['likestatus']	=	getLikeStatus($userid,$rs['albumsid'],'album');
					$rs['userid']		=	$userinfo['userid'];
					$rs['name']			=	$userinfo['name'];
					$rs['username']		=	$userinfo['username'];
					$rs['email']		=	$userinfo['email'];
					$rs['userimagebig']		=	$userinfo['userimagebig'];
					$rs['userimagethumb']	=	$userinfo['userimagethumb'];
					$rs['time_text']	=	getTimeInfo($rs['dtdate'], date("Y-m-d H:i:s"), "x");
					$rs['link']			=	$mainlink.trim($userName).'/photos/'.adddash($rs['albumsid']);
					$comment[] =  $rs;
				}
			}
			$msg	=	'Successfully';
			$status="true";
		}
		else
			$msg="No record found on this post";
	}
		$arr=array("message"=>$msg,"data"=>$comment,"totalpost" => $total_rows,"totalpage" => $lastPage,"pagesize" => $pageSize,"status"=>$status);
		return $arr;
}

function getAllTopPhoto($userid){
	$status = "false";
	$msg	= '';
	$i		= 0;

	if ($userid<=0 || $userid=="")
		$msg="Invalid user";
	else{
//		$sql	=	"SELECT uid, SUM( CHAR_LENGTH( `like` ) - CHAR_LENGTH( REPLACE( `like` , ',', '' ) ) +1 ) cnt FROM `jos_community_likes` WHERE UPPER( element ) = 'photo' AND `like` != '' GROUP BY uid ORDER BY RAND() LIMIT 100";
		$sql	=	"SELECT * from view_totallikes where element ='photo' ORDER BY RAND() LIMIT 100";
		
		$result = mysql_query($sql);

		if (mysql_num_rows($result)>0){

			while ($rs=mysql_fetch_assoc($result)){
				//$getphoto	=	"SELECT id AS photoid, albumid,permissions, caption AS photocaption, creator AS userid, image AS smalimage, thumbnail, original, hits AS photohits FROM jos_community_photos  WHERE id = '".$rs['uid']."' AND creator not in(select blocked_userid from jos_community_blocklist where userid=".$userid.") AND creator in(select id from jos_users where block=0) ORDER BY created ASC";
				$getphoto	=	"SELECT id AS photoid, albumid,permissions, caption AS photocaption, creator AS userid, image AS smalimage, thumbnail, original, hits AS photohits FROM jos_community_photos  WHERE id = '".$rs['uid']."' AND creator not in(select userid from jos_community_blocklist where blocked_userid=".$userid.") AND creator in(select id from jos_users where block=0) ORDER BY created ASC";
				$result2 = mysql_query($getphoto);

				while ($rs2=mysql_fetch_assoc($result2)){
					if($rs2['albumid'] > 0)
					{
						$show	=	"YES";
						if ($rs2['userid']!=$userid){
							if (getAlbumPermission($rs2['albumid'])==40 ){
								if (!checkUserPermission($rs2['albumid'],$userid))
									$show="NO";
							}else {
								$userinfo	=	userInfo($rs2['userid']);
								//var_dump(getUserFollowPermission($userid,$rs2['userid'],$rs2['albumid']));
								if (!getUserFollowPermission($userid,$rs2['userid'],$rs2['albumid']))
								{
									//echo $userinfo['followpermission'];
									//if($userinfo['followpermission'] == 1)
									$show="NO";
								}
							}
						}
						
						if($rs2['userid'] != $userid && $rs2['permissions'] == 40) {
							$selesql	=	mysql_query("select * from albumdetail where userid = '".$userid."' AND albumid = '".$rs2['albumid']."'");
							if(mysql_num_rows($selesql) <= 0)
							{
								$show	=	"NO";
							}
						}


						if($show ==	"YES")
						{
							$rs2['totalcomment']	=	gettotcomment($rs2['photoid']);
							$rs2['totallike']		=	getTotLike($rs2['photoid'],'photo');
							$rs2['likestatus']		=	getLikeStatus($userid,$rs2['photoid'],'photo');

							$comment[] =  $rs2;
							$msg	=	'Successfully';
							$status="true";
							$i++;
						}
					}
				}
				if ($i==15)
					break 1;
			}
		}
		else
			$msg="No record found on this post";
	}
	$arr=array("message"=>$msg,"data"=>$comment,"status"=>$status,"count"=>$i);
	return $arr;
}

function getSinglePhoto($userid,$albumid,$photoid,$loginid){
	global $mainlink;
	$status ="false";
	$msg	=	'';
	$comment	=	'';
	if ($userid<=0 || $userid=="")
		$msg="Invalid user or post";
	else{
		$sql = "SELECT id AS albumsid, creator AS userid, name AS albumsname, description AS aldescription, created AS dtdate,	allow_follower as allowfollower, merge_private FROM jos_community_photos_albums WHERE id = '".$albumid."'";
		$result = mysql_query($sql);
		$timetext	=	'';
		if (mysql_num_rows($result)>0){

			$allow_merge = 1;
			while ($rs=mysql_fetch_assoc($result)){
				$getphoto	=	"SELECT id AS photoid, caption AS photocaption, creator AS userid, image AS smalimage, thumbnail, original, hits AS photohits,created, cropped, parentid FROM jos_community_photos WHERE albumid = '".$rs['albumsid']."' AND id ='".$photoid."' ORDER BY id ASC ";
				$result2 = mysql_query($getphoto);
				
				while ($rs2=mysql_fetch_assoc($result2)){
					$userinfo2		=	userInfo($rs2['userid']);
					$rs2['name']			=	$userinfo2['name'];
					$rs2['username']		=	$userinfo2['username'];
					$rs2['userimagebig']	=	$userinfo2['userimagebig'];
					$rs2['userimagethumb']	=	$userinfo2['userimagethumb'];
					$rs2['totalcomment']	=	gettotcomment($rs2['photoid']);
					$rs2['latestcomment']	=	getPhotoComment($rs2['photoid']);
					$rs2['totallike']		=	getTotLike($rs2['photoid'],'photo');
					$rs2['likestatus']		=	getLikeStatus($userid,$rs2['photoid'],'photo');
					$rs2['photocaption']	=	removeSlesh(stripslashes(tag($rs2['photocaption'])));
						
					$timetext		=	getTimeInfo($rs2['created'], date("Y-m-d H:i:s"), "x");
					$rs['phptodetail'][] =  $rs2;
					
					if ($parentid == 0 && ($cropped == null || $cropped == "")) {
						$allow_merge = 0;
					}
				}
				
				$userinfo		=	userInfo($rs['userid']);
				$rs['totalcomment']	=	totAlbumcomment($rs['albumsid']);
				$rs['totalphoto']	=	getTotAlbumPhoto($albumid);
				$rs['currentphoto']	=	getPhotoPosition($albumid,$photoid);
				$rs['totallike']	=	getTotAlPhotoLike($rs['albumsid'],$rs['userid']);
				$rs['likestatus']	=	getLikeStatus($userid,$rs['albumsid'],'album');
				$rs['userid']	=	$userinfo['userid'];
				$rs['name']		=	$userinfo['name'];
				$rs['username']	=	$userinfo['username'];
				$rs['email']	=	$userinfo['email'];
				$rs['userimagebig']		=	$userinfo['userimagebig'];
				$rs['userimagethumb']	=	$userinfo['userimagethumb'];
				$rs['photolink']		=	$mainlink.$userinfo['username'].'/photos/'.$albumid.'#photoid='.$photoid;
				$rs['time_text']	=	$timetext;//getTimeInfo($rs['dtdate'], date("Y-m-d H:i:s"), "x");
				
				$rs['allow_merge'] = '1';
				if ($rs['userid'] == $userid) {
					$rs['allow_merge'] = '0';
				}else if (intval($merge_private) == 1){
					$selesql = mysql_query("select * from notification_post where type = 'MERGE' AND posttype = 'ALBUM' AND ref_id = '".$userid."' AND userid = '".$rs['userid']."'AND mainid = '".$albumid."' LIMIT 1");
					if(mysql_num_rows($selesql) <= 0)
						$rs['allow_merge']	= '0';
				}

				if ($rs['allow_merge'] == 1) {
					if ($rs['cropped'] == null || $rs['cropped'] == "")
						$rs['allow_merge'] = '0';
				}

				if ($rs['allow_merge'] == 1 && $allow_merge == 0)
					$rs['allow_merge'] = 0;
				$comment[] =  $rs;
			}
			$msg	=	'Successfully';
			$status="true";
		}
		else
			$msg="No record found on this post";
	}
		$arr=array("message"=>$msg,"data"=>$comment,"status"=>$status);
		return $arr;
}

function likeUserList($userid,$photoid,$posttype)
{
	$status ="false";
	$msg	=	'';
	$comment	=	array();
	if ($userid<=0 || $userid=="")
		$msg="Invalid user";
	else{
		$posttype	=	strtoupper($posttype);
		if($posttype == "PHOTO")
			$sql	=	"SELECT `like`,uid FROM `jos_community_likes` WHERE UPPER( element ) = 'PHOTO' AND uid = '".$photoid."' ";
		else
			$sql	=	"SELECT `like`,uid FROM `jos_community_likes` WHERE UPPER( element ) = 'PHOTO' AND uid in (select id from jos_community_photos where albumid = '".$photoid."') ";

		$result = mysql_query($sql);
		$cou	=	0;
		if (mysql_num_rows($result)>0){
			while ($rs=mysql_fetch_assoc($result)){
				$info	=	explode(",",$rs['like']);
			
				if(count($info) > 0)
				{
					foreach($info as $val)
					{
						if(intval($val) > 0)
						{
							$cou++;
							$userinfo	=	userInfo($val);
							$photoinfo	=	getAlbumId($rs['uid']);
							$com['albumid']	=	$photoinfo['albumid'];
							$com['photouserid']	=	$photoinfo['adduserid'];
							$com['photoid']	=	$rs['uid'];
							$com['userimagebig'] =  $userinfo['userimagebig'];
							$com['userimagethumb'] =  $userinfo['userimagethumb'];
							$com['userid'] =  $userinfo['userid'];
							$com['name'] =  $userinfo['name'];
							$com['username'] =  $userinfo['username'];
							$com['email'] =  $userinfo['email'];
							$comment[]=$com;
							
							
						}
				
					}
				}
			
			}
			
		}
		else
			$msg="No record found on this";
		if($cou > 0)
		{
			$msg	=	'Successfully';
			$status="true";
		}else
			$msg="No record found on this";
	}
		$arr=array("message"=>$msg,"data"=>$comment,"status"=>$status);
		return $arr;
}
function deletePost($userid,$postid,$posttype){
		global $db;
		$status="false";
		$msg="";

		
		if ($userid<=0 && $postid<=0)
			$msg="Invalid selected post";
		else{
			if($posttype == "PHOTO")
			{
				$photoInfo	=	getAlbumId($postid);
				
				$sqlselect	=	"SELECT id FROM jos_community_photos WHERE creator  = '".$userid."' and id='".$postid."'";
				$queryse	=	mysql_query($sqlselect);
			
				if (mysql_num_rows($queryse)>0)
				{
					$sqllike="delete from jos_community_likes where element = 'photo' AND uid =".$postid;
					mysql_query($sqllike);
					$sqllike="delete from photohashtag where type = 'PHOTOS' AND photoid =".$postid;
					mysql_query($sqllike);
					$sqlComment="delete from jos_community_wall where type = 'photo' AND contentid =".$postid;
					mysql_query($sqlComment);
					$sqlComment="delete from notification where posttype = 'PHOTO' AND mainid =".$postid;
					mysql_query($sqlComment);
					
					$sqlComment2="delete from notification_post where posttype in('PHOTO') AND mainid =".$postid;
					mysql_query($sqlComment2);
					
					$sqlComment2="delete from notification_comment where posttype in('PHOTO') AND mainid =".$postid;
					mysql_query($sqlComment2);

					$sqlComment2="delete from notification_merge where postid =".$postid;
					mysql_query($sqlComment2);
						
					$sqlni="delete from jos_community_photos WHERE creator  = '".$userid."' and id='".$postid."'";
					mysql_query($sqlni);
					
					$position	=	getPhotoPosition($photoInfo['albumid'],$postid);
					if($position == 1)
					{
						$sql	=	"SELECT id FROM jos_community_photos WHERE albumid = '".$photoInfo['albumid']."' order by id asc";

						$result = mysql_query($sql);
						$photoposi	=	0;
						while($rs	=	mysql_fetch_assoc($result))
						{
							mysql_query("UPDATE jos_community_photos_albums SET `photoid` = '".$rs['id']."' WHERE id ='".$photoInfo['albumid']."' ");
							break;
						}
					}
					
					$msg="Successfully";
					$status="true";
				}else
					$msg="You are not delete this photo.";
			}else
			{
				$sqlselect1	=	"SELECT id FROM jos_community_photos_albums WHERE id='".$postid."'";
				$queryse1	=	mysql_query($sqlselect1);
			
				if (mysql_num_rows($queryse1)>0)
				{
					$sqlselect	=	"SELECT id FROM jos_community_photos WHERE creator  = '".$userid."' and albumid='".$postid."'";
				
					
					$queryse	=	mysql_query($sqlselect);
					if (mysql_num_rows($queryse)>0)
					{
						while($rows	=	mysql_fetch_array($queryse))
						{
							$sqllike="delete from jos_community_likes where element = 'photo' AND uid =".$rows['id'];
							mysql_query($sqllike);
							
							$sqlComment="delete from jos_community_wall where type = 'photo' AND contentid =".$rows['id'];
							mysql_query($sqlComment);
							
							//changes on 16 august 2015
						   $sqlgetphoto	=	"SELECT group_concat(id) as photosid FROM jos_community_photos WHERE parentid =".$rows['id'];
				           $querysephoto	=	mysql_query($sqlgetphoto);

                         if (mysql_num_rows($querysephoto)>0)
					    {
						     while($rows	=	mysql_fetch_array($querysephoto))
							 {
								
								$sqlComment2="delete from notification_post where posttype in('PHOTO') AND mainid in(".$rows['photosid'].") ";
                                 				   
							   mysql_query($sqlComment2);
								
								
							 }
					
					    }
						
						//end
							//$sqlComment2="delete from notification_post where posttype in('PHOTO') AND mainid =".$rows['id'];
							//mysql_query($sqlComment2);
					
							//$sqlComment="delete from notification where posttype = 'PHOTO' AND mainid =".$rows['id'];
							//mysql_query($sqlComment);
						
						}
						
						$sqllike="delete from photohashtag where type = 'ALBUMS' AND albumid =".$postid;
						mysql_query($sqllike);
						
						//$sqlComment2="delete from notification where type in('TAG','PRIVATE') AND mainid =".$postid;
						//mysql_query($sqlComment2);
						
						$sql_self	=	"SELECT group_concat(id) as photosid FROM jos_community_photos WHERE albumid =".$postid;
				        $queryself	=	mysql_query($sql_self);
						
						
						if (mysql_num_rows($queryself)>0)
					    {
						     while($rows	=	mysql_fetch_array($queryself))
							 {
								
								$sqlComment2="delete from notification_post where posttype in('PHOTO') AND mainid in(".$rows['photosid'].") ";
                                 				   
							   mysql_query($sqlComment2);
								
								
							 }
					
					    }
						
						
						//$sqlComment2="delete from notification_post where posttype in('PHOTO') AND mainid =".$postid;
						//mysql_query($sqlComment2);
						
						$sqlComment2="delete from notification_comment where posttype in('PHOTO') AND mainid =".$postid;
						mysql_query($sqlComment2);
						
						$sqlni="delete from jos_community_photos WHERE  albumid='".$postid."'";
						mysql_query($sqlni);
						
						
					}
					$sqllike1="delete from jos_community_likes where element = 'album' AND uid =".$postid;
					mysql_query($sqllike1);
					$sqlComment2="delete from notification_post where posttype in('ALBUM','COLLAGE') AND mainid =".$postid;
					mysql_query($sqlComment2);
					
					$sqlComment2="delete from notification_comment where posttype in('ALBUM','COLLAGE') AND mainid =".$postid;
					mysql_query($sqlComment2);
					$sqlComment1="delete from jos_community_wall where type = 'album' AND contentid =".$postid;
					mysql_query($sqlComment1);
					mysql_query("delete from photohashtag where type = 'ALBUMS' AND albumid =".$postid);
					mysql_query("delete from comment_collage where albumid =".$postid);
					mysql_query("delete from albumdetail where albumid =".$postid);
					$sqlComment11="delete from jos_community_photos_albums where id =".$postid;
					mysql_query($sqlComment11);
					
					$msg="Successfully";
					$status="true";
				}
			}
			
		}
			$arr=array("message"=>$msg,"status"=>$status);
			return $arr;
}

function getCollageComment1($albumid,$userid){
	$status ="false";
	$msg	=	'';
$comment	=	array();
checkTableInDatabase();
	if ($userid<=0 || $userid=="")
		$msg="Invalid user or album";
	else{
		$sql	=	"SELECT id AS albumsid, creator AS userid, name AS albumsname, description AS aldescription, created AS dtdate FROM jos_community_photos_albums WHERE id = '".$albumid."'";
		$result = mysql_query($sql);
		
		if (mysql_num_rows($result)>0){
			while ($rs=mysql_fetch_assoc($result)){
				$getphoto	=	"SELECT CP.id AS photoid, CP.caption AS photocaption, CP.creator AS userid, CP.image AS smalimage, CP.thumbnail, CP.original,CP.hits AS photohits FROM jos_community_photos AS CP left join jos_community_likes AS CL on CP.id=CL.uid  WHERE CP.albumid = '".$rs['albumsid']."' ORDER BY SUM( CHAR_LENGTH( `like` ) - CHAR_LENGTH( REPLACE( `like` , ',', '' ) ) +1 ) DESC  ";
				$result2 = mysql_query($getphoto);
				
			while ($rs2=mysql_fetch_assoc($result2)){
				
				$rs2['totalcomment']	=	gettotcomment($rs2['photoid']);
				$rs2['latestcomment']	=	getPhotoComment($rs2['photoid']);
				$rs2['totallike']		=	getTotLike($rs2['photoid'],'photo');
				$rs2['likestatus']		=	getLikeStatus($userid,$rs2['photoid'],'photo');
				$rs2['photocaption']	=	removeSlesh(stripslashes(tag($rs2['photocaption'])));
				$rs['phptodetail'][] =  $rs2;
		
			}
			
			$userinfo		=	userInfo($rs['userid']);
			$totcommnet		=	getCollageAlbumComment($rs['albumsid']);
			$rs['totalcomment']	=	count($totcommnet);
			$rs['collagecomment']	=	$totcommnet;
			$rs['totallike']	=	getTotAlPhotoLike($rs['albumsid'],$rs['userid']);
			$rs['likestatus']	=	getLikeStatus($userid,$rs['albumsid'],'album');
			$rs['userid']	=	$userinfo['userid'];
			$rs['name']		=	$userinfo['name'];
			$rs['username']	=	$userinfo['username'];
			$rs['email']	=	$userinfo['email'];
			$rs['userimagebig']		=	$userinfo['userimagebig'];
			$rs['userimagethumb']	=	$userinfo['userimagethumb'];
			$rs['time_text']	=	getTimeInfo($rs['dtdate'], date("Y-m-d H:i:s"), "x");
			$comment[] =  $rs;
			}
			$msg	=	'Successfully';
			$status="true";
		}
		else
			$msg="No record found on this post";
	}
		$arr=array("message"=>$msg,"data"=>$comment,"status"=>$status);
		return $arr;
}



//function to get total merge

function totmerge($albumid){
	$sql = "select count(distinct(userid)) as cnt from notification_merge where albumid = ".$albumid;
	
	$result = mysql_query($sql);
			if (mysql_num_rows($result)>0){
				while ($rs=mysql_fetch_assoc($result)){
					return $rs['cnt']; 
				}
				
			}
			return 0;
	
	
	
}
function getCollageComment($albumid,$userid){
	$status ="false";
	$msg	=	'';
$comment	=	array();
checkTableInDatabase();
	if ($userid<=0 || $userid=="")
		$msg="Invalid user or album";
	else{
		$sql	=	"SELECT id AS albumsid, creator AS userid, name AS albumsname, description AS aldescription, created AS dtdate FROM jos_community_photos_albums WHERE id = '".$albumid."'";
		$result = mysql_query($sql);
		
		if (mysql_num_rows($result)>0){
			while ($rs=mysql_fetch_assoc($result)){
				//echo $getphoto	=	"SELECT CP.id AS photoid, CP.caption AS photocaption, CP.creator AS userid, CP.image AS smalimage, CP.thumbnail, CP.original,CP.hits AS photohits FROM jos_community_photos AS CP left join jos_community_likes AS CL on CP.id=CL.uid  WHERE CP.albumid = '".$rs['albumsid']."' ORDER BY SUM( CHAR_LENGTH( `like` ) - CHAR_LENGTH( REPLACE( `like` , ',', '' ) ) +1 ) DESC  ";
				
				$getphoto	=	"SELECT albumid, CP.id AS photoid, CP.caption AS photocaption, CP.creator AS userid, CP.image AS smalimage, CP.thumbnail, CP.original, CP.hits AS photohits,total_likes FROM jos_community_photos AS CP LEFT JOIN view_totallikes AS VTL ON CP.id = VTL.uid WHERE CP.albumid ='".$rs['albumsid']."' ORDER BY total_likes DESC ";
				$result2 = mysql_query($getphoto);
				
			while ($rs2=mysql_fetch_assoc($result2)){
				
				$rs2['totalcomment']	=	gettotcomment($rs2['photoid']);
				$rs2['latestcomment']	=	getPhotoComment($rs2['photoid']);
				$rs2['totallike']		=	getTotLike($rs2['photoid'],'photo');
				$rs2['likestatus']		=	getLikeStatus($userid,$rs2['photoid'],'photo');
				$rs2['photocaption']	=	removeSlesh(stripslashes(tag($rs2['photocaption'])));
				$rs['phptodetail'][] =  $rs2;
		
			}
			
			$userinfo		=	userInfo($rs['userid']);
			$totcommnet		=	getCollageAlbumComment($rs['albumsid']);
			$rs['totalcomment']	=	count($totcommnet);
			$rs['collagecomment']	=	$totcommnet;
			$rs['totallike']	=	getTotAlPhotoLike($rs['albumsid'],$rs['userid']);
			$rs['likestatus']	=	getLikeStatus($userid,$rs['albumsid'],'album');
			$rs['userid']	=	$userinfo['userid'];
			$rs['name']		=	$userinfo['name'];
			$rs['username']	=	$userinfo['username'];
			$rs['email']	=	$userinfo['email'];
			$rs['userimagebig']		=	$userinfo['userimagebig'];
			$rs['userimagethumb']	=	$userinfo['userimagethumb'];
			$rs['time_text']	=	getTimeInfo($rs['dtdate'], date("Y-m-d H:i:s"), "x");
			$comment[] =  $rs;
			}
			$msg	=	'Successfully';
			$status="true";
		}
		else
			$msg="No record found on this post";
	}
		$arr=array("message"=>$msg,"data"=>$comment,"status"=>$status);
		return $arr;
}



$arr=array();

if (isset($_REQUEST['type']) && $_REQUEST['type']!=""){
	$data=array();
	if (strtoupper($_REQUEST['type'])=="ADD"){
		
		//$data=json_decode(str_replace("\\","",$_GET['data']));
		//echo "<pre>";
		//print_r($_POST);
		//print_r($_FILES);
		//die;
		$arr=addNewPost($_POST,$_FILES);
		
		echo json_encode($arr);
	}
	if (strtoupper($_REQUEST['type'])=="ALBUM"){
		
		//$data=json_decode(str_replace("\\","",urldecode($_GET['data'])));
		
		$userid		=	intval(trim($_REQUEST['userid']));
		$name		=	specialCharRemove(trim($_REQUEST['name']));
		$desc		=	specialCharRemove(trim($_REQUEST['desc']));
		$hits		=	(trim($_REQUEST['hits']));
		//$alowuser	=	(($_REQUEST['alowuser']));
		$alowuser	=	json_decode(stripslashes($_REQUEST['alowuser']));
		$permission		=	strtoupper(trim($_REQUEST['permission']));
		$allow_follower		=	strtoupper(trim($_REQUEST['allowfollower']));
		//$inviteuser		=	(($_REQUEST['inviteuser']));
		//$albumhashtag	=	(($_REQUEST['albumhashtag']));
		$inviteuser		=	json_decode(stripslashes($_REQUEST['inviteuser']));
		$albumhashtag	=	json_decode(stripslashes($_REQUEST['albumhashtag']));
		$arr=addNewAlbum($userid,$name,$desc,$hits,$permission,$allow_follower,$alowuser,$inviteuser,$albumhashtag);
		echo json_encode($arr);
	}
	
	if (strtoupper($_REQUEST['type'])=="GET"){
		
		$data		=	json_decode(str_replace("\\","",urldecode($_GET['data'])));
		$loginid	=	intval(trim($data[0]->loginid));
		$albumid	=	intval(trim($data[0]->albumid));
		$arr=getSinglePost($loginid, $albumid);
		
		echo json_encode($arr);
	}
	if (strtoupper($_REQUEST['type'])=="GETPHOTO"){
		
		$data		=	json_decode(str_replace("\\","",urldecode($_GET['data'])));
		$userid		=	intval(trim($data[0]->userid));
		$albumid	=	intval(trim($data[0]->albumid));
		$photoid	=	intval(trim($data[0]->photoid));
		$loginid	=	intval(trim($data[0]->loginid));
		$arr=getSinglePhoto($userid,$albumid,$photoid,$loginid);
		
		echo json_encode($arr);
	}
	
	if (strtoupper($_REQUEST['type'])=="LIST"){
		$data=json_decode(str_replace("\\","",urldecode($_GET['data'])));
		$pageno		=	($data[0]->pageno);
		$userid		=	intval(trim($data[0]->userid));
		
		$arr=getAllPost($pageno,$userid);
		echo json_encode($arr);
	}
	if (strtoupper($_REQUEST['type'])=="TOPPHOTO"){
		$data=json_decode(str_replace("\\","",urldecode($_GET['data'])));
		$userid		=	intval(trim($data[0]->userid));
		$arr=getAllTopPhoto($userid);
		echo json_encode($arr);
	}
	if (strtoupper($_REQUEST['type'])=="LIKE"){
		$data=json_decode(str_replace("\\","",urldecode($_GET['data'])));
		$userid		=	intval(trim($data[0]->userid));
		$photoid	=	intval(trim($data[0]->photoid));
		$like		=	intval(trim($data[0]->like));
		$posttype	=	intval(trim($data[0]->posttype));
		$arr=likePost($userid,$photoid,$like,$posttype);
		
		echo json_encode($arr);
	}
	if (strtoupper($_REQUEST['type'])=="LIKEUSER"){
		$data=json_decode(str_replace("\\","",urldecode($_GET['data'])));
	
		$userid		=	intval(trim($data[0]->userid));
		$photoid	=	intval(trim($data[0]->photoid));
		$posttype	=	trim($data[0]->posttype);
		$arr=likeUserList($userid,$photoid,$posttype);
		
		echo json_encode($arr);
	}
	if (strtoupper($_REQUEST['type'])=="POST"){
		$data=json_decode(str_replace("\\","",urldecode($_GET['data'])));
		$type		=	(trim($data[0]->posttype));
		$arr=getPost($type);
		echo json_encode($arr);
	}
	
	if (strtoupper($_REQUEST['type'])=="DELETE"){
		$data=json_decode(str_replace("\\","",urldecode($_GET['data'])));
		$userid		=	intval(trim($data[0]->userid));
		$postid		=	intval(trim($data[0]->postid));
		$posttype	=	strtoupper(trim($data[0]->posttype));
		$arr=deletePost($userid,$postid,$posttype);
		echo json_encode($arr);
	}
	
	if (strtoupper($_REQUEST['type'])=="USERALBUM"){
		
		$data		=	json_decode(str_replace("\\","",urldecode($_GET['data'])));
		$userid		=	intval(trim($data[0]->userid));
		$arr=getUserAlbum($userid,$userid);
		echo json_encode($arr);
	}
	if (strtoupper($_REQUEST['type'])=="COLLAGECOMMENT"){
		
		$data		=	json_decode(str_replace("\\","",urldecode($_GET['data'])));
		$userid		=	intval(trim($data[0]->userid));
		$albumid	=	intval(trim($data[0]->albumid));
		$arr=getCollageComment($albumid,$userid);
		
		echo json_encode($arr);
	}
	
}