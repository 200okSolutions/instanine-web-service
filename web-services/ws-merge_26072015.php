<?php
include("config.php");
include("image_functions.php");
include("functions.php");
include("classes/follow.class.php");
include("classes/push-notification.class.php");
?>

<?php
function tag($string)
{
	  $string = str_replace ( "(", ' ', $string );
	  $string = str_replace ( ')', ' ', $string );
	  $string = str_replace ( '[', '', $string );
	  $string = str_replace ( ']', '', $string );
	  $string = str_replace ( '"', '', $string );
	  $string = str_replace ( '\n', '', $string );
	  $string = str_replace ( "n  ", '', $string );
	  $string = str_replace ( '\ ', '', $string );
	  $string = str_replace ( '\\', '', $string );
	  return $string;
}

function removeCommaArr($string)
{
	  $string = str_replace ( '"', '', $string );
	  $string = str_replace ( '[', '', $string );
	  $string = str_replace ( ']', '', $string );	 
	  return $string;
}

function createDir($path){
	return ((!is_dir($path)) ? @mkdir($path, 0777):TRUE); 
}

function escapeChars($value){
	if (is_array($value))
		$value = array_map('security', $value);
	else
		$value = mysql_real_escape_string($value);
	
	return $value;
}

function unEscapeChars($value){
	if (is_array($value))
		$value = array_map('security', $value);
	else
		$value = stripslashes($value);
	return $value;
}

function addMergePhoto($rs, $imgName){
	global $mainlink;
	global $db;
	$status			=	"false";
	$msg 			= 	"You have an error while merging photos. Please try later.";
	$lastid 		= 	'';
	$photoUrl 		= 	'';
	$albumid		=	intval($rs['albumid']);
	$parentid		=	intval($rs['postid']);
	$userid			=	intval($rs['creator']);
	$mergerid		=	intval($rs['merger']);

	$uploadstatus 	= 	"NO";
	
	if ($userid == "" || $userid <= 0) {
		$msg = "Invalid selected user";
	}
	else
	{
		$aluserid	=	getAlbumUserId($albumid);
		$permission	=	getAlbumPermission($albumid);

		$path = "../images/photos/".$userid;
		if(!is_dir($path))
		{
			mkdir($path, 0777);
		}

		$path = "../images/photos/".$userid.'/'.$albumid;
		if(!is_dir($path))
		{
			mkdir($path, 0777);
		}

		$pathorg = "../images/originalphotos/".$userid;
		if(!is_dir($pathorg))
		{
			mkdir($pathorg, 0777);
		}

		$org = "../images/originalphotos/".$userid.'/'.$albumid;
		if(!is_dir($org))
		{
			mkdir($org, 0777);
		}
	
		$dir	=	"../images/photos/".$userid.'/'.$albumid.'/';
		$dir2	=	"images/photos/".$userid.'/'.$albumid.'/';
		/****************orignal image upload path *************************/
		$dir3	=	"../images/originalphotos/".$userid.'/'.$albumid.'/';
		$dir4	=	"images/originalphotos/".$userid.'/'.$albumid.'/';

		$rs['imagecaption'] = '';  // For Test.
		$captionget	= explode(",",$rs['imagecaption']);
		$totalimg	= count($imgName);
		$count = 0;
		$photo = '';

		$sql = "SELECT MAX(ordering) AS maxorder FROM jos_community_photos WHERE albumid = '".$albumid."'";
		$result = mysql_query($sql);
		$rs1 = mysql_fetch_assoc($result);
		$ordermax = intval($rs1['maxorder']) + 1;
		$rand			=	rand(0000,9999);

		if (!isset($imgName['image']['name']))
			$uploadstatus = "NO:Unset";

		$img_name 		= 	$rand.$imgName['image']['name'];
		$img 			=	$imgName['image']['tmp_name'];
		
		$upload_ori		=	"{$dir3}{$img_name}";
		$photo_ori		=	"{$dir4}{$img_name}";		
		
		$upload_small	=	"{$dir}{$img_name}";
		$photo_small	=	"{$dir2}{$img_name}";
		
		$upload_thumb	=	"{$dir}thumb_{$img_name}";
		$photo_thumb	=	"{$dir2}thumb_{$img_name}";
		//if($i == 1)
		$photo = $photo_ori;
		$image = new UploadImage();
		
		try {
			$image->load($img);	
			$image->save($upload_ori, 100);
			$uploadstatus = "YES";
		} catch (Exception $e) {
			if ($uploadstatus == "NO")
				$uploadstatus = "NO:".$e->getMessage();
		}

		if (file_exists($upload_ori)) {
			$image->resize(300, 300);
			$image->save($upload_small, 95);
			$image->resize(150, 150);
			$image->save($upload_thumb, 95);
			
			$caption	=	specialCharRemove(tag(trim($captionget[$count])));
			$img_query	=	"INSERT INTO jos_community_photos SET ".
							" albumid	 	= '".$albumid."', ".
							" caption	 	= '".$caption."', ".
							" published	 	= '1', ".
							" creator	 	= '".$mergerid."', ".
							" original		= '".$photo_ori."', ".
							" image			= '".$photo_small."', ".
							" thumbnail		= '".$photo_thumb."', ".
							" permissions	= '".$permission."', ".
							" filesize		= '0', ".
							" storage		= 'file', ".
							" ordering		= '".$ordermax."', ".
							" parentid		= '".$parentid."', ".
							" created		= '".nowDateTime()."'";
			mysql_query($img_query);
			$lastid = mysql_insert_id();
			$count++;
			$photoUrl = $photo_ori;

			$img_query	=	"UPDATE jos_community_photos_albums SET ".
							" updated 	= '".nowDateTime()."'".
							" WHERE	id	= '".$albumid."'";
			mysql_query($img_query);

			addMergeNotification($mergerid, $aluserid, $lastid, $albumid);

			$msg = "You have successfully merged photos";
			$status = "true";
		}
	}

	$arr = array("message"=>$msg, "status"=>$status, "mergephoto"=>$lastid, "photourl"=>$photoUrl);
	return $arr;
}

/*
 * Delete a photo in the album.
 */

function deleteMergePhoto($userid, $postid){
	global $db;
	$status="false";
	$msg="";

	if ($postid<=0)
		$msg="Invalid selected post";
	else{
		$photoInfo = getAlbumId($postid);
		
		$sqlselect = "SELECT id, albumid FROM jos_community_photos WHERE id='".$postid."'";
		$queryse = mysql_query($sqlselect);
	
		if (mysql_num_rows($queryse)>0)
		{
			$sqllike="delete from jos_community_likes where element = 'photo' AND uid =".$postid;
			mysql_query($sqllike);
			$sqllike="delete from photohashtag where type = 'PHOTOS' AND photoid =".$postid;
			mysql_query($sqllike);
			$sqlComment="delete from jos_community_wall where type = 'photo' AND contentid =".$postid;
			mysql_query($sqlComment);

			// $sqlComment="delete from notification where posttype = 'PHOTO' AND mainid =".$postid;
			// mysql_query($sqlComment);
			
			$sqlComment2="delete from notification_post where posttype in('PHOTO') AND mainid =".$postid;
			mysql_query($sqlComment2);
			
			$sqlComment2="delete from notification_comment where posttype in('PHOTO') AND mainid =".$postid;
			mysql_query($sqlComment2);
				
			$sqlni="delete from jos_community_photos WHERE id='".$postid."'";
			mysql_query($sqlni);
			
			$position = getPhotoPosition($photoInfo['albumid'],$postid);
			orderAlbum($photoInfo['albumid']);
			$msg="Successfully";
			$status="true";
		}else
			$msg="You can't delete this merge photo.";
	}
	$arr=array("message"=>$msg,"status"=>$status);
	return $arr;
}

function likeMergePhoto($userid, $postid, $bLike){
	$status = "false";
	$msg = '';

	if ($userid <= 0 || $userid == ""){
		$msg = "Invalid user or post";
	}else {
		$result = mysql_query("SELECT * FROM jos_community_likes WHERE element = 'photo' AND uid ='".$postid."' LIMIT 1");
		$rs = mysql_fetch_assoc($result);

		if ($rs == null || !isset($rs['like'])) {
			mysql_query(sprintf("INSERT INTO jos_community_likes (`element`, `uid`, `like`, `dislike`) VALUES ('photo', '%s', '%s', '%s')", $postid, ($bLike?$userid:''), (!$bLike?$userid:'')));
		}else{
			if ($rs['like'] != "")
				$like_list = explode(",", $rs['like']);
			else
				$like_list = array();
			if ($rs['dislike'] != "")
				$dislike_list = explode(",", $rs['dislike']);
			else
				$dislike_list = array();

			$like_no = array_search($userid, $like_list);
			$dislike_no = array_search($userid, $dislike_list);

			if ($bLike == 'Like') {
				if ($like_no === FALSE) {
					array_push($like_list, $userid);
				}
				if ($dislike_no !== FALSE) {
					array_splice($dislike_list, $dislike_no, 1);
				}
			}
			if ($bLike == 'Dislike') {
				if ($dislike_no === FALSE) {
					array_push($dislike_list, $userid);
				}
				if ($like_no !== FALSE) {
					array_splice($like_list, $like_no, 1);
				}
			}
			mysql_query(sprintf("UPDATE jos_community_likes SET `like` = '%s', `dislike` = '%s' WHERE element = 'photo' AND uid='%s'", implode(",", $like_list), implode(",", $dislike_list), $postid));

			if ($bLike == 'Like'){
				likeNotification($userid, $postid);
			}
		}
		$msg = 'Successfully';
		$status = "true";	
	}
	$arr = array("message"=>$msg,"status"=>$status,"photoid"=>$postid);
	return $arr;
}

// Delete photos in an album and the album.
function deleteAlbum($albumid) {
	global $db;

	$sqlphoto = "delete from jos_community_photos WHERE albumid = '".$albumid."'";
	mysql_query($sqlphoto);

	$sqlalbum = "delete from jos_community_photos_albums WHERE id = '".$albumid."'";
	mysql_query($sqlalbum);
}

// Arrange the photos in album.
function orderAlbum($albumid) {
	global $db;
	$sql = "SELECT id, ordering FROM jos_community_photos WHERE albumid = '".$albumid."' order by ordering asc";
	$result = mysql_query($sql);
	$photoposi = 0;
	while($rs = mysql_fetch_assoc($result))
	{
		$photoposi++;
		if ($photoposi != $rs['ordering']){
			mysql_query("UPDATE jos_community_photos SET `ordering` = '".$photoposi."' WHERE id ='".$rs['id']."' ");
		}
	}
}

$arr = array();
if (isset($_REQUEST['type']) && $_REQUEST['type'] != "") {
	$data = array();
	if (strtoupper($_REQUEST['type']) == "ADD"){			// Add merged photo to album.
		$arr=addMergePhoto($_POST, $_FILES);
		echo json_encode($arr);
	}
	if (strtoupper($_REQUEST['type']) == "DISLIKE"){			// Delete merged photo in the album.
		$userid = intval(trim($_REQUEST['userid']));
		$postid = intval(trim($_REQUEST['postid']));
		
		$arr = likeMergePhoto($userid, $postid, 'Dislike');
		echo json_encode($arr);
	}
	if (strtoupper($_REQUEST['type']) == "LIKE"){			// Bring the merged photo liked to the front.
		$userid = intval(trim($_REQUEST['userid']));
		$postid = intval(trim($_REQUEST['postid']));
		$arr = likeMergePhoto($userid, $postid, 'Like');
		echo json_encode($arr);
	}
	if (strtoupper($_REQUEST['type']) == "DELETE"){			// Bring the merged photo liked to the front.
		$userid = intval(trim($_REQUEST['userid']));
		$postid = intval(trim($_REQUEST['postid']));
		$arr = deleteMergePhoto($userid, $postid);
		echo json_encode($arr);
	}
}