<?php
include("config.php");
include("functions.php");
include("classes/follow.class.php");

function getFacebookUser($userid,$fbusers){
	global $db;
	$status ="false";
	$FOLLOW = new FOLLOW_CLASS;
	$fbFriend=array();
	if (($userid=="" || $userid<=0))
		$msg="Invalid user ";
	else{
		foreach($fbusers as $fbuser){
			$email=$fbuser->email;
			if ($email!=""){
				$sql="select * from jos_users where id not in(select blocked_userid from jos_community_blocklist where userid=".$userid.") AND  upper(email)='".strtoupper($email)."'";
				$result=$db->query($sql);
				if ($result->size()>0){
					$rs=$result->fetch();
					$userDetail = userInfo($rs['id']);
					if($userid == $rs['id'])
						$userDetail["followstatus"] = "-1";
					else
						$userDetail["followstatus"] = "".$FOLLOW->getUserStatus($userid,$rs['id'],"FOLLOWING");
					$fbFriend[]=$userDetail;
				}
			}
		}
		if (!empty($fbFriend))
			$status="true";
	}
	$arr=array("message"=>$msg,"status"=>$status,"fbuser"=>$fbFriend);
	return $arr;
}

function getFacebookSiteUser($userid,$fbusers){
	global $db;
	$status ="false";
	$FOLLOW = new FOLLOW_CLASS;
	$fbFriend=array();
	if (($userid=="" || $userid<=0))
		$msg="Invalid user ";
	else{
		if(count($fbusers) > 0)
		{
		foreach($fbusers as $fbuser){
			$email=$fbuser->id;
			
			if ($email!=""){
				$sql="select * from jos_users where id not in(select blocked_userid from jos_community_blocklist where userid=".$userid.") AND  upper(fb_id)='".strtoupper($email)."'";
				$result=$db->query($sql);
				if ($result->size()>0){
					$rs=$result->fetch();
					$userDetail = userInfo($rs['id']);
					if($userid == $rs['id'])
						$userDetail["followstatus"] = "-1";
					else
						$userDetail["followstatus"] = "".$FOLLOW->getUserStatus($rs['id'],$userid,"FOLLOWING");
					$fbFriend[]=$userDetail;
				}
			}
			
		}
		}
		if (!empty($fbFriend))
			$status="true";
		else
			$msg="User not found.";
	}
	$arr=array("message"=>$msg,"status"=>$status,"fbuser"=>$fbFriend);
	return $arr;
}

function suggestFriend($userid){
	global $db;
	$status="false";
	$FOLLOW = new FOLLOW_CLASS;
	$follow_data = array();
	if (($userid=="" || $userid<=0))
		$msg="Invalid user ";
	else{
		$sql="select * from view_friends where userid=".$userid." AND friend_id not in  (select blocked_userid from jos_community_blocklist where userid=".$userid.") and friend_id in(select id from jos_users where block=0)  ";

		$result=$db->query($sql);
		if ($result->size()>0){
			while($rs=$result->fetch()){
				$sql1="select * from view_friends where userid=".$rs['friend_id']." and friend_id in(select id from jos_users where block=0)";
				$result1 = $db->query($sql1);
				if ($result1->size()>0){
					$arr1= array();
					while($rs1=$result1->fetch()){
						if($rs1['friend_id'] > 0)
						{
							$userDetail=userInfo($rs1['friend_id']);
							$userDetail["followstatus"] = "".$FOLLOW->getUserStatus($rs1['friend_id'],$userid,"FOLLOWING");
							$follow_data[]= $userDetail;	
						}
					}
				}
				//$follow_data[]=$arr1;
				
			}
		}else
			$msg ="No one your frind found";
			if (!empty($follow_data))
				$status="true";
	}
	$arr=array("message"=>$msg,"status"=>$status,"data"=>$follow_data);
	return $arr;
}
	
$arr=array();

if (strtoupper($_POST['type'])=="CONTACT")
{
	$userid		=	($_POST['userid']);
	$userArr	=	stripcslashes($_POST['fbuser']);
	$fbuser		=	json_decode($userArr);

	$arr	=	getFacebookUser($userid,$fbuser);
	echo json_encode($arr);
}
if (strtoupper($_POST['type'])=="FACEBOOK")
{
	$userid		=	($_POST['userid']);
	$userArr	=	stripcslashes($_POST['fbuser']);
	$fbuser		=	json_decode($userArr);

	$arr	=	getFacebookSiteUser($userid,$fbuser);
	echo json_encode($arr);
}
if (strtoupper($_REQUEST['type'])=="SUGGEST")
{
	$data		=	json_decode(str_replace("\\","",urldecode($_GET['data'])));
	$userid		=	($data[0]->userid);
	$arr		=	suggestFriend($userid);
	echo json_encode($arr);
}
if (strtoupper($_REQUEST['type'])=="RESET")
{
	$data		=	json_decode(str_replace("\\","",urldecode($_GET['data'])));
	$userid		=	escapeChars($data[0]->userid);
	$arr		=	updateBedge($userid,$pass,$oldpass);
	echo json_encode($arr);
}
